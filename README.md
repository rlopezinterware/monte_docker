# Auto sin resguardo #

## Requisitos ##

* node versión 6.9.+
* npm versión 3.10.+
* mongo versión 3+
* docker versión 1.12+


## SPA React js y node server ## 

### Clonar repositorio ###

### Reposotorio Cliente
```
git clone git@github.com:MontePiedadMx/ASR_SPA.git
```

### Reposotorio Organizacion
```
git clone git@54.211.243.152:proyectos/monte-piedad-pr-1376.git
```

### Instalar dependencias con npm ###

```
npm install
```


### Correr el proyecto en modo desarrollo front-end ###

```
npm run dev
```

### Correr el proyecto en modo desarrollo back-end node ###

```
npm run start
```

### Correr el proyecto en modo producción back-end node y front-end ###

```
npm run start prepare
```

## Mongo db ##


### Exportar backup de la base de datos de mongo ###

```
mongodump --host localhost:27017 --db autoavanza -o ./backup
```

### Inportar backup de la base de datos a mongo ###

```
mongorestore  /ruta/backup/autoavanza -d autoavanza
```

## Docker ##

### Construir imagen  ###

```
docker build -t nmp/lanave:v1.0.0 .
```

### Correr imagen  ###
### NODE_DEPLOY [local, docker, bluemix]
### APP_ENV [dev, tst, pre, pro]
### LEVEL_LOG [TRACE, DEBUG, INFO, WARN, ERROR, FATAL]

```
docker run -e NODE_DEPLOY=docker -e APP_ENV=pro -e LEVEL_LOG=ERROR -it -d -p 4000:4000 -p 9090:9090 nmp/lanave:v1.0.0
```
