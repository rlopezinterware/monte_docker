FROM node:6.9.3
MAINTAINER InterWare

# docker, bluemix, local
ENV NODE_DEPLOY "docker"

# "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"
#ENV LEVEL_LOG "DEBUG"

# "dev", "tst", "pre", "pro"
#ENV APP_ENV "dev"

#ENV SSL "1"

RUN mkdir -p /opt/app
COPY package.json /opt/app/package.json
COPY ./Frontend /opt/app
COPY ./Backend/src /opt/app/src
COPY ./crs /opt/app/crs

RUN cd /opt/app && npm install 

WORKDIR /opt/app

CMD ["npm","run", "prepare"]
