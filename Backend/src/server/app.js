#!/usr/bin/env nodejs

/* eslint-disable no-console */

var cfenv = require('cfenv');
var appenv = cfenv.getAppEnv();

var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    cookieParser = require("cookie-parser"),
    http = require("http"),
    fs = require("fs"),
    path = require('path'),
    https = require("https"),
    methodOverride = require("method-override"),
    server =  process.env.SSL ?  https.createServer( { key: fs.readFileSync(path.join(__dirname, '../../', "crs/key.pem")), cert: fs.readFileSync(path.join(__dirname, '../../', "crs/cert.pem")), passphrase: 'montedepiedad'}, app) : http.createServer(app),

    parseString = require("xml2js").parseString;

/*
 * Config file"s inclusion
 */
CONFIG = require("./conf/config.js"); //Config file
ERROR_WRAPPER = require("./conf/error_handler.js");// Error handler should be called as callback: ERROR_WRAPPER.wrapper(res)
ERROR_HANDLER = require("./conf/error_handler.js").handler;// Basic manual error handler: ERROR_HANDLER(err)
LOGGER = require("./conf/logger").Logger; //How to use: LOGGER("ERROR", "ERROR REGISTRADO");
HTTP_REQUEST = require("./utils/httpRequest"); // Generic invocations to others servers and sockets
VALIDATIONS = require("./utils/validation"); // Generic invocations to validations
LABELS = require("./utils/labels"); // Generic labels
MONGOOSE = require("./conf/db"); // Establihses a database connection and returns a reference to this

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json()); //This is a node.js middleware for handling JSON, Raw, Text and URL encoded form data.
app.use(methodOverride()); 
app.use(cookieParser());//Parse Cookie header and populate req.cookies with an object keyed by the cookie names.

var routerApi = express.Router(); // get an instance of the express routerApi
var routerAuth = express.Router(); // get an instance of the express routerAuth

app.use(require("./conf/error_handler.js").errorHandler()); // Sets a handler for all the ungaught errors or exceptions
app.use("/api", routerApi); //adds a prefix for all requests
app.use("/auth", routerAuth); //adds a prefix for all requests

var dir= CONFIG.PUBLIC_PATH;//'./public'; //Establishes "public" as a public directory to serve statics files, as images, js, css or anything else.
app.use(express.static(dir));

/*
 * This section defines the Access-Controll-Allow Headers. CORS
 * Middleware to use for all requests, may be used to make validations or preprocesses
 */
routerApi.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Uid");
    
    // decode token
    if (req.method !== "OPTIONS") {
        if (req.get("authorization")) {
            req.body.token = req.get("authorization");
            req.body.idUser = req.get("uid");
            next(); // make sure we go to the next routes and don"t stop here
        } else {
            // if there is no token
            // return an error
            return res.status(403).send({ 
                codigoError: 403, 
                message: 'No Authorization provided.' 
            });
        }
    } else {
        next(); // make sure we go to the next routes and don"t stop here
    }
});

routerAuth.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next(); // make sure we go to the next routes and don"t stop here
});

routerApi.get('/file',function(req,res){ 
      res.sendFile(__dirname + "/index.html");
});

app.use((err, req, res, next) => {
    ERROR_HANDLER(err);
    res.status(500).send({
        "code": "500",
        "message": err.name
    });
});

/*
 * App controllers
 * All app controller has its own url definitions, in order to admin then in a modular way.
 */
user = require("./controllers/user")(routerAuth);
catalog = require("./controllers/catalog")(routerApi);
client = require("./controllers/client")(routerApi);
quote = require("./controllers/quote")(routerApi);

// Starting the server
var port = process.env.PORT || 4000;

server.listen(port, () => {

    let protocol = process.env.SSL ? "https" : "http"

    if (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") {
        LOGGER("INFO","Node server running on " + protocol + "://localhost:"+ port);
    } else if (process.env.NODE_DEPLOY === "bluemix") {
        LOGGER("INFO","Node server running on "+ appenv.url);
    } else if (process.env.NODE_DEPLOY === "docker") {
        LOGGER("INFO","Node server running on " + protocol + "://10.30.4.213:"+ port);
    }   
});

//If code is <> 0, the server doesn't finish manually 
process.on('exit', (code) => {
    console.info("Exit with code: " + code);
});

//TODO 
process.on('uncaughtException', (err) => {
    
    console.error("Error: " + JSON.stringify(err));

    //if (process.env.NODE_DEPLOY === 'production') {
        /*
        transport.sendMail({
                             from: 'alerts@mail.com',
                             to: 'alert@mail.com',
                             subject: err.message,
                             text: err.stack // [4]
                           }, function (er) {
                                if (er) console.error(er)
                           })
        */
        //process.exit(1);
    //}
    
});

process.on('warning', (warning) => {
  console.log(warning.name);    // Print the warning name
  console.log(warning.message); // Print the warning message
  console.log(warning.stack);   // Print the stack trace
});