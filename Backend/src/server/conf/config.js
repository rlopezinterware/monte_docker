/**
 * Global variables config file.
 */

//private vars:
var _log_path = "src/server/logs/";
var _public_path = "src/server/public"
var IP = process.env.IP || "localhost";

module.exports = {

  /**
   * Data base configurations
   */
  //DB_URL: "mongodb://montepiedad:montepiedad123@localhost/",
  DB_URL: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "mongodb://autoavanza:123456@10.30.4.214/" :
          process.env.APP_ENV === "dev" ? "mongodb://autoavanza:123456@10.30.4.214/" :
          process.env.APP_ENV === "tst" ? "mongodb://autoavanza:123456@10.30.4.214/" :
          process.env.APP_ENV === "pre" ? "mongodb://autoavanza:123456@10.30.4.214/" : 
          process.env.APP_ENV === "pro" ? "mongodb://autoavanza:123456@10.30.4.73/" : "ERROR",

  DATA_BASE: "autoavanza",

  /**
   * Documents configs
   */
  PUBLIC_PATH: _public_path,
  DEFAULT_FILE_NAME: "auto",
  
  /**
   * Category Logs
   */
  CATEGORY : new Array("AutoSinResguardo", "Controllers.Catalog", "Controllers.User", "Controllers.Quote", "Controllers.Client", "Utils.HttpRequest"),
  CATEGORY_LOGGER_DEFAULT : 0,
  CATEGORY_LOGGER_CATALOG : 1,
  CATEGORY_LOGGER_USER : 2,
  CATEGORY_LOGGER_QUOTE : 3,
  CATEGORY_LOGGER_CLIENT : 4,
  CATEGORY_LOGGER_REQUEST : 5,

  /**
   * Log configurations
   */
  LOG_MAX_SIZE: 100024,
  LOG_MAX_BACKUPS: 10000,
  LOGS_ERRORS: _log_path, // + "rest_errors.log",
  LOGS_INFO: _log_path, // + "rest_info.log",
  LOGS_CONNECTIONS: _log_path, // + "rest_connections.log",

  /**
   * Services configurations
   */
  
  SERVICE_TOKEN_AUTHORIZATION: { BODY:  {
      REDIRECT_URI: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "http://localhost:9090/asr/Continue" :
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "https://dev1775-frontasr.mybluemix.net/asr/Continue" : 
            process.env.APP_ENV === "tst" ? "https://tst1775-frontasr.mybluemix.net/asr/Continue" : 
            process.env.APP_ENV === "pre" ? "https://pre1775-frontasr.mybluemix.net/asr/Continue": 
            process.env.APP_ENV === "pro" ? "https://1775-frontasr.mybluemix.net/asr/Continue" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "http://localhost:9090/asr/Continue" : 
            process.env.APP_ENV === "dev" ? "http://10.30.4.213:9090/asr/Continue" : 
            process.env.APP_ENV === "tst" ? "http://10.30.4.44:9090/asr/Continue" :
            process.env.APP_ENV === "pre" ? "http://10.30.4.213:9090/asr/Continue" : 
            process.env.APP_ENV === "pro" ? "http://10.30.4.44:9090/asr/Continue" : "ERROR" : "ERROR",
      GRANT_TYPE_CLIENT: "authorization_code",
      STATE: "getaccess",
      SCOPE: "UserProfile.me"},
      HEADER: {AUTHORIZATION: "Basic ", PROFILE:"Bear ", CONTENT: "application/x-www-form-urlencoded;charset=UTF-8"},
      PROTOCOL: "https:",
      HOST: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" :
            process.env.APP_ENV === "pro" ? "iam.montepiedad.com.mx" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "4444" : 
            process.env.APP_ENV === "dev" ? "4444" : 
            process.env.APP_ENV === "tst" ? "4444" : 
            process.env.APP_ENV === "pre" ? "4444" : 
            process.env.APP_ENV === "pro" ? "4444" : "ERROR", 
      PATH: "/ms_oauth/oauth2/endpoints/oauthservice/tokens",
      POST: "POST"
  },

  SERVICE_TOKEN_REFRESH: { BODY:  {
      REDIRECT_URI: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "http://localhost:9090/asr/Continue" :
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "https://dev1775-frontasr.mybluemix.net/asr/Continue" : 
            process.env.APP_ENV === "tst" ? "https://tst1775-frontasr.mybluemix.net/asr/Continue" : 
            process.env.APP_ENV === "pre" ? "https://pre1775-frontasr.mybluemix.net/asr/Continue": 
            process.env.APP_ENV === "pro" ? "https://1775-frontasr.mybluemix.net/asr/Continue" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "http://localhost:9090/asr/Continue" : 
            process.env.APP_ENV === "dev" ? "http://10.30.4.213:9090/asr/Continue" : 
            process.env.APP_ENV === "tst" ? "http://10.30.4.44:9090/asr/Continue" :
            process.env.APP_ENV === "pre" ? "http://10.30.4.213:9090/asr/Continue" : 
            process.env.APP_ENV === "pro" ? "http://10.30.4.44:9090/asr/Continue" : "ERROR" : "ERROR",
      GRANT_TYPE_CLIENT: "refresh_token"},
      HEADER: {AUTHORIZATION: "Basic ", PROFILE:"Bear ", CONTENT: "application/x-www-form-urlencoded;charset=UTF-8"},
      PROTOCOL: "https:",
      HOST: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "iam.montepiedad.com.mx" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "4444" : 
            process.env.APP_ENV === "dev" ? "4444" : 
            process.env.APP_ENV === "tst" ? "4444" : 
            process.env.APP_ENV === "pre" ? "4444" : 
            process.env.APP_ENV === "pro" ? "4444" : "ERROR",
      PATH: "/ms_oauth/oauth2/endpoints/oauthservice/tokens",
      POST: "POST"
  },

  SERVICE_TOKEN_LOGOUT: { BODY:  {
      REDIRECT_URI: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "http://localhost:9090/asr/Continue" :
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "https://dev1775-frontasr.mybluemix.net/asr/Continue" : 
            process.env.APP_ENV === "tst" ? "https://tst1775-frontasr.mybluemix.net/asr/Continue" : 
            process.env.APP_ENV === "pre" ? "https://pre1775-frontasr.mybluemix.net/asr/Continue": 
            process.env.APP_ENV === "pro" ? "https://1775-frontasr.mybluemix.net/asr/Continue" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "http://localhost:9090/asr/Continue" : 
            process.env.APP_ENV === "dev" ? "http://10.30.4.213:9090/asr/Continue" : 
            process.env.APP_ENV === "tst" ? "http://10.30.4.44:9090/asr/Continue" :
            process.env.APP_ENV === "pre" ? "http://10.30.4.213:9090/asr/Continue" : 
            process.env.APP_ENV === "pro" ? "http://10.30.4.44:9090/asr/Continue" : "ERROR" : "ERROR",
      GRANT_TYPE_CLIENT: "oracle-idm:/oauth/grant-type/resource-access-token/jwt", ACTION: "delete"},
      HEADER: {AUTHORIZATION: "Basic ", PROFILE:"Bear ", CONTENT: "application/x-www-form-urlencoded;charset=UTF-8"},
      PROTOCOL: "https:",
      HOST: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "iam.montepiedad.com.mx" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "4444" : 
            process.env.APP_ENV === "dev" ? "4444" : 
            process.env.APP_ENV === "tst" ? "4444" : 
            process.env.APP_ENV === "pre" ? "4444" : 
            process.env.APP_ENV === "pro" ? "4444" : "ERROR",
      PATH: "/ms_oauth/oauth2/endpoints/oauthservice/tokens",
      POST: "POST"
  },

  SERVICE_USER_PROFILE: { 
      HEADER: {PROFILE:"Bearer ", CONTENT: "application/x-www-form-urlencoded;charset=UTF-8"},
      PROTOCOL: "https:",
      HOST: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "iam.montepiedad.com.mx" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "4444" : 
            process.env.APP_ENV === "dev" ? "4444" : 
            process.env.APP_ENV === "tst" ? "4444" : 
            process.env.APP_ENV === "pre" ? "4444" : 
            process.env.APP_ENV === "pro" ? "4444" : "ERROR",
      PATH: "/ms_oauth/resources/userprofile/me",
      GET: "GET"
  },

  SERVICE_USER_ROLE: { 
      HEADER: {PROFILE:"Basic ", CONTENT: "application/json"},
      PROTOCOL: "https:",
      HOST: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "iamdr.montepiedad.com.mx" : 
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" :
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "187.210.97.199" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "10.30.3.44" : 
            process.env.APP_ENV === "dev" ? "10.30.3.44" : 
            process.env.APP_ENV === "tst" ? "10.30.70.9" :
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "rsi.montepiedad.com.mx" : "ERROR" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "8089" : 
            process.env.APP_ENV === "dev" ? "8089" : 
            process.env.APP_ENV === "tst" ? "8089" : 
            process.env.APP_ENV === "pre" ? "8089" : 
            process.env.APP_ENV === "pro" ? "8089" : "ERROR",
      PATH: "/NMP/GestionIdentidades/Roles/v1/Aplicacion",
      POST: "POST",
      APP: "Auto Sin Resguardo"
  },

  CLIENT_ID: "asr",
  CLIENT_PASSWORD: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "TKsNHPpp7FXscIN6" : 
                   process.env.APP_ENV === "dev" ? "TKsNHPpp7FXscIN6" : 
                   process.env.APP_ENV === "tst" ? "TKsNHPpp7FXscIN6" : 
                   process.env.APP_ENV === "pre" ? "TKsNHPpp7FXscIN6" :
                   process.env.APP_ENV === "pro" ? "MOme5CX6UwE2sBF" : "ERROR",
  
  SERVICE_CATALOG: {
      PROTOCOL: "https:",
      HOST: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "iamdr.montepiedad.com.mx" : 
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "187.210.97.199" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "10.30.3.44" : 
            process.env.APP_ENV === "dev" ? "10.30.3.44" : 
            process.env.APP_ENV === "tst" ? "10.30.70.9" :
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "rsi.montepiedad.com.mx" : "ERROR" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "8089" : 
            process.env.APP_ENV === "dev" ? "8089" : 
            process.env.APP_ENV === "tst" ? "8089" : 
            process.env.APP_ENV === "pre" ? "8089" : 
            process.env.APP_ENV === "pro" ? "8089" : "ERROR",
      GET: "GET",
      ID_CONSUMIDOR: "24",
      ID_DESTINO: "17",
      PATH_MARCA: "/NMP/GestionCatalogos/Autos/v1/Marca",
      PATH_SUB_MARCA: "/NMP/GestionCatalogos/Autos/v1/SubMarca",
      PATH_MODELOS: "/NMP/GestionCatalogos/Autos/v1/Modelo",
      PATH_VERSIONES: "/NMP/GestionCatalogos/Autos/v1/Version",
      PATH_KILOMETRAJE_COMPLEMENTOS: "/NMP/GestionCatalogos/Autos/v1/KilometrajeComplementos",
      PATH_LOCALIDAD_PLD: "/NMP/GestionCatalogos/Sepomex/v1/Localidad"
  },

  SERVICE_CLIENTE: {
      PROTOCOL: "https:",
      HOST: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "iamdr.montepiedad.com.mx" : 
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "187.210.97.199" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "10.30.3.44" : 
            process.env.APP_ENV === "dev" ? "10.30.3.44" : 
            process.env.APP_ENV === "tst" ? "10.30.70.9" :
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "rsi.montepiedad.com.mx" : "ERROR" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "8089" : 
            process.env.APP_ENV === "dev" ? "8089" : 
            process.env.APP_ENV === "tst" ? "8089" : 
            process.env.APP_ENV === "pre" ? "8089" : 
            process.env.APP_ENV === "pro" ? "8089" : "ERROR",
      GET: "GET",
      POST: "POST",
      PUT: "PUT",
      ID_CONSUMIDOR: "24",
      ID_DESTINO: "17",
      CLIENTE_PATH: "/NMP/GestionClientes/Cliente/Credito/v1/Consultar",

      SOLICITUD_ID_CONSUMIDOR: "24",
      SOLICITUD_ID_DESTINO: "100",
      SOLICITUD_PATH: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Solicitud/Cliente",
   
      BP_ID_CONSUMIDOR: "24",
      BP_ID_DESTINO: "9",
      BP_PATH: "/NMP/GestionClientes/SAP/Cliente/v1/crear"
  },

  SERVICE_QUOTE: {
      PROTOCOL: "https:",
      HOST: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === "local") ? "iamdr.montepiedad.com.mx" : 
            process.env.NODE_DEPLOY === "bluemix" ? 
            process.env.APP_ENV === "dev" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "tst" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "187.210.97.199" : "ERROR" :
            process.env.NODE_DEPLOY === "docker" ? 
            process.env.APP_ENV === "local" ? "10.30.3.44" : 
            process.env.APP_ENV === "dev" ? "10.30.3.44" : 
            process.env.APP_ENV === "tst" ? "10.30.70.9" :
            process.env.APP_ENV === "pre" ? "iamdr.montepiedad.com.mx" : 
            process.env.APP_ENV === "pro" ? "rsi.montepiedad.com.mx" : "ERROR" : "ERROR",
      PORT: process.env.APP_ENV === undefined || process.env.APP_ENV === "local" ? "8089" : 
            process.env.APP_ENV === "dev" ? "8089" : 
            process.env.APP_ENV === "tst" ? "8089" : 
            process.env.APP_ENV === "pre" ? "8089" : 
            process.env.APP_ENV === "pro" ? "8089" : "ERROR",
      GET: "GET",
      POST: "POST",
      PUT: "PUT",
      
      FINANZA_ID_CONSUMIDOR: "24",
      FINANZA_ID_DESTINO: "15",
      PATH_PLAZOS: "/NMP/GestionCatalogos/Finanzas/v1/TasaPlazo",

      COTIZACION_ID_CONSUMIDOR: "24",
      COTIZACION_ID_DESTINO: "15",
      PATH_COTIZACION: "/NMP/OperacionCreditos/Cotizador/v1/Vehiculo",

      CREDITO_ID_CONSUMIDOR: "24",
      CREDITO_ID_DESTINO: "15",
      PATH_TABLA_AMORTIZACION: "/NMP/OperacionCreditos/Credito/v1/TablaAmortizacion",

      VSR_ID_CONSUMIDOR: "24",
      VSR_ID_DESTINO: "15",
      PATH_MONTOPRESTAMO: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Prestamo",
      PATH_CONSULTA_SOLICITUD: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Solicitud/Consultar",
      PATH_ACTUALIZAR_SOLICITUD: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Solicitud/Actualizar",
      PATH_CANCELAR_SOLICITUD: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Solicitud/Cancelar",
      PATH_ANALISIS_CREDITO: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Solicitud/AnalisisCredito",
  
      VSR_CATALOGOS_ID_CONSUMIDOR: "24",
      VSR_CATALOGOS_ID_DESTINO: "103",
      PATH_MOTIVO_RECHAZO: "/NMP/OperacionCreditos/VehiculoSinResguardo/Catalogos/v1/MotivosRechazo",

      VSR_CREAR_SOLICITUD_ID_CONSUMIDOR: "24",
      VSR_CREAR_SOLICITUD_ID_DESTINO: "101",
      PATH_CREAR_SOLICITUD: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Solicitud",
      PATH_CREAR_CONTRATO: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/Contrato",
      PATH_FIRMAR_CONTRATO: "/NMP/OperacionCreditos/VehiculoSinResguardo/v1/FirmarContrato"
  }
};
