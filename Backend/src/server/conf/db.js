// Bring Mongoose into the app
var mongoose = require("mongoose");
var cfenv = require('cfenv');

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on("connected", () => {
    LOGGER("INFO", "Mongoose default connection open to " + dbURI);
});

// If the connection throws an error
mongoose.connection.on("error", (err) => {
    LOGGER("INFO", "Mongoose default connection error: " + err);
});

// When the connection is disconnected
mongoose.connection.on("disconnected", () => {
    LOGGER("INFO", "Mongoose default connection disconnected");
});

// If the Node process ends, close the Mongoose connection
process.on("SIGINT", () => {
    mongoose.connection.close(() => {
        LOGGER("INFO", "Mongoose default connection disconnected through app termination");
        process.exit(0);
    });
});

var dbURI = CONFIG.DB_URL + CONFIG.DATA_BASE;
var appenv = cfenv.getAppEnv();
var services = appenv.services;
var mongodb_services = services["compose-for-mongodb"];

if (mongodb_services) {

    var credentials = mongodb_services[0].credentials;
    var ca = [new Buffer(credentials.ca_certificate_base64, 'base64')];
    dbURI = credentials.uri;

    // Create the database connection
    mongoose.connect(credentials.uri, {
        mongos: {
            ssl: true,
            sslValidate: true,
            sslCA: ca,
            poolSize: 1,
            reconnectTries: 1
        }
    });
} else {

    // Create the database connection
    mongoose.connect(dbURI);
}

/*
 * Returns a reference to mongoose library with all previous configurations.
 */
module.exports = mongoose;
