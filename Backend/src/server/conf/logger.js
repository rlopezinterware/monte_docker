/**
 * Configuration for system logger
 * Logger library: Log4js
 * Encapsulates the logger functions, the main idea is to have a centralized way to
 * manage al logging configurations
 */
//Validates if constants config file is included
CONFIG === "undefined" ? CONFIG = require("./config") : null;
log4js = require("log4js");
fs = require('fs');
CATEGORIES = CONFIG.CATEGORY;

var moment = require("moment");
var today = moment(Date.now()).format('YYYYDDMM');
var appenders =  new Array();

appenders.push({"type": "console"});

CATEGORIES.map(category => {
    appenders.push({
                    "type": "file",
                    "filename": CONFIG.LOGS_INFO+"auto_"+today+".log",
                    "category": category,
                    "maxLogSize": CONFIG.LOG_MAX_SIZE,
                    "backups": CONFIG.LOG_MAX_BACKUPS
    });
});

//To reload the configuration and update de DATE
var options = {
  "reloadSecs": 180,
  "tokens" : { "pid" : function() { return process.pid; } }
};

log4js.configure({
    appenders: appenders,
    replaceConsole: true // this will replace the default behavior of the console.log() method
}); 
/*
fs.writeFile('src/server/conf/file.json', JSON.stringify({"appenders" : appenders}), function(err) {
    if( err ){
        console.log( err );
    }
    else{
        console.log('Se ha escrito correctamente');
        //Main configuration
        log4js.configure("src/server/conf/file.json", options);
    }
});
*/

/**
 * Establishes a main logger for all conections in the app.
 * @param {express application} app
 * @returns none
 */
module.exports.getConectionsLogger = (app) => {

    //define logger
    logger = log4js.getLogger(CATEGORIES[0]);
    //logger.setLevel('ERROR');
    /**
     * ### AUTO LEVEL DETECTION
     * http responses 3xx, level = WARN
     * http responses 4xx & 5xx, level = ERROR
     * else.level = INFO
     * app.use(log4js.connectLOGGER(logger, { level: log4js.levels.INFO }));
     * Les details: app.use(log4js.connectLOGGER(logger, { level: "auto", format: ":method :url :status" }));
     * More details:
     */
    app.use(log4js.connectLogger(logger, {
        level: "auto", format: ":method :url :status"
    }));

};

/**
 * Registers a log message and show it in the console.
 * @param {string} logLevel Possible values: ["TRACE","DEBUG", "INFO","WARN", "ERROR", "FATAL", ]
 * @param {string} logMessage A string with the content to be logged.
 * @returns {none}
 */
module.exports.Logger = (logLevel, logMessage, catalog) => {

    var logger = null;
    var catalogIndex = catalog ? catalog : CONFIG.CATEGORY_LOGGER_DEFAULT;

    log1 = ["TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"];
    logger = log4js.getLogger(CATEGORIES[catalogIndex]);
    logger.setLevel(process.env.LEVEL_LOG === undefined ? "TRACE" : process.env.LEVEL_LOG);

    switch (logLevel) {
        case "TRACE":
            logger.trace(logMessage);
            break;
        case "INFO":
            logger.info(logMessage);
            break;
        case "WARN":
            logger.warn(logMessage);
            break;
        case "ERROR":
            logger.error(logMessage);
            break;
        case "FATAL":
            logger.fatal(logMessage);
            break;
        default: //DEBUG
            logger.debug(logMessage);
            break;
    }
};

/*
 * Returns a reference to log4js library with all previous configurations.
 */
module.exports.log4js = log4js;

