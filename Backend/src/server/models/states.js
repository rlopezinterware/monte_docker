"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var statesSchema = new Schema({
    _id: String,
    state: String,
    description: String
});

module.exports = mongoose.model("states", statesSchema, "states");
