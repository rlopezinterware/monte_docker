"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var templateSchema = new Schema({
    type: String,
    template: String
});

module.exports = mongoose.model("templates", templateSchema, "templates");
