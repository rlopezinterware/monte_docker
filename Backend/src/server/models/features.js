"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var featuresSchema = new Schema({
    _id: String,
    feature: String,
    description: String
});

module.exports = mongoose.model("features", featuresSchema, "features");
