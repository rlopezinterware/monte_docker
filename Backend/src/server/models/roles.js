"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var rolesSchema = new Schema({
    _id: String,
    role: String,
    description: String
});

module.exports = mongoose.model("roles", rolesSchema, "roles");
