"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var permissionSchema = new Schema({
    role_id: String,
    feature_id: [{ type: String }],
    state_id: [{ type: String }]
});

module.exports = mongoose.model("permission", permissionSchema, "permission");
