"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var groupsSchema = new Schema({
    _id: String,
    group: String,
    role_id: [{ type: String }],
    description: String
});

module.exports = mongoose.model("groups", groupsSchema, "groups");
