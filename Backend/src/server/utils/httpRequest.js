/*eslint no-unused-vars: "error"*/
var http = require("http");
var https = require('https');

var querystring = require('querystring');
var multer= require("multer");// Manages uploaded files
var fs= require("fs");// Manages uploaded files
var request = require("request");

/* exported doRequestRest: Function to connect with a Rest Services */
doRequestRest = (protocol, host, port, endpoint, method, headers, body, onSuccess, onError) => {

    var dataString;  

    var options = { protocol: protocol,
                    host: host,
                    port: port,
                    path: endpoint,
                    method: method,
                    headers: headers,
                    query: dataString
    };

    var link; 

    if (protocol === "https:"){
        options.requestCert = true;
        options.rejectUnauthorized = false;

        link = https;

    } else {
        link = http;
    }

    if (method === "GET"){
        dataString = querystring.stringify(body);
        options.path = options.path + "?" + dataString;
    } else {
        dataString = JSON.stringify(body);
    }

    var req = link.request(options, (res) => {
        LOGGER("DEBUG", "OPTIONS: " + JSON.stringify(options), CONFIG.CATEGORY_LOGGER_REQUEST);

        res.setEncoding("utf8");
        var responseString = "";

        res.on("data", (chunk) => {
            responseString += chunk;
        });

        res.on("end", () => {
            onSuccess(responseString);
        });
    });

    LOGGER("DEBUG", "BODY: " + dataString, CONFIG.CATEGORY_LOGGER_REQUEST);
    
    if (method !== "GET"){ req.write(dataString); }
    
    req.on("error", (e) => {
        ERROR_HANDLER(e);
        onError(e);
    });
    req.end();
};

doRequestRestURLEncoded = (protocol, host, port, endpoint, method, headers, body, onSuccess, onError) => {

    var data = querystring.stringify(body);

    var options = { protocol: protocol,
                    host: host,
                    port: port,
                    path: endpoint,
                    method: method,
                    headers: headers
    };

    var link; 

    if (protocol === "https:"){
        options.requestCert = true;
        options.rejectUnauthorized = false;
        
        link = https;
    } else {
        link = http;
    }

    var req = link.request(options, (res) => {
        LOGGER("DEBUG", "OPTIONS: " + JSON.stringify(options), CONFIG.CATEGORY_LOGGER_REQUEST);

        res.setEncoding("utf8");
        var responseString = "";

        res.on("data", (chunk) => {
            responseString += chunk;
        });

        res.on("end", () => {
            onSuccess(responseString);
        });
    });

    LOGGER("DEBUG", "BODY: " + data, CONFIG.CATEGORY_LOGGER_REQUEST);
    req.write(data);
    req.on("error", (e) => {
        ERROR_HANDLER(e);
        onError(e);
    });
    req.end();
};
