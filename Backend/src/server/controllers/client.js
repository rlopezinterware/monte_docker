var parseString = require("xml2js").parseString;
var moment = require("moment");

module.exports = (router) => {
    
    getClienteById = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 
            || (data.idCliente === undefined || data.idCliente === "") 
            || (data.idSucursal === undefined || data.idSucursal === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getClienteById.ValidationError Need:  idCliente, idSucursal, idUser, tokken", CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need:  idCliente, idCliente, idSucursal, idUser, tokken"});

        } else {
            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CLIENTE.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CLIENTE.ID_CONSUMIDOR};
           
            var body = {
                "idCliente" : data.idCliente,
                "idSucursal" : data.idSucursal,
                "usuario" : userId
            };
            
            LOGGER("INFO","[User:"+userId+"] "+"getClienteById: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CLIENT);

            doRequestRest(CONFIG.SERVICE_CLIENTE.PROTOCOL 
                , CONFIG.SERVICE_CLIENTE.HOST
                , CONFIG.SERVICE_CLIENTE.PORT
                , CONFIG.SERVICE_CLIENTE.CLIENTE_PATH
                , CONFIG.SERVICE_CLIENTE.POST, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] getClienteById.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_CLIENT);
                
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else {  

                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                    
                        LOGGER("ERROR","[User:"+userId+"] "+response, CONFIG.CATEGORY_LOGGER_CLIENT);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                            
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getClienteById: Exitoso", CONFIG.CATEGORY_LOGGER_CLIENT);
                        return res.status(200).send(responseJSON); 
                    
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_CLIENT);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getClienteSolicitud = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 
            || (data.idCliente === undefined || data.idCliente === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getClienteSolicitud.ValidationError Need:  idCliente, idUser, tokken", CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need:  idCliente, idUser, tokken"});

        } else {
            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CLIENTE.SOLICITUD_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CLIENTE.SOLICITUD_ID_CONSUMIDOR};
           
            var body = {
                "numeroCliente" : data.idCliente
            };
            
            LOGGER("INFO","[User:"+userId+"] "+"getClienteSolicitud: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CLIENT);
            
            doRequestRest(CONFIG.SERVICE_CLIENTE.PROTOCOL 
                , CONFIG.SERVICE_CLIENTE.HOST
                , CONFIG.SERVICE_CLIENTE.PORT
                , CONFIG.SERVICE_CLIENTE.SOLICITUD_PATH
                , CONFIG.SERVICE_CLIENTE.POST, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] getClienteSolicitud.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_CLIENT);
                    
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else {  

                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                    
                        LOGGER("ERROR","[User:"+userId+"] "+response, CONFIG.CATEGORY_LOGGER_CLIENT);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                            
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getClienteSolicitud: Exitoso", CONFIG.CATEGORY_LOGGER_CLIENT);
                        return res.status(200).send(responseJSON);        
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_CLIENT);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };


    newClienteBP = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"newClienteBP.ValidationError Need:  idUser, tokken", CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need:  idUser, tokken"});

        } else {
            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CLIENTE.BP_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CLIENTE.BP_ID_CONSUMIDOR};
        
            var body = {
                "Accionista" : data.Accionista,
                "AccionistaPa" : data.AccionistaPa,
                "ActPropia" : data.ActPropia,
                "Actividad" : data.Actividad,
                "Apematerno" : data.Apematerno,
                "Apepaterno" : data.Apepaterno,
                "Calle" : data.Calle,
                "Calle1" : data.Calle1,
                "Calle2" : data.Calle2,
                "CapacidadPago" : data.CapacidadPago,
                "CatRiesgo" : data.CatRiesgo,
                "Celular" : data.Celular,
                "Clabe" : data.Clabe,
                "ClaveCol" : data.ClaveCol,
                "CompPago" : data.CompPago,
                "Comportamiento" : data.Comportamiento,
                "Cp" : data.Cp,
                "Curp" : data.Curp,
                "Dependientes" : data.Dependientes,
                "Desempenio" : data.Desempenio,
                "Email" : data.Email,
                "Escolaridad" : data.Escolaridad,
                "Estado" : data.Estado,
                "Estadocivil" : data.Estadocivil,
                "ExtensionTel" : data.ExtensionTel,
                "FechaIngreso" : data.FechaIngreso,
                "FechaVivienda" : data.FechaVivienda,
                "Fechanac" : data.Fechanac,
                "GiroCnbv" : data.GiroCnbv,
                "IdCliente" : data.IdCliente,
                "ListaNegra" : data.ListaNegra,
                "LocalidadPld" : data.LocalidadPld,
                "Lote" : data.Lote,
                "LugarTrabajo" : data.LugarTrabajo,
                "Manzana" : data.Manzana,
                "Nacionalidad" : data.Nacionalidad,
                "NegocioPropio" : data.NegocioPropio,
                "Nombre" : data.Nombre,
                "Nombre2" : data.Nombre2,
                "NumCuenta" : data.NumCuenta,
                "Numeroext" : data.Numeroext,
                "Numeroint" : data.Numeroint,
                "PagosNmp" : data.PagosNmp,
                "Pais" : data.Pais,
                "PaisNac" : data.PaisNac,
                "ParientePpe" : data.ParientePpe,
                "PersonaPolitica" : data.PersonaPolitica,
                "Pi" : data.Pi,
                "PrestaPromValu" : data.PrestaPromValu,
                "PrestamoProm" : data.PrestamoProm,
                "PrestamosNmp" : data.PrestamosNmp,
                "RecursoLegal" : data.RecursoLegal,
                "RegioNac" : data.RegioNac,
                "Rfc" : data.Rfc,
                "SectorNmp" : data.SectorNmp,
                "Sexo" : data.Sexo,
                "Siva" : data.Siva,
                "Sueldo" : data.Sueldo,
                "TelLabora" : data.TelLabora,
                "Telefono" : data.Telefono,
                "Telefono2" : data.Telefono2,
                "TipoCuenta" : data.TipoCuenta,
                "TipoVivienda" : data.TipoVivienda,
                "Tipopersona" : data.Tipopersona,
                "Town" : data.Town
            };
            
            LOGGER("INFO","[User:"+userId+"] "+"newClienteBP: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CLIENT);
            
            doRequestRest(CONFIG.SERVICE_CLIENTE.PROTOCOL 
                , CONFIG.SERVICE_CLIENTE.HOST
                , CONFIG.SERVICE_CLIENTE.PORT
                , CONFIG.SERVICE_CLIENTE.BP_PATH
                , CONFIG.SERVICE_CLIENTE.POST, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] newClienteBP.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_CLIENT);
                    
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else {  

                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                    
                        LOGGER("ERROR","[User:"+userId+"] "+response, CONFIG.CATEGORY_LOGGER_CLIENT);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                        
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] newClienteBP: Exitoso", CONFIG.CATEGORY_LOGGER_CLIENT);
                        return res.status(200).send(responseJSON);  
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_CLIENT);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };


    //Link routes and functions
    router.post("/getClienteById", getClienteById);
    router.post("/getClienteSolicitud", getClienteSolicitud);
    router.post("/newClienteBP", newClienteBP);
};
