var multer= require("multer");// Manages uploaded files

module.exports = (router) => {

    getPlazos = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.producto === undefined || data.producto === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"GetPlazos.ValidationError Need: idUser, producto, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, producto, token "});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.FINANZA_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.FINANZA_ID_CONSUMIDOR};
            var body = data;

            LOGGER("INFO","[User:"+userId+"] "+"GetPlazos.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_PLAZOS
                , CONFIG.SERVICE_QUOTE.GET, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] getPlazos.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
                
                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 

                } else { 
                    var responseJSON = JSON.parse(response);                     

                    if (responseJSON.errorMessage) {
                        LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(responseJSON), CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getPlazos: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };
    
    getMontoPrestamo = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 
            || (data.idMarca === undefined || data.idMarca === "") 
            || (data.idSubMarca === undefined || data.idSubMarca === "") 
            || (data.idModelo === undefined || data.idModelo === "") 
            || (data.idVersion === undefined || data.idVersion === "") 
            || (data.idKilometraje === undefined || data.idKilometraje === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getMontoPrestamo.ValidationError Need:  idUser, idMarca, idSubMarca, idModelo, idVersion, idKilometraje, tokken" + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need:  idUser, idMarca, idSubMarca, idModelo, idVersion, idKilometraje, tokken"});

        } else {
            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_ID_CONSUMIDOR};
           
            var body = {
                "vehiculo" : {
                    "anio" : data.anio,
                    "marca"       : {"idMarca" : data.idMarca, "descripcion" : data.marcaDescription}, 
                    "submarca"    : {"idSubMarca" : data.idSubMarca, "descripcion" : data.submarcaDescription}, 
                    "modelo"      : {"idModelo" : data.idModelo, "descripcion" : data.modeloDescription},
                    "version"     : {"idVersion" : data.idVersion, "descripcion" : data.versionDescription},
                    "kilometraje" : {"idKilometraje" : data.idKilometraje, "descripcion" : data.kilometrajeDescription},
                    complementos  : {}
                },
                "cotizar" : {"tipoVehiculo" : 1, "plazo" : data.plazo}
            };
            
            if (data.idComplemento !== undefined && data.idComplemento !== "") {

                let arrayDescription = data.equipoAdicionalDescription.split("#");
			    let index = 0;

                let array = data.idComplemento.split(",");
                let complementos = array.map(complemento => {
                    return {
                        "idComplemento": complemento,
                        "descripcion": arrayDescription[index]
				    }; index = index + 1;
                });
            
                body.vehiculo.complementos["complemento"] = complementos;            
            }

            LOGGER("INFO","[User:"+userId+"] "+"getMontoPrestamo: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_MONTOPRESTAMO
                , CONFIG.SERVICE_QUOTE.POST, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] getMontoPrestamo.RESPONSE: " +  response, CONFIG.CATEGORY_LOGGER_QUOTE);
                    
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else {  

                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                    
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                            
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getMontoPrestamo: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getCotizacion = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.tipoVehiculo === undefined || data.tipoVehiculo === "") 
            || (data.plazo === undefined || data.plazo === "") 
            || (data.montoAvaluo === undefined || data.montoAvaluo === "") 
            || (data.anio === undefined || data.anio === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getCotizacion.ValidationError Need:  idUser, tipoVehiculo, plazo, montoAvaluo, anio, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need:  idUser, tipoVehiculo, plazo, montoAvaluo, anio, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.COTIZACION_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.COTIZACION_ID_CONSUMIDOR};
            var body = {
                "tipoVehiculo": data.tipoVehiculo, 
                "plazo": data.plazo, 
                "montoAvaluo": data.montoAvaluo, 
                "calificacionMidas" : data.calificacionMidas, 
                "sobreAforo": data.sobreAforo,
                "anio": data.anio
            };

            LOGGER("INFO","[User:"+userId+"] "+"getCotizacion.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_COTIZACION
                , CONFIG.SERVICE_QUOTE.GET, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] getCotizacion.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);

                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                
                    return res.status(500).send(dataResponse); 

                } else { 
                    var responseJSON = JSON.parse(response);                                
                    
                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getCotizacion: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getTablaAmortizacion = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 
            || (data.sociedad === undefined || data.sociedad === "") 
            || (data.producto === undefined || data.producto === "") 
            || (data.subproducto === undefined || data.subproducto === "") 
            || (data.montoSolicitado === undefined || data.MontoSolicitado === "") 
            || (data.frecuencia === undefined || data.frecuencia === "") 
            || (data.plazo === undefined || data.plazo === "") 
            || (data.tasaInteres === undefined || data.tasaInteres === "") 
            || (data.fechaDesembolso === undefined || data.fechaDesembolso === "") 
            || (data.tipoPago === undefined || data.tipoPago === "")
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getTablaAmortizacion.ValidationError Need: idUser, sociedad, producto, subproducto, montoSolicitado, frecuencia, plazo, tasaInteres, fechaDesembolso, tipoPago, idUser, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, sociedad, producto, subproducto, montoSolicitado, frecuencia, plazo, tasaInteres, fechaDesembolso, tipoPago, idUser, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.CREDITO_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.CREDITO_ID_CONSUMIDOR};
            var body = {
                "sociedad" : data.sociedad,
                "producto" : data.producto,
                "subproducto" : data.subproducto,
                "montoSolicitado" : data.montoSolicitado,
                "frecuencia" : data.frecuencia,
                "plazo" : data.plazo,
                "tasaInteres" : data.tasaInteres,
                "fechaDesembolso" : data.fechaDesembolso,
                "tipoPago" : data.tipoPago
            };

            LOGGER("INFO","[User:"+userId+"] "+"getTablaAmortizacion: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);
            
            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_TABLA_AMORTIZACION
                , CONFIG.SERVICE_QUOTE.POST, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] getTablaAmortizacion.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
                
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else { 
                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                        
                    } else {        
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getTablaAmortizacion: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON);
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getSolicitud = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.folio === undefined || data.folio === "") 
            || (data.numeroCliente === undefined || data.numeroCliente === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getSolicitud.ValidationError Need: idUser, folio, numeroCliente, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, folio, numeroCliente, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_ID_CONSUMIDOR};
            var body = {
                "folio": data.folio, 
                "numeroCliente": data.numeroCliente,
            };


            LOGGER("INFO","[User:"+userId+"] "+"getSolicitud.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);
            
            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_CONSULTA_SOLICITUD
                , CONFIG.SERVICE_QUOTE.POST, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] getSolicitud.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);

                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 

                } else { 
                    var responseJSON = JSON.parse(response);                                
             
                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ responseJSON, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getSolicitud: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    updateSolicitud = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.solicitud === undefined || data.solicitud === "" ) 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"updateSolicitud.ValidationError Need: idUser, solicitud, token" + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, solicitud, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_ID_CONSUMIDOR};
            //var body = data.solicitud;

            var solicitud = data.solicitud;

            var body = {
                "folio" : solicitud.folio,
                "cliente" : solicitud.cliente,
                "vehiculo" : {
                    "tipoVehiculo" : solicitud.vehiculo.tipoVehiculo,
                    "vin" : solicitud.vehiculo.vin,
                    "serieMotor" : solicitud.vehiculo.serieMotor,
                    "placas" : solicitud.vehiculo.placas,
                    "marca" : solicitud.vehiculo.marca,
                    "submarca" : solicitud.vehiculo.submarca,
                    "modelo" : solicitud.vehiculo.modelo,
                    "version" : solicitud.vehiculo.version,
                    "kilometraje" : solicitud.vehiculo.kilometraje,
                    "complementos" : solicitud.vehiculo.complementos,
                    "color" : solicitud.vehiculo.color,
                    "serieChasis" : solicitud.vehiculo.serieChasis,
                    "numeroTarjetaCirculacion" : solicitud.vehiculo.numeroTarjetaCirculacion,
                    "numeroFactura" : solicitud.vehiculo.numeroFactura,
                    "rfcEmisor" : solicitud.vehiculo.rfcEmisor,
                    "agenciaOrigen" : solicitud.vehiculo.agenciaOrigen,
                    "comentarios" : solicitud.vehiculo.comentarios,
                    "anio" : solicitud.vehiculo.anio,
                    "fechaEmisionFactura" : solicitud.vehiculo.fechaEmisionFactura,
                    "montoFactura" : solicitud.vehiculo.montoFactura,
                    "lugarEmisionFactura" : solicitud.vehiculo.lugarEmisionFactura,
                },
                "cotizacion" : {
                    "montoAvaluo" : solicitud.cotizacion.montoAvaluo,
                    "montoCastigo" : solicitud.cotizacion.montoCastigo
                }
            }

            if (solicitud.listaOfertas) {

                body.listaOfertas = {};
                body.listaOfertas.oferta = [{
                    "plazo": solicitud.listaOfertas.oferta[0].plazo,
                    "montoCredito": solicitud.listaOfertas.oferta[0].montoCredito,
                    "pagoMensual": solicitud.listaOfertas.oferta[0].pagoMensual,
                    "tasa": solicitud.listaOfertas.oferta[0].tasa,
                    "creadoPor": solicitud.listaOfertas.oferta[0].creadoPor,
                    "fechaCreacion": solicitud.listaOfertas.oferta[0].fechaCreacion,
                    "actualizadoPor": solicitud.listaOfertas.oferta[0].actualizadoPor,
                    "fechaActualizacion": solicitud.listaOfertas.oferta[0].fechaActualizacion
                }];
            }

            LOGGER("INFO","[User:"+userId+"] "+"updateSolicitud.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_ACTUALIZAR_SOLICITUD
                , CONFIG.SERVICE_QUOTE.POST, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] updateSolicitud.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
                
                if(isHTML(response)){                    
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else { 
                    var responseJSON = JSON.parse(response);                                
             
                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);       
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] updateSolicitud: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    cancelSolicitud = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.folio === undefined || data.folio === "") 
            || (data.numeroCliente === undefined || data.numeroCliente === "") 
            || (data.motivo === undefined || data.motivo === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"cancelSolicitud.ValidationError Need: idUser, folio, numeroCliente, motivo, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, folio, numeroCliente, motivo, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_ID_CONSUMIDOR};
            var body = {
                "folio": data.folio, 
                "numeroCliente": data.numeroCliente, 
                "motivo": data.motivo
            };

            LOGGER("INFO","[User:"+userId+"] "+"cancelSolicitud.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_CANCELAR_SOLICITUD
                , CONFIG.SERVICE_QUOTE.PUT, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] cancelSolicitud.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);

                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 

                } else { 
                    var responseJSON = JSON.parse(response);                                
             
                    if (responseJSON.codigoError) {
                        responseJSON.code = 500;
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] cancelSolicitud: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getMotivosRechazo = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getMotivosRechazo.ValidationError Need: idUser, token" + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_CATALOGOS_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_CATALOGOS_ID_CONSUMIDOR};
            var body = {};

            LOGGER("INFO","[User:"+userId+"] "+"getMotivosRechazo.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_MOTIVO_RECHAZO
                , CONFIG.SERVICE_QUOTE.GET, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] getMotivosRechazo.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);

                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 

                } else { 
                    var responseJSON = JSON.parse(response);                                

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] getMotivosRechazo: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    analisisCredito = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.folio === undefined || data.folio === "") 
            || (data.numeroCliente === undefined || data.numeroCliente === "") 
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"analisisCredito.ValidationError Need: idUser, folio, numeroCliente, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, folio, numeroCliente, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_ID_CONSUMIDOR};
            var body = {
                "folio": data.folio, 
                "numeroCliente": data.numeroCliente    
            };

            LOGGER("INFO","[User:"+userId+"] "+"analisisCredito.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_ANALISIS_CREDITO
                , CONFIG.SERVICE_QUOTE.PUT, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] analisisCredito.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
            
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else { 
                    var responseJSON = JSON.parse(response);   

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] analisisCredito: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    newSolicitud = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
            || (data.idProducto === undefined || data.idProducto === "")  
            || (data.nombreProducto === undefined || data.nombreProducto === "")  
            || (data.idSubProducto === undefined || data.idSubProducto === "")  
            || (data.numeroCliente === undefined || data.numeroCliente === "")  
            || (data.tipoVehiculo === undefined || data.tipoVehiculo === "")  
            || (data.vin === undefined || data.vin === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"newSolicitud.ValidationError Need: idUser, token, idProducto, nombreProducto, idSubProducto, numeroCliente, tipoVehiculo, vin " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, token, idProducto, nombreProducto, idSubProducto, numeroCliente, tipoVehiculo, vin"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_CREAR_SOLICITUD_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_CREAR_SOLICITUD_ID_CONSUMIDOR};

            var body = {
                "idProducto":data.idProducto,
                "nombreProducto": data.nombreProducto,
                "idSubProducto": data.idSubProducto,
                "cliente" : {
                    "numeroCliente" : data.numeroCliente
                },
                "vehiculo" : {
                    "tipoVehiculo" : data.tipoVehiculo,
                    "vin" : data.vin   
                }
            };

            LOGGER("INFO","[User:"+userId+"] "+"newSolicitud.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_CREAR_SOLICITUD
                , CONFIG.SERVICE_QUOTE.POST, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] newSolicitud.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
            
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else { 
                    var responseJSON = JSON.parse(response);   

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] newSolicitud: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    newContrato = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
            || (data.folio === undefined || data.folio === "")  
            || (data.idCliente === undefined || data.idCliente === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"newContrato.ValidationError Need: idUser, token, idCliente, folio" + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, token, idCliente, folio"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_CREAR_SOLICITUD_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_CREAR_SOLICITUD_ID_CONSUMIDOR};

            var body = {
                "idCliente" : data.idCliente,
                "folio" : data.folio,
                "datosContrato": {
                    "sociedad": data.sociedad,
                    "producto": data.producto,
                    "subproducto": data.subproducto,
                    "montoSolicitado": data.montoSolicitado,
                    "frecuencia": data.frecuencia,
                    "plazo": data.plazo,
                    "tasaInteres": data.tasaInteres,
                    "fechaDesembolso": data.fechaDesembolso,
                    //"fechaPrimerCobro": data.fechaPrimerCobro,
                    "multiploMoratorio": data.multiploMoratorio,
                    "origen": data.origen,
                    "sucursal": data.sucursal,
                    "promotor": data.idUser, //"GMORALESB"
                    "destino": data.destino,
                    "tipoPago": data.tipoPago,
                    "porcentajeAmortizacion": data.porcentajeAmortizacion,
                    "diasGracia": data.diasGracia,
                    "pagoMensual": data.pagoMensual
                }/*,
                    ///OPCIONAL  à Para garantías, de momento no enviar este nodo
                "garantia": {
                    "numeroGarantia": data.numeroGarantia
                }
                */
            };

            LOGGER("INFO","[User:"+userId+"] "+"newContrato.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_CREAR_CONTRATO
                , CONFIG.SERVICE_QUOTE.POST, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] newContrato.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
            
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else { 
                    var responseJSON = JSON.parse(response);   

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] newContrato: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    desembolso = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0
            || (data.idUser === undefined || data.idUser === "")
            || (data.token === undefined || data.token === "")  
            || (data.folio === undefined || data.folio === "")  
            || (data.idCliente === undefined || data.idCliente === "")  
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"desembolso.ValidationError Need: idUser, token, idCliente, folio" + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, token, idCliente, folio"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_QUOTE.VSR_CREAR_SOLICITUD_ID_DESTINO, "idConsumidor": CONFIG.SERVICE_QUOTE.VSR_CREAR_SOLICITUD_ID_CONSUMIDOR};

            var body = {
                folio: data.folio,
                numeroCliente: data.idCliente
            };

            LOGGER("INFO","[User:"+userId+"] "+"desembolso.BODY: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_QUOTE.PROTOCOL 
                , CONFIG.SERVICE_QUOTE.HOST
                , CONFIG.SERVICE_QUOTE.PORT
                , CONFIG.SERVICE_QUOTE.PATH_FIRMAR_CONTRATO
                , CONFIG.SERVICE_QUOTE.PUT, headers, body
                , (response) => {

                LOGGER("DEBUG","[User:"+userId+"] desembolso.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
            
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                    
                    return res.status(500).send(dataResponse); 
                } else { 
                    var responseJSON = JSON.parse(response);   

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+ response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "[User:"+userId+"] desembolso: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    //Link routes and functions
    router.post("/getPlazos", getPlazos);
    router.post("/getMontoPrestamo", getMontoPrestamo);
    router.post("/getCotizacion", getCotizacion);
    router.post("/getTablaAmortizacion", getTablaAmortizacion);
    router.post("/getSolicitud", getSolicitud);
    router.post("/updateSolicitud", updateSolicitud);
    router.post("/cancelSolicitud", cancelSolicitud);
    router.post("/getMotivosRechazo", getMotivosRechazo);
    router.post("/analisisCredito", analisisCredito);
    router.post("/newSolicitud", newSolicitud);
    router.post("/newContrato", newContrato);
    router.post("/desembolso", desembolso);
};
