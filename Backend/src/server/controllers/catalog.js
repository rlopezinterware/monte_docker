var parseString = require("xml2js").parseString;
var Templates = require("../models/templates.js");

module.exports = (router) => {

    getMarcas = (req, res) => {

        var data = req.body;
  
        if (Object.keys(data).length === 0 || (data.idUser === undefined || data.token === "")) {

            LOGGER("ERROR", "[User:"+data.idUser+"] getMarcas.ValidationError: Need idUser, token", CONFIG.CATEGORY_LOGGER_CATALOG);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, token "});

        } else {
            
            var userId = data.idUser; 
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CATALOG.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CATALOG.ID_CONSUMIDOR};
            var body = {};

            LOGGER("INFO","[User:"+userId+"] getMarcas.BODY: " + JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CATALOG);
            
            doRequestRest(CONFIG.SERVICE_CATALOG.PROTOCOL 
                , CONFIG.SERVICE_CATALOG.HOST
                , CONFIG.SERVICE_CATALOG.PORT
                , CONFIG.SERVICE_CATALOG.PATH_MARCA
                , CONFIG.SERVICE_CATALOG.GET, headers, body
                , (response) => {
                
                LOGGER("DEBUG","[User:"+userId+"] getMarcas.RESPONSE: " + response , CONFIG.CATEGORY_LOGGER_CATALOG);
                
                if(isHTML(response)) {
                        LOGGER("ERROR","[User:"+userId+"] ", CONFIG.CATEGORY_LOGGER_CATALOG);                      
                        var dataResponse = new Object();
                        dataResponse.code = 500;
                        dataResponse.codigoError = 500;
                        dataResponse.descripcionError = "Datos Incorrectos";
                        dataResponse.data = response;

                        return res.status(500).send(dataResponse);
                } else {       
                    var responseJSON = JSON.parse(response);
                    
                    if (responseJSON.codigoError) {
                        LOGGER("ERROR", response, CONFIG.CATEGORY_LOGGER_CATALOG);
                        responseJSON.code = 500;
                    
                        return res.status(500).send(responseJSON);
                    } else {

                        var dataResponse = new Object();

                        if(isEmptyJSON(responseJSON) || isEmptyJSON(responseJSON.marca)){
                            dataResponse.code = 404;
                            dataResponse.data = [];
                        }else{    
                            var list = responseJSON.marca;
                            var arr = new Array();
                            list.map(item => {
                                arr.push({
                                    "id": item.id === null ? "" : item.id,
                                    "valor": item.descripcion === null ? "" : item.descripcion.trim()
                                });
                            });
                            dataResponse.code = 200;
                            dataResponse.data = arr;
                        }

                        LOGGER("INFO", "[User:"+userId+"] getMarcas: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send(dataResponse); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR", err, CONFIG.CATEGORY_LOGGER_CATALOG);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getSubMarcas = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 || (data.idMarca === undefined || data.idMarca === "") ||
            (data.idUser === undefined || data.idUser === "") || (data.token === undefined || data.token === "") ) {
            
            LOGGER("ERROR", "[User:"+data.idUser+"] getSubMarcas.ValidationError: Need idUser, idMarca, token", CONFIG.CATEGORY_LOGGER_CATALOG);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, idMarca, token "});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CATALOG.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CATALOG.ID_CONSUMIDOR};
            var body = { "idMarca" : data.idMarca };

            LOGGER("INFO","[User:"+userId+"] getSubMarcas.BODY: " + JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CATALOG);
            
            doRequestRest(CONFIG.SERVICE_CATALOG.PROTOCOL 
                , CONFIG.SERVICE_CATALOG.HOST
                , CONFIG.SERVICE_CATALOG.PORT
                , CONFIG.SERVICE_CATALOG.PATH_SUB_MARCA
                , CONFIG.SERVICE_CATALOG.GET, headers, body
                , (response) => {
                
                LOGGER("DEBUG","[User:"+userId+"] getSubMarcas.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_CATALOG);
                
                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                    
                    return res.status(500).send(dataResponse); 

                } else { 

                    var responseJSON = JSON.parse(response);
                    
                    if (responseJSON.codigoError) {
                        LOGGER("ERROR", response, CONFIG.CATEGORY_LOGGER_CATALOG);
                        responseJSON.code = 500;
                    
                        return res.status(500).send(responseJSON);
                    } else {
                        var dataResponse = new Object();

                        if(isEmptyJSON(responseJSON) || isEmptyJSON(responseJSON.subMarca)){
                            dataResponse.code = 404;
                            dataResponse.data = [];

                        }else{
                            dataResponse.code = 200;
                            var list = responseJSON.subMarca;
                            var arr = new Array();
                            
                            list.map(item => {
                                arr.push({
                                    "id": item.id === null ? "" : item.id,
                                    "valor": item.descripcion === null ? "" : item.descripcion.trim()
                                });
                            });
                            dataResponse.data = arr;   
                        }
                        LOGGER("INFO", "[User:"+userId+"] getSubMarcas: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(dataResponse.code).send(dataResponse); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR", err, CONFIG.CATEGORY_LOGGER_CATALOG);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getModelos = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 ||
            (data.idMarca === undefined || data.idMarca === "") || (data.idSubMarca === undefined || data.idSubMarca === "") ||
            (data.idUser === undefined || data.idUser === "") || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] getModelos.ValidationError Need: idUser, idMarca, idSubMarca, idUser, token",CONFIG.CATEGORY_LOGGER_CATALOG);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, idMarca, idSubMarca, idUser, token "});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CATALOG.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CATALOG.ID_CONSUMIDOR};
            var body = { "idMarca" : data.idMarca, "idSubMarca" : data.idSubMarca };
            
            LOGGER("INFO","[User:"+userId+"] getModelos.BODY: " + JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CATALOG);
            
            doRequestRest(CONFIG.SERVICE_CATALOG.PROTOCOL 
                , CONFIG.SERVICE_CATALOG.HOST
                , CONFIG.SERVICE_CATALOG.PORT
                , CONFIG.SERVICE_CATALOG.PATH_MODELOS
                , CONFIG.SERVICE_CATALOG.GET, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] getModelos.RESPONSE: "+ response , CONFIG.CATEGORY_LOGGER_CATALOG);
                
                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                
                    return res.status(500).send(dataResponse); 

                } else { 
                    
                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR",response,CONFIG.CATEGORY_LOGGER_CATALOG);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                        
                        var dataResponse = new Object();
                
                        if(isEmptyJSON(responseJSON) || isEmptyJSON(responseJSON.modelo)){
                            dataResponse.code = 404;
                            dataResponse.data = [];
                        }else{
                            dataResponse.code = 200;
                            
                            var list = responseJSON.modelo;
                            var arr = new Array();
                            
                            list.map(item => {
                                arr.push({
                                    "id": item.id === null ? "" : item.id,
                                    "valor": item.descripcion === null ? "" : item.descripcion.trim()
                                });
                            });
                            dataResponse.data = arr;
                        }
                        LOGGER("INFO", "[User:"+userId+"] getModelos: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(dataResponse.code).send(dataResponse); 
                    }
                }
            }, (err) => {
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getVersiones = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 || (data.idMarca === undefined || data.idMarca === "") ||
            (data.idSubMarca === undefined || data.idSubMarca === "") || (data.idModelo === undefined || data.idModelo === "") ||
            (data.idUser === undefined || data.idUser === "") || (data.token === undefined || data.token === "") ) {

            LOGGER("ERROR","[User:"+data.idUser+"] getVersiones.ValidationError Need: idUser, idMarca, idSubMarca, idModelo, token",CONFIG.CATEGORY_LOGGER_CATALOG);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, idMarca, idSubMarca, idModelo, token "});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CATALOG.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CATALOG.ID_CONSUMIDOR};
            var body = {"idMarca" : data.idMarca, "idSubMarca" : data.idSubMarca, "idModelo" : data.idModelo };
            
            LOGGER("INFO","[User:"+userId+"] getVersiones.BODY: " + JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CATALOG);
            
            doRequestRest(CONFIG.SERVICE_CATALOG.PROTOCOL 
                , CONFIG.SERVICE_CATALOG.HOST
                , CONFIG.SERVICE_CATALOG.PORT
                , CONFIG.SERVICE_CATALOG.PATH_VERSIONES
                , CONFIG.SERVICE_CATALOG.GET, headers, body
                , (response) => {
                    
                LOGGER("DEBUG","[User:"+userId+"] getVersiones.RESPONSE: " + response , CONFIG.CATEGORY_LOGGER_CATALOG);
                
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;

                    return res.status(500).send(dataResponse); 
                } else { 
                
                    if (response.codigoError) {
                        LOGGER("ERROR",response,CONFIG.CATEGORY_LOGGER_CATALOG);
                        response.code = 500;
                
                        return res.status(500).send(response);
                    } else {
                        var responseJSON = JSON.parse(response);
                        var dataResponse = new Object();
                
                        if(isEmptyJSON(responseJSON) || isEmptyJSON(responseJSON.version)){
                            dataResponse.code = 404;
                            dataResponse.data = [];
                        }else{
                            dataResponse.code = 200;
                            var list = responseJSON.version;
                            var arr = new Array();
                
                            list.map(item => {
                                arr.push({
                                    "id": item.id === null ? "" : item.id,
                                    "valor": item.descripcion === null ? "" : item.descripcion.trim()
                                });
                            });
                            dataResponse.data = arr;
                        }
                        LOGGER("INFO", "[User:"+userId+"] getVersiones: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(dataResponse.code).send(dataResponse); 
                    }
                }
            }, (err) => {
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getKilometrajeComplementos = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 
            || (data.idMarca === undefined || data.idMarca === "") || (data.idSubMarca === undefined || data.idSubMarca === "") 
            || (data.idModelo === undefined || data.idModelo === "") || (data.idVersion === undefined || data.idVersion === "") 
            || (data.idUser === undefined || data.idUser === "") || (data.token === undefined || data.token === "")
        ) {
            LOGGER("ERROR","[User:"+data.idUser+"] "+"getKilometrajeComplementos.ValidationError Need: idUser,idMarca, idSubMarca, idModelo, idVersion, idUser, token " + JSON.stringify(data), CONFIG.CATEGORY_LOGGER_QUOTE);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, idMarca, idSubMarca, idModelo, idVersion, idUser, token"});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CATALOG.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CATALOG.ID_CONSUMIDOR};
            var body = { "idMarca" : data.idMarca, "idSubMarca" : data.idSubMarca, "idModelo" : data.idModelo, "idVersion" : data.idVersion };
            
            LOGGER("INFO","[User:"+userId+"] "+"getKilometrajeComplementos: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_QUOTE);

            doRequestRest(CONFIG.SERVICE_CATALOG.PROTOCOL 
                , CONFIG.SERVICE_CATALOG.HOST
                , CONFIG.SERVICE_CATALOG.PORT
                , CONFIG.SERVICE_CATALOG.PATH_KILOMETRAJE_COMPLEMENTOS
                , CONFIG.SERVICE_CATALOG.GET, headers, body
                , (response) => {
               
                LOGGER("DEBUG","[User:"+userId+"] getKilometrajeComplementos.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_QUOTE);
                
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.code = 200;
                    dataResponse.data = [];
                    
                    return res.status(200).send(dataResponse); 
                } else {

                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                        LOGGER("ERROR","[User:"+userId+"] "+responseJSON, CONFIG.CATEGORY_LOGGER_QUOTE);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    } else {
                    
                        var dataResponse = new Object();
                        
                        if(isHTML(response)){
                            LOGGER("ERROR","[User:"+userId+"] "+response, CONFIG.CATEGORY_LOGGER_QUOTE);
                        
                            dataResponse.code = 500;
                            dataResponse.codigoError = 500;
                            dataResponse.descripcionError = "Datos Incorrectos";
                            dataResponse.data = response;
                        }else{
                            
                            if(isEmptyJSON(responseJSON)){
                                dataResponse.code = 404;
                                dataResponse.dataKil = [];
                                dataResponse.dataCom = [];
                            }else{
                                dataResponse.code = 200;

                                var listKil = responseJSON.kilometros ? responseJSON.kilometros.kilometraje : null;
                                var lisCom = responseJSON.complementos ? responseJSON.complementos.complemento : null;
                                var arrKil = new Array(), arrCom = new Array();
                    
                                listKil !== null && listKil.map(item => {
                                    arrKil.push({
                                        "id": item.id === null ? "" : item.id,
                                        "valor": item.descripcion === null ? "" : item.descripcion.trim(),
                                        "avaluo": item.avaluo === null ? "" : item.avaluo
                                    });
                                });
                                dataResponse.dataKil = arrKil;

                                lisCom !== null && lisCom.map(item => {
                                    arrCom.push({
                                        "id": item.id === null ? "" : item.id,
                                        "valor": item.descripcion === null ? "" : item.descripcion.trim()
                                    });
                                });
                                dataResponse.dataCom = arrCom;
                            } 
                        } 
                        LOGGER("INFO", "[User:"+userId+"] getKilometrajeComplementos: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(dataResponse.code).send(dataResponse);
                    }
                }
            }, (err) => {
                LOGGER("ERROR","[User:"+userId+"] "+JSON.stringify(err), CONFIG.CATEGORY_LOGGER_QUOTE);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getLocalidadPLD = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 || (data.idEstado === undefined || data.idEstado === "") ||
            (data.idUser === undefined || data.idUser === "") || (data.token === undefined || data.token === "") ) {
            
            LOGGER("ERROR", "[User:"+data.idUser+"] getLocalidadPLD.ValidationError: Need idUser, idEstado, token", CONFIG.CATEGORY_LOGGER_CATALOG);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: idUser, idEstado, token "});

        } else {

            var userId = data.idUser;
            var headers = { "Content-Type": "application/json", "Authorization": "Basic " + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'), "usuario": userId, "oauth.bearer": data.token, "idDestino": CONFIG.SERVICE_CATALOG.ID_DESTINO, "idConsumidor": CONFIG.SERVICE_CATALOG.ID_CONSUMIDOR};
            var body = { "estado" : data.idEstado };

            LOGGER("INFO","[User:"+userId+"] getLocalidadPLD.BODY: " + JSON.stringify(body), CONFIG.CATEGORY_LOGGER_CATALOG);
            
            doRequestRest(CONFIG.SERVICE_CATALOG.PROTOCOL 
                , CONFIG.SERVICE_CATALOG.HOST
                , CONFIG.SERVICE_CATALOG.PORT
                , CONFIG.SERVICE_CATALOG.PATH_LOCALIDAD_PLD
                , CONFIG.SERVICE_CATALOG.GET, headers, body
                , (response) => {
                
                LOGGER("DEBUG","[User:"+userId+"] getLocalidadPLD.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_CATALOG);
                
                if(isHTML(response)){
                
                    var dataResponse = new Object();
                    dataResponse.code = 500;
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                    
                    return res.status(500).send(dataResponse); 

                } else { 

                    var responseJSON = JSON.parse(response);
                    
                    if (responseJSON.codigoError) {
                        LOGGER("ERROR", response, CONFIG.CATEGORY_LOGGER_CATALOG);
                        responseJSON.code = 500;
                    
                        return res.status(500).send(responseJSON);
                    } else {
                        var dataResponse = new Object();

                        if(isEmptyJSON(responseJSON) || isEmptyJSON(responseJSON.catalogo)){
                            dataResponse.code = 404;
                            dataResponse.data = [];

                        }else{

                            dataResponse.code = 200;
                            var list = responseJSON.catalogo;
                            var arr = new Array();
                 
                            list.map(item => {
                                arr.push({
                                    "id": item.IdCatalogo === null ? "" : item.IdCatalogo,
                                    "valor": item.descripcionCatalogo === null ? "" : item.descripcionCatalogo.trim()
                                });
                            });
                            dataResponse.data = arr;   
                        }
                        LOGGER("INFO", "[User:"+userId+"] getLocalidadPLD: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(dataResponse.code).send(dataResponse); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR", err, CONFIG.CATEGORY_LOGGER_CATALOG);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    };

    getTemplate = (req, res) => {

        var data = req.body;
        
        if (Object.keys(data).length === 0  || (data.type === undefined || data.type === "")
            || (data.idUser === undefined || data.idUser === "") || (data.token === undefined || data.token === "") ) {
                
            LOGGER("ERROR","getTemplate.ValidationError Need: type, idUser", CONFIG.CATEGORY_LOGGER_CATALOG);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: type, idUser"});

        } else {

            var userId = data.idUser;

            var query = {
                type: {
                    $in: data.type
                }
            };
            
            LOGGER("INFO", "[User:"+userId+"]" + " getTemplate.QUERY: " + JSON.stringify(query),CONFIG.CATEGORY_LOGGER_CATALOG);
            Templates.find(query).select().exec((err, modulResponse) => {

                if (!err) {
                    
                    if (Object.keys(modulResponse).length === 0) {
                        return res.status(400).send({
                            code: 200,
                            data: ""
                        });

                    } else {
                        LOGGER("DEBUG", "[User:"+userId+"]" + " getTemplate.RESPONSE" + modulResponse[0], CONFIG.CATEGORY_LOGGER_CATALOG);
                        LOGGER("INFO", "[User:"+userId+"] getTemplate: Exitoso", CONFIG.CATEGORY_LOGGER_CATALOG);
                        return res.status(200).send({
                            code: 200,
                            data: modulResponse[0] != undefined ? modulResponse[0].template : ""
                        });
                    }
                } else {
                    LOGGER("ERROR", err, CONFIG.CATEGORY_LOGGER_CATALOG);
                    ERROR_HANDLER(err);
                }
            });
        }
    };

    //Link routes and functions
    router.post("/getMarcas", getMarcas);
    router.post("/getSubMarcas", getSubMarcas);
    router.post("/getModelos", getModelos);
    router.post("/getVersiones", getVersiones);
    router.post("/getKilometrajeComplementos", getKilometrajeComplementos);
    router.post("/getLocalidadPLD", getLocalidadPLD);
    router.post("/getTemplate", getTemplate);    
};
