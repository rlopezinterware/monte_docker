var parseString = require("xml2js").parseString;
var Roles = require("../models/roles.js");
var Permission = require("../models/permission.js");
var Features = require("../models/features.js");
var States = require("../models/states.js");
var Groups = require("../models/groups.js");

module.exports = (router) => {

    getUserToken = (req, res) => {
    
        var data = req.body;

        var responseAccess = new Object();

        if (Object.keys(data).length === 0 || (data.code === undefined || data.code === "")) {
            LOGGER("ERROR","userToken.ValidationError Need: code ", CONFIG.CATEGORY_LOGGER_USER);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: code "});

        } else {

            var userToken = new Promise(function(resolve, reject) {

                var respJSON = new Object();

                var headers = {
                    "Authorization": CONFIG.SERVICE_TOKEN_AUTHORIZATION.HEADER.AUTHORIZATION + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'),
                    "Content-Type": CONFIG.SERVICE_TOKEN_AUTHORIZATION.HEADER.CONTENT
                };

                var body = {
                    "grant_type": CONFIG.SERVICE_TOKEN_AUTHORIZATION.BODY.GRANT_TYPE_CLIENT,
                    "state": CONFIG.SERVICE_TOKEN_AUTHORIZATION.BODY.STATE,
                    "redirect_uri": CONFIG.SERVICE_TOKEN_AUTHORIZATION.BODY.REDIRECT_URI,
                    "code": data.code
                }
                          
                LOGGER("INFO", "getTokenAuthorization: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_USER);

                //To obtain the token
                doRequestRestURLEncoded(CONFIG.SERVICE_TOKEN_AUTHORIZATION.PROTOCOL
                    , CONFIG.SERVICE_TOKEN_AUTHORIZATION.HOST
                    , CONFIG.SERVICE_TOKEN_AUTHORIZATION.PORT
                    , CONFIG.SERVICE_TOKEN_AUTHORIZATION.PATH
                    , CONFIG.SERVICE_TOKEN_AUTHORIZATION.POST, headers , body 
                    , (response) => {

                        LOGGER("DEBUG","getTokenAuthorization.response: " + response, CONFIG.CATEGORY_LOGGER_USER);

                        respJSON = JSON.parse(response);
                        if(respJSON.error){
                            reject(respJSON);
                        }else{
                            responseAccess.access = respJSON;
                            LOGGER("INFO", "Authorization: Exitoso", CONFIG.CATEGORY_LOGGER_USER);
                            resolve(respJSON);
                        }
                    }, (err) => {
                        reject(err);
                    });
            });

            userToken.then(function successHandler(result) {

                var userProfile = new Promise(function(resolve, reject) {

                    var respJSON = new Object();

                    var headers = {
                        "Authorization": CONFIG.SERVICE_USER_PROFILE.HEADER.PROFILE + result.access_token,
                        "Content-Type": CONFIG.SERVICE_USER_PROFILE.HEADER.CONTENT
                    };

                    //To User Profile
                    doRequestRestURLEncoded(CONFIG.SERVICE_USER_PROFILE.PROTOCOL
                        , CONFIG.SERVICE_USER_PROFILE.HOST
                        , CONFIG.SERVICE_USER_PROFILE.PORT
                        , CONFIG.SERVICE_USER_PROFILE.PATH
                        , CONFIG.SERVICE_USER_PROFILE.GET, headers , {} 
                        , (response) => {
                            
                            LOGGER("DEBUG","getProfile.response: " + response, CONFIG.CATEGORY_LOGGER_USER);

                            respJSON = JSON.parse(response);
                            if(respJSON.error){
                                reject(respJSON);
                            }else{
                                responseAccess.profile = respJSON;
                                LOGGER("INFO", "Profile: Exitoso", CONFIG.CATEGORY_LOGGER_USER);
                                resolve(respJSON);
                            }
                        }, (err) => {
                            reject(err);
                        }
                    );
                });        

                userProfile.then(function successHandler(result) {
                
                    var headers = {
                        "Authorization": CONFIG.SERVICE_USER_ROLE.HEADER.PROFILE + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'),
                        "oauth.bearer": responseAccess.access.access_token,
                        "Content-Type": CONFIG.SERVICE_USER_ROLE.HEADER.CONTENT
                    };

                    var body = {
                        "usuario" : result.samaccountname,
                        "nombreAplicacion" : CONFIG.SERVICE_USER_ROLE.APP
                    };

                    LOGGER("INFO", "getUserRole: "+JSON.stringify(body), CONFIG.CATEGORY_LOGGER_USER);

                    doRequestRest(CONFIG.SERVICE_USER_ROLE.PROTOCOL 
                        , CONFIG.SERVICE_USER_ROLE.HOST
                        , CONFIG.SERVICE_USER_ROLE.PORT
                        , CONFIG.SERVICE_USER_ROLE.PATH
                        , CONFIG.SERVICE_USER_ROLE.POST, headers, body
                        , (response) => {
                            
                            LOGGER("DEBUG","getUserRole.response: " + response, CONFIG.CATEGORY_LOGGER_USER);
                            
                            if(isHTML(response)){
                                var dataResponse = new Object();
                                dataResponse.codigoError = 500;
                                dataResponse.descripcionError = "Datos Incorrectos";
                                dataResponse.data = response;
                                return res.status(500).send(dataResponse); 
                            } else {  

                                var responseJSON = JSON.parse(response);

                                if (responseJSON.codigoError) {
                                
                                    LOGGER("ERROR", responseJSON, CONFIG.CATEGORY_LOGGER_USER);
                                    responseJSON.code = 500;
                                    return res.status(500).send(responseJSON); 
                                
                                } else {

                                    LOGGER("INFO", "Roles: Exitoso", CONFIG.CATEGORY_LOGGER_USER);

                                    let groupArray = responseJSON.roles && responseJSON.roles.rol.map(role => {
                                        return role.nombre;
                                    });

                                    var query = { group: { $in: groupArray } };
                                    
                                    LOGGER("DEBUG", JSON.stringify(query), CONFIG.CATEGORY_LOGGER_USER);

                                    Groups.find(query).select().populate({path: 'rol_id', model: 'roles'}).exec((err, groupResponse) => {

                                        let arrayGroups = groupResponse.map( group => {
                                            return group.role_id[0];
                                        });

                                        query = { role_id: { $in: arrayGroups } };
                                        
                                        LOGGER("DEBUG", JSON.stringify(query), CONFIG.CATEGORY_LOGGER_USER);

                                        Permission.find(query).populate({path: 'feature_id', model: 'features'}).populate({path: 'state_id', model: 'states'}).exec(function(err, permissionResponse) {
                                            LOGGER("INFO", JSON.stringify(permissionResponse), CONFIG.CATEGORY_LOGGER_USER);
                                            
                                            responseAccess.roles = permissionResponse;
                                            return res.status(200).send(responseAccess);  
                                        });
                                    });
                                }
                           }
                    }, (err) => {
                        LOGGER("ERROR",JSON.stringify(err), CONFIG.CATEGORY_LOGGER_USER);
                        err.code = 500;
                        return res.status(500).send(err); 
                });

                }, function failureHandler(err) {
                    LOGGER("ERROR", JSON.stringify(err), CONFIG.CATEGORY_LOGGER_USER);
                    err.code = 500;
                    return res.status(500).send(err);
                });
            
            }, function failureHandler(err) {
                LOGGER("ERROR", JSON.stringify(err), CONFIG.CATEGORY_LOGGER_USER);
                err.code = 500;
                return res.status(500).send(err);
            });
        }
    }

    getRefreshToken = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 || (data.refreshToken === undefined || data.refreshToken === "")
        ) {
            LOGGER("ERROR","getRefreshToken.ValidationError Need: refreshToken ", CONFIG.CATEGORY_LOGGER_USER);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: refreshToken "});

        } else {

            var respJSON = new Object();
            var userId = data.idUser;

            var headers = {
                "Authorization": CONFIG.SERVICE_TOKEN_REFRESH.HEADER.AUTHORIZATION + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'),
                "Content-Type": CONFIG.SERVICE_TOKEN_REFRESH.HEADER.CONTENT
            };

            var body = {
                "grant_type": CONFIG.SERVICE_TOKEN_REFRESH.BODY.GRANT_TYPE_CLIENT,
                "refresh_token": data.refreshToken,
            }
            
            LOGGER("INFO","getRefreshToken.headers: " + headers, CONFIG.CATEGORY_LOGGER_USER);
            
            //To obtain the token
            doRequestRestURLEncoded(CONFIG.SERVICE_TOKEN_REFRESH.PROTOCOL
                , CONFIG.SERVICE_TOKEN_REFRESH.HOST
                , CONFIG.SERVICE_TOKEN_REFRESH.PORT
                , CONFIG.SERVICE_TOKEN_REFRESH.PATH
                , CONFIG.SERVICE_TOKEN_REFRESH.POST, headers , body 
                , (response) => {
                    
                LOGGER("DEBUG","getRefreshToken.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_USER);
                
                if(isHTML(response)){    
                    var dataResponse = new Object();
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                    return res.status(500).send(dataResponse); 
                } else {  
                    
                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                    
                        LOGGER("ERROR", responseJSON, CONFIG.CATEGORY_LOGGER_USER);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                        responseJSON.code = 200;
                        LOGGER("INFO", "RefreshToken: Exitoso", CONFIG.CATEGORY_LOGGER_USER);
                        return res.status(200).send(responseJSON); 
                    }
                }
            }, (err) => {
                LOGGER("ERROR",JSON.stringify(err), CONFIG.CATEGORY_LOGGER_USER);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    }
    
    getLogoutToken = (req, res) => {

        var data = req.body;

        if (Object.keys(data).length === 0 || (data.code === undefined || data.code === "")
        ) {
            LOGGER("ERROR","getLogoutToken.ValidationError Need: code ", CONFIG.CATEGORY_LOGGER_USER);
            return res.status(500).send({codigoError : "500", descripcionError : "ValidationError Need: code "});

        } else {

            var respJSON = new Object();
            var userId = data.idUser;

            var headers = {
                "Authorization": CONFIG.SERVICE_TOKEN_LOGOUT.HEADER.AUTHORIZATION + new Buffer(CONFIG.CLIENT_ID+":"+CONFIG.CLIENT_PASSWORD).toString('base64'),
                "Content-Type": CONFIG.SERVICE_TOKEN_LOGOUT.HEADER.CONTENT
            };

            var body = {
                "grant_type": CONFIG.SERVICE_TOKEN_LOGOUT.BODY.GRANT_TYPE_CLIENT,
                "oracle_token_action": CONFIG.SERVICE_TOKEN_LOGOUT.BODY.ACTION,
                "assertion" : data.code
            }
            
            LOGGER("INFO","getLogoutToken.headers: " + headers, CONFIG.CATEGORY_LOGGER_USER);
            
            //To obtain the token
            doRequestRestURLEncoded(CONFIG.SERVICE_TOKEN_LOGOUT.PROTOCOL
                , CONFIG.SERVICE_TOKEN_LOGOUT.HOST
                , CONFIG.SERVICE_TOKEN_LOGOUT.PORT
                , CONFIG.SERVICE_TOKEN_LOGOUT.PATH
                , CONFIG.SERVICE_TOKEN_LOGOUT.POST, headers , body 
                , (response) => {
                    
                LOGGER("DEBUG","getLogoutToken.RESPONSE: " + response, CONFIG.CATEGORY_LOGGER_USER);
                
                if(isHTML(response)){
                    var dataResponse = new Object();
                    dataResponse.codigoError = 500;
                    dataResponse.descripcionError = "Datos Incorrectos";
                    dataResponse.data = response;
                    return res.status(500).send(dataResponse); 
                } else {  
                    
                    var responseJSON = JSON.parse(response);

                    if (responseJSON.codigoError) {
                    
                        LOGGER("ERROR",responseJSON, CONFIG.CATEGORY_LOGGER_USER);
                        responseJSON.code = 500;
                        return res.status(500).send(responseJSON);
                    
                    } else {
                    
                        responseJSON.code = 200;
                        LOGGER("INFO", "LogoutToken: Exitoso", CONFIG.CATEGORY_LOGGER_USER);
                        return res.status(200).send(responseJSON); 
                    } 
                }
            }, (err) => {
                LOGGER("ERROR",JSON.stringify(err), CONFIG.CATEGORY_LOGGER_USER);
                err.code = 500;
                return res.status(500).send(err); 
            });
        }
    }

    //Link routes and functions
    router.post("/getUserToken", getUserToken);
    router.post("/getRefreshToken", getRefreshToken);
    router.post("/getLogoutToken", getLogoutToken);    
};
