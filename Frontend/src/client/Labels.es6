const Labels = {
    MANAGE_VALUER_TAB_TITLE: "Calcular préstamo",
    MANAGE_VALUER_TAB_CALF_CUSTOMER: "Calificación de cliente",

    LOADING: "Cargando",

    FOOTER_COPY_RIGHT: "Derechos Reservados La Paz, 2017",
    HEADER_LOG_OUT: "Cerrar sesión",

    FORM_INPUT_LABEL_PERCENTAGE_PENALTY: "Porcentaje de castigo",
    FORM_TEXTAREA_LABEL_COMMENTS: "Comentarios",
    FORM_SUBMIT_LABEL_QUOTE: "Cotizar",
    FORM_RESULT_LABEL: "Préstamo"
};
export default Labels;

export const SETTINGS = {
    ENABLE_REQUEST_FINGERPRINT: "Solicitar huella",
    DISABLE_REQUEST_FINGERPRINT: "Ya no solicitar huella",
};

export const FORM_NUM_CREDENTIAL_USER = {
    TITLE: "No. de credencial",
    VALUE_SUBMIT_BTN: "Buscar",
    SELECT_LIST_PROCEDURES: "Trámites"
};

export const ASR = {
    MENU_FILTER: {
        TITLE: "Auto sin resguardo",
        ITEM_0: "Nueva solicitud",
        ITEM_1: "Ofertas en validación"
    },
    FORMS: {
        CUSTOMER_DATA: {
            TITLE: "Datos del cliente",
            FIRST_NAME: "Nombre",
            LAST_NAME: "Apellido Paterno",
            SUR_NAME: "Apellido Materno",
            CALLE: "Calle",
            NUMERO_EXTERIOR: "No. Exterior",
            NUMERO_INTERIOR: "No. Interior",
            COLONIA: "Colonia",
            MUNICIPIO: "Municipio",
            ESTADO: "Estado",
            CP: "Código Postal",
            TIPO_DOMICILIO: "Tipo de domicilio",
            RFC: "RFC",
            GENERO: "Género",
            NO_IDENTIFICACION: "No. de identificación",
            TIPO_IDENTIFICACION: "Tipo de identificación",
            OCUPACION: "Ocupación"
        },
        AUTO: {
            TITLE: "Datos del auto",
            NUMERO_MOTOR: "No. de serie del motor",
            NUMERO_CHASIS: "No. de serie del chasis",
            COLOR: "Color",
            PLACAS: "Placas",
            EQUIPO_ADICIONAL: "Equipo adicional",
            MARCA: "Marca",
            SUB_MARCA: "Sub marca",
            MODELO: "Modelo",
            VERSION: "Versión",
            KILOMETRAJE: "Kilometraje"
        },
        CREDITO: {
            PRESTAMO: 'Préstamo solicitado',
            MONTO_MINIMO: 'Monto mínimo',
            SUBPRODUCTO: 'Sub producto',
            PLAZO: 'Plazo',
            TASA: 'Tasa de interés'
        }
    }
};