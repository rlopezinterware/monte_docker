export default {
    showMenu: false,
    alertConfig: {},
    navItems: [],
    isLoading: false,
    customerData: {},
    autoData: {},
    marcas: [],
    submarcas: [],
    modelos: [],
    versiones: [],
    complementos: [],
    kilometrajes: [],
    localidadpld: [],
    equipo: [],
    auto: {
        marca: 0,
        submarca: 0,
        modelo: 0,
        version: 0,
        kilometraje: 0,
        equipoAdicional: ''
    },
    tablaAmortizacion: [],
    header: true,
    plazos: [],
    credito: { montoMinimo: 0, montoMaximo: 0, producto: ''},
    prestamo: {},
    solicitud: {},
    motivosRechazo: [],
    motivoSeleccionado: '',
    errorMsg:''
};
