import ClientHttpRequest from "./ClientHttpRequest";

export const existsTokens = () => {
    return !(sessionStorage.uid === undefined ||
    sessionStorage.mail === undefined ||
    sessionStorage.lastname === undefined ||
    sessionStorage.firstname === undefined ||
    sessionStorage.access_token === undefined ||
    sessionStorage.features === undefined);
};

