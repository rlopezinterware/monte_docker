import Axios from "axios";
import Constants, * as types from "./../constants";
import {TIEMPO_SESION} from "./../constants";

export default class ClientHttpRequest {

    constructor(config) {

        this.config = config;

        this.instanceAxios = Axios.create({
            baseURL: config.url,
            timeout: 45000,
            url: config.endpoint,
            responseType: "json",
            headers: {
                "Content-Type": "application/json"
            }
        });
    }

    request() {

        let _this = this;

        return new Promise((resolve, reject) => {
           
            _this.instanceAxios.request(_this.config)
            .then(response => {
                sessionStorage.session_time = Number(new Date()) - TIEMPO_SESION.ALERTA + TIEMPO_SESION.INACTIVIDAD;
                resolve(response);
            })
            .catch(error => { 

                let __this = this;

                if (error.response && error.response.data.codigoError === Constants.CODE_REFRESH) { // Refresh and Recall Service
                
                    __this.refreshToken = {
                        "url": Constants.BASE_URL_SEC + Constants.END_POINT_GET_USER_REFRESH,
                        "method":"POST",
                        "data": {
                            "idUser": sessionStorage.uid,
                            "refreshToken":sessionStorage.refresh_token
                        }
                    };

                    __this.refreshTokenAxios = Axios.create({
                        baseURL: __this.refreshToken.url,
                        timeout: 45000,
                        url: __this.refreshToken.endpoint,
                        responseType: "json",
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });

                    __this.refreshTokenAxios.request(__this.refreshToken) // Refresh token
                    .then(response => { 

                        sessionStorage.access_token = response.data.access_token;
                        sessionStorage.refresh_token = response.data.refresh_token;

                        _this.config.headers.Authorization = response.data.access_token;

                        _this.instanceAxios.request(_this.config) // Recall Service New Token
                        .then(response => {
                            resolve(response); 
                        })
                        .catch(error => {
                            reject(error);
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
                } else if (error.response && error.response.data.codigoError === Constants.CODE_SESSION_OUT) { // Session Out Service

                    reject(error);

                } else if (error.response) { // Error Service
                    
                    reject(error);

                } else { // Error Connect
                    reject({response: {data: {descripcionError: "Error de conexión, intente de nuevo"}} });
                }
            });
        });
    }
}
