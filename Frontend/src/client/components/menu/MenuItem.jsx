import React from "react";

const MenuItem = ({data, navigate}) => {
    return (
        <div
            className={"menu-item " + (data.iconClass & "menu-start") }
            onClick={ () => {
                navigate(data.uri)
            } }>
            <i className={data.iconClass}/>
            {data.description}
            <span className="badge danger">{data.valueBadges}</span>
        </div>
    );
};

MenuItem.propTypes = {
    data: React.PropTypes.object.isRequired,
    navigate: React.PropTypes.func.isRequired
};

export default MenuItem;
