import * as types from "../../constants";

const toggleMenu = (flag) => {
    return {type: flag ? types.ACTION_SHOW_MENU : types.ACTION_HIDE_MENU, flag};
};

export const showMenu = () => {
    return (dispatch) => {
        dispatch(toggleMenu(true));
    };
};

export const hideMenu = () => {
    return (dispatch) => {
        dispatch(toggleMenu(false));
    };
};
