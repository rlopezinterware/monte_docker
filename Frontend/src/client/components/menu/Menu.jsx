import * as MenuActions from "./MenuActions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {browserHistory} from "react-router";
import MenuItem from "./MenuItem";
import React, {PropTypes} from "react";

class Menu extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.navigate = this.navigate.bind(this);
        this.clickToHideMenu = this.clickToHideMenu.bind(this);
    }

    clickToHideMenu() {
        this.props.actions.hideMenu();
    }

    navigate(hash) {
        browserHistory.push(hash);
    }

    render() {
        const createMenuItem = (menuItem) => {
            return (
                <MenuItem key={menuItem.id} data={menuItem} navigate={this.navigate}/>
            );
        };
        const flag = this.props.menu;
        return (
            <div
                className={"background-menu" + (flag ? " show-menu" : " hide-menu") }
                ref="back"
                onClick={this.clickToHideMenu}>
                <div className="menu">
                    <div className={(flag ? "visible " : "novisible ") + this.props.alignment}>
                        {this.props.data.map(createMenuItem, this) }
                    </div>
                </div>
            </div>
        );
    }
}

Menu.propTypes = {
    data: React.PropTypes.array.isRequired,
    menu: React.PropTypes.bool,
    actions: React.PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        menu: state.menu
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(MenuActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);