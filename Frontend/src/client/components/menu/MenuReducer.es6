import * as types from "../../constants";
import InitialData from "../../InitialData";

const menuReducer = (state = InitialData.showMenu, action) => {
    switch (action.type) {
        case types.ACTION_SHOW_MENU:
            return action.flag;

        case types.ACTION_HIDE_MENU:
            return action.flag;

        default:
            return state;
    }
};

export default menuReducer;
