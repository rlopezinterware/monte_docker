import {Row, Col, Table, Pagination, Button} from "react-bootstrap";
import numeral from "numeral";
import React from "react";
import {connect} from "react-redux";
import {setMotivoRechazoSeleccionado} from "./ModalRejectActions";

class ModalReject extends React.Component {
	constructor(props) {
		super(props);
		this.setMotivo = this.setMotivo.bind(this);
	}

	setMotivo(e) {
		this.props.setMotivoRechazoSeleccionado(e.target.value);
	}

	componentWillMount(){
		this.props.setMotivoRechazoSeleccionado('');
	}

	render() {
		let motivos = this.props.listaMotivos.map(i => {
			if (i.etapa === this.props.etapa) {
				return (
					<div className="radio" key={i.id}>
						<label><input type="radio" name="a" value={i.id} onClick={this.setMotivo}/> {i.descripcion}</label>
					</div>
				);
			} else return;
		});
		return (
			<Row>
				<Col xs={12}>
					<h3 className={'text-center'}>Motivos de rechazo</h3>
					{motivos}
				</Col>
			</Row>
		);
	};
}

const mapStateToProps = (state, ownProps) => {
	return {
		listaMotivos: state.motivosRechazo,
		etapa: ownProps.etapa
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		setMotivoRechazoSeleccionado: (motivo)=>dispatch(setMotivoRechazoSeleccionado(motivo))
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(ModalReject);