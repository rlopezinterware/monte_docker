import * as types from "../../constants";
import InitialData from "../../InitialData";

const modalRejectReducer = (state = InitialData.motivoSeleccionado, action) => {
    switch (action.type) {
        case types.ACTION_SET_MOTIVO_RECHAZO:
            return action.motivoRechazo;
        default:
            return state;
    }
};

export default modalRejectReducer;