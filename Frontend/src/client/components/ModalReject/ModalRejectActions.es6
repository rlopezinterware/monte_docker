import * as types from "../../constants";

export const setMotivoRechazoSeleccionado = (motivoRechazo) => {
    return (dispatch) => {
        dispatch({type: types.ACTION_SET_MOTIVO_RECHAZO,motivoRechazo});
    }
}