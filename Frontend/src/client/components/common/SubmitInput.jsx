import React, {PropTypes} from "react";

const SubmitInput = ({active, classCSSActive, classCSSNoActive, onSubmit, value}) => {
    return (
        <input
            type="submit"
            className={!active ? (classCSSActive ? classCSSActive : "form-btn-main") : (classCSSNoActive ? classCSSNoActive : "form-btn")}
            value={value}
            disabled={active}
            onClick={onSubmit}/>
    );
};

SubmitInput.propTypes = {
    value: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
    classCSSActive: PropTypes.string,
    classCSSNoActive: PropTypes.string,
    active: PropTypes.bool.isRequired
};

export default SubmitInput;
