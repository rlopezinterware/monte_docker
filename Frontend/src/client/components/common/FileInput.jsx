import React, {PropTypes} from "react";
import {MIMETYPES,ICONS} from  "../../constants";

const FileInput = ({disabled, label, name, onChange, preview, type, required}) => {
    const getPreview = () => {
        if (type){
            switch (type) {
                case MIMETYPES.JPG:
                case MIMETYPES.PNG:
                    return <img src={preview} className="previewImageInput"/>;
                    break;
                case MIMETYPES.PDF:
                    return <i className={ICONS.PDF}/>;
                    break;
                case MIMETYPES.XML:
                    return <i className={ICONS.XML}/>;
                    break;
                default:
                    return <i className={ICONS.DEFAULT}/>;
                    break;
            }
        }else{
            return <i className={ICONS.IMG}/>;
        }
    };

    return (
        <div className="col-lg-6 col-md-6 col-sm-6 form-area file-container">
            {required && <i className={'fa fa-circle requerido'}/>}
            <div className="uploadfile">
                {getPreview()}
                <br/>
                <label htmlFor={"file-" + label} className="flat-btn">
                    Selecciona
                </label>
                <input
                    id={"file-" + label}
                    type="file"
                    name={name}
                    onChange={onChange}
                    disabled={disabled}
                    autoComplete="off"
                    className="flat-btn"/>
            </div>
        </div>
    );
};

FileInput.propTypes = {};

export default FileInput;
