import React, {PropTypes} from "react";
	
import {Row, Col} from "react-bootstrap";

const RadioGroupInput = ({error, name, label, subLabel, onChange, required, value, value1, option1, value2, option2}) => {

		let wrapperClass = "form-group";
		if (error && required && (value===undefined)) {
			wrapperClass += " has-error";
		}

		let handleChange = (e) => {
			onChange(e);
		}

		return <div className={wrapperClass}>
			<label className="control-label" htmlFor={name}>
					{label} {subLabel && <span className="small text-muted" >{subLabel}</span>}
					{required && <i className={'fa fa-circle requerido'}/>}
			</label>
			<Row className="radio-group">
				<Col xs={6}>
					<label><input 
								type="radio" 
								name={name} 
								onChange={handleChange} 
								value={value1} 
								checked={value===undefined ? false : value === value1 ? true : false} />{option1}</label>
				</Col>
				<Col xs={6}>
					<label><input 
								type="radio" 
								name={name} 
								onChange={handleChange} 
								value={value2} 
								checked={value===undefined ? false : value === value2 ? true : false} />{option2}</label>
				</Col>
			</Row>
			{error && required && (value===undefined) && <p>{'Este campo es requerido'}</p>}
		</div>;
};

RadioGroupInput.propTypes = {
		name: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired,
		onChange: PropTypes.func,
		error: PropTypes.bool
};

export default RadioGroupInput;
