import React, {PropTypes} from "react";

const TextareaInput = ({error, label, name, onChange, readOnly, required, value, maxLength, rows}) => {

  let wrapperClass = "form-group ";
  if (error && required && !value) {
    wrapperClass += " has-error";
  }

  let handleChange = (e) => {
      let selTemp = e.target.selectionStart;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = selTemp;
      e.target.selectionEnd = selTemp;
      onChange(e);
  }

  return (
    <div className={wrapperClass}>
      <label className="control-label"  htmlFor={name}>
        {label}
        {required && <i className={'fa fa-circle requerido'}/>}
      </label>
      <textarea
        name={name}
        value={value}
        onChange={handleChange}
        readOnly={readOnly}
        maxLength={maxLength}
        autoComplete="off"
        rows={rows}
        className="form-control">
      </textarea>
      {(error && required && !value) && <p>{'Este campo es requerido'}</p>}
    </div>
  );
};

TextareaInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.bool,
  required: PropTypes.bool,
  readOnly: PropTypes.bool
};

export default TextareaInput;
