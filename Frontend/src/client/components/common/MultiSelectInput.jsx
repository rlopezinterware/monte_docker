import React, {PropTypes} from "react";
import SelectAuto from "react-select";
import "react-select/dist/react-select.css";

const MultiSelectInput = ({name, disabled, error, label, onChange, options, required, readOnly, value, autoFocus}) => {

	
    let wrapperClass = "form-group ";

    if (error && required && !value) {
        wrapperClass += " has-error";
    }

    let searchable = true,
        clearable  = false;

    let data = [];

    options.map(option => {
		data.push({value: String(option.id), label: option.valor});
    });
    
    return (
		<div className={wrapperClass}>
            <label className="control-label" htmlFor={name}>
                {label}
                {required && <i className={'fa fa-circle requerido'}/>}
            </label>
                
            <SelectAuto 
				multi 
				autofocus={autoFocus}
                clearable={clearable}
                disabled={disabled}
                noResultsText="No existe la opción"
                name={name}
                onChange={onChange}
                options={data}
                placeholder="Seleccione una opción"
                searchable={searchable}
                simpleValue
				readOnly={readOnly}
                value={value} />
            {error && required && !value && <p>{'Este campo es requerido'}</p>}
		</div>
    );
};

MultiSelectInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.object),
    required: PropTypes.bool,
    disabled: PropTypes.bool
};

export default MultiSelectInput;
