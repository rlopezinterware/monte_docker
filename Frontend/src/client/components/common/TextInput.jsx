import React, {PropTypes} from "react";

const TextInput = ({error, name, label, subLabel, onChange, placeholder, readOnly, required, value, disabled, maxLength, alpha, numeric, alphanum}) => {

		let wrapperClass = "form-group";
		if (error && required && (!value)) {
				wrapperClass += " has-error";
		}

		let handleChange = (e) => {

				e.preventDefault();
				let selTemp = e.target.selectionStart;

				let ival = e.target.value.replace(/[!*$%&()=",+_{}´¨~¿?'¡!¬^`;:<>@°“”µ½·¸¢«»æłß€¶ŧ←↓→øþłĸŋđð\\\|\/\[\]]/g,'');
				if (numeric) {
					ival = ival.replace(/[a-zA-ZáéíóúÁÉÍÓÚÄËÏÖÜäëïöüñÑ#·\ \.\-]/g,'');
				} else if (alpha) {
					ival = ival.replace(/[0-9#\.\-]/g,'');
				} else if (alphanum) {
					ival = ival.replace(/[#\ \.\-]/g,'');
				}
				e.target.value = ival.toUpperCase();
				e.target.selectionStart = selTemp;
				e.target.selectionEnd = selTemp;
				onChange(e);
		}

		return <div className={wrapperClass}>
				<label className="control-label" htmlFor={name}>
						{label} {subLabel && <span className="small text-muted" >{subLabel}</span>}
						{required && <i className={'fa fa-circle requerido'}/>}
				</label>
				
				<input
						type="text"
						name={name}
						className="form-control"
						placeholder={placeholder}
						value={value}
						readOnly={readOnly}
						maxLength={maxLength}
						autoComplete="off"
						disabled={disabled}
						onChange={handleChange}/>
						{error && required && (!value) && <p>{'Este campo es requerido'}</p>}
		</div>;
};

TextInput.propTypes = {
		name: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired,
		onChange: PropTypes.func,
		placeholder: PropTypes.string,
		error: PropTypes.bool,
		required: PropTypes.bool,
		readOnly: PropTypes.bool,
		alpha: PropTypes.bool,
		numeric: PropTypes.bool
};

export default TextInput;
