module.exports = [
	{id: '01', valor: 'NMP'},
	{id: '02', valor: 'Agricultura, ganadería, aprovechamiento forestal, pesca y caza'},
	{id: '03', valor: 'Minería'},
	{id: '04', valor: 'Industrias manufactureras'},
	{id: '05', valor: 'Construcción'},
	{id: '06', valor: 'Electricidad, agua y suministro de gas por ductos al consumidor final'},
	{id: '07', valor: 'Comercio al por menor'},
	{id: '08', valor: 'Transportes, correos y almacenamiento'},
	{id: '09', valor: 'Información en medios masivos'},
	{id: '10', valor: 'Servicios financieros y de seguros'},
	{id: '11', valor: 'Servicios inmobiliarios y de alquiler de bienes muebles e intangibles'},
	{id: '12', valor: 'Servicios profesionales, científicos y técnicos'},
	{id: '13', valor: 'Servicios de esparcimiento culturales y deportivos, y otros servicios recreativos'},
	{id: '14', valor: 'Otros servicios excepto actividades del gobierno'},
	{id: '15', valor: 'Servicios educativos'},
	{id: '16', valor: 'Servicios de salud y de asistencia social'},
	{id: '17', valor: 'Actividades del gobierno y de organismos internacionales y extraterritoriales'}
]
