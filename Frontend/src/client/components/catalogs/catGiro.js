module.exports = {
	'01': [
		{id: '1', valor: 'Jefes de estado o de gobierno'},
		{id: '2', valor: 'Líderes políticos'},
		{id: '3', valor: 'Funcionarios gubernamentales'},
		{id: '4', valor: 'Jefe ó empleado de la policía judicial'},
		{id: '5', valor: 'Secretaría de la defensa nacional (militar)'},
		{id: '6', valor: 'Funcionarios de partidos políticos'},
		{id: '7', valor: 'Abogados o asociados de abogados'},
		{id: '8', valor: 'Ministros de culto religioso (sacerdote, pastor, monja)'},
		{id: '9', valor: 'Policías'},
		{id: '10', valor: 'Gestores de cobranza'}
	],
	'02': [
		{id: '100008', valor: 'Agricultura'},
		{id: '200006', valor: 'Ganadería'},
		{id: '300004', valor: 'Silvicultura'},
		{id: '400002', valor: 'Pesca'},
		{id: '500000', valor: 'Caza'}
	],
	'03': [
		{id: '1100007', valor: 'Extracción y beneficio de carbón mineral y grafito'},
		{id: '1200005', valor: 'Extracción de petróleo crudo y gas natural'},
		{id: '1300003', valor: 'Extracción y beneficio de minerales metálicos'},
		{id: '1400001', valor: 'Extracción de minerales no metálicos, excepto sal'},
		{id: '1500009', valor: 'Explotación de sal'}
	],
	'04': [
		{id: '2000008', valor: 'Fabricación de alimentos'},
		{id: '2100006', valor: 'Fabricación y elaboración de bebidas (agua, refrescos, cerveza, vinos y licores)'},
		{id: '2200004', valor: 'Fabricación y beneficio de productos de tabaco'},
		{id: '2300002', valor: 'Industria textil (fabricación de: hilados y tejidos)'},
		{id: '2400000', valor: 'Fabricación de prendas de vestir y otros artículos confeccionados con textiles y otros materiales excepto calzado'},
		{id: '2500008', valor: 'Fabricación de calzado e industria del cuero'},
		{id: '2600006', valor: 'Industria y productos de madera y corcho; excepto muebles'},
		{id: '2700004', valor: 'Fabricación y reparación de muebles y accesorios; excepto los de metal y los de plástico moldeado'},
		{id: '2800002', valor: 'Industria del papel'},
		{id: '2900000', valor: 'Industrias editorial, de impresión y conexas'},
		{id: '3000007', valor: 'Industria química'},
		{id: '3100005', valor: 'Refinación de petróleo y derivados del carbón mineral'},
		{id: '3200003', valor: 'Fabricación de productos de hule y de plástico'},
		{id: '3300001', valor: 'Fabricación de productos de minerales no metálicos; excepto del petróleo y del carbón mineral'},
		{id: '3400009', valor: 'Industrias metálicas básicas'},
		{id: '3500007', valor: 'Fabricación de productos metálicos; excepto maquinaria y equipo'},
		{id: '3600005', valor: 'Fabricación, ensamble y reparación de maquinaria, equipo y sus partes; excepto los eléctricos'},
		{id: '3700003', valor: 'Fabricación y ensamble de maquinaria, equipo, aparatos, accesorios y artículos eléctricos, electrónicos y sus partes'},
		{id: '3800001', valor: 'Construcción, reconstrucción y ensamble de equipo de transporte y sus partes'},
		{id: '3900001', valor: 'Fabricación de artículos de joyería'},
		{id: '3900002', valor: 'Tallado de piedras preciosas'},
		{id: '3900009', valor: 'Otras industrias manufactureras (joyeria, articulos de escritorio, fotograficos, para dentista, para optica, deportivos)'},
		{id: '6800003', valor: 'Servicios de blindaje'}
	],
	'05': [
		{id: '4100004', valor: 'Contratación de obras completas de construcción (casas, departamentos, inmuebles, pavimentación, no residenciales, vias de comunicación)'},
		{id: '4200002', valor: 'Servicios especiales prestados por subcontratistas (demoliciones, carpinteria, impermiabilización, inst eslectricas, pintura, plomeria)'}
	],
	'06': [
		{id: '5000005', valor: 'Generación, transmisión y distribución de energía eléctrica'},
		{id: '5100003', valor: 'Captación, tratamiento, conducción y distribución de agua potable excepto de riego'}
	],
	'07': [
		{id: '6100002', valor: 'Compraventa de alimentos, bebidas y productos de tabaco'},
		{id: '6200000', valor: 'Compraventa de prendas de vestir y otros artículos de uso personal'},
		{id: '6300008', valor: 'Compraventa de artículos para el hogar (electrodomesticos, refacciones, loza y porcelana, antiguedades)'},
		{id: '6400006', valor: 'Compraventa en tiendas de autoservicio y de departamentos especializados por línea de mercancías'},
		{id: '6500004', valor: 'Compraventa de gases, combustibles y lubricantes'},
		{id: '6600002', valor: 'Compraventa de materias primas, materiales y auxiliares (algodón, cemento, sanitarios, pieles, ferreteria, madera, pinturas)'},
		{id: '6700000', valor: 'Compraventa de maquinaria, equipo, instrumentos, aparatos y herramientas, sus refacciones y accesorios'},
		{id: '6800001', valor: 'Venta de vehículos, embarcaciones ó aeronaves  nuevos'},
		{id: '6800002', valor: 'Venta de vehículos, embarcaciones ó aeronaves  usados'},
		{id: '6800008', valor: 'Compraventa de equipo de transporte, sus refacciones y accesorios'},
		{id: '6900002', valor: 'Compraventa de diamantes'},
		{id: '6900003', valor: 'Tarjeta de crédito y comercialización de tarjetas de servicios'},
		{id: '6900006', valor: 'Compraventa de bienes inmuebles y artículos diversos'},
		{id: '8700008', valor: 'Preparación y servicio de alimentos y bebidas'}
	],
	'08': [
		{id: '7100001', valor: 'Transporte terrestre'},
		{id: '7200009', valor: 'Transporte por agua'},
		{id: '7300007', valor: 'Transporte aéreo'},
		{id: '7400005', valor: 'Servicios conexos al transporte'},
		{id: '7500003', valor: 'Servicios relacionados con el transporte en general'}
	],
	'09': [{id: '7600001', valor: 'Comunicaciones'}],
	'10': [
		{id: '8100000', valor: 'Servicios de instituciones de crédito, organizaciones auxiliares e instituciones de seguros y fianzas'},
		{id: '8200001', valor: 'Montepío ó casas de empeño'},
		{id: '8200008', valor: 'Servicios colaterales a instituciones financieras y de seguros'}
	],
	'11': [
		{id: '8300006', valor: 'Servicios relacionados con inmuebles'},
		{id: '8500002', valor: 'Servicios de alquiler a empresas'},
		{id: '8600000', valor: 'Servicios de alojamiento temporal'}
	],
	'12': [
		{id: '8400001', valor: 'Contaduría y auditoría'},
		{id: '8400002', valor: 'Asesores  fiscales y financieros'},
		{id: '8400003', valor: 'Notarías públicas'},
		{id: '8400004', valor: 'Servicios profesionales y técnicos'}
	],
	'13': [
		{id: '8800001', valor: 'Hipódromo'},
		{id: '8800002', valor: 'Juegos de feria y apuestas'},
		{id: '8800003', valor: 'Agencias de rifas y sorteos (quinielas y lotería)'},
		{id: '8800006', valor: 'Servicios recreativos y de esparcimiento'}
	],
	'14': [
		{id: '8900004', valor: 'Servicios personales, para el hogar y diversos'},
		{id: '9300001', valor: 'Organizaciones de abogados'},
		{id: '9300005', valor: 'Agrupaciones mercantiles, profesionales, cívicas, políticas, laborales y religiosas'}
	],
	'15': [{id: '9100009', valor: 'Servicios de enseñanza, investigación científica y difusión cultural'}],
	'16': [{id: '9200007', valor: 'Servicios médicos, de asistencia social y veterinarios'}],
	'17': [
		{id: '9400003', valor: 'Servicios de administración pública, defensa y seguridad social'},
		{id: '9800000', valor: 'Empleado del sector publico'},
		{id: '9900003', valor: 'Servicios de organizaciones internacionales y otros organismos extraterritoriales'}
	]
}