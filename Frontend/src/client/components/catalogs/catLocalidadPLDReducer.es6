import * as types from "../../constants";
import InitialData from "../../InitialData";

const catLocalidadPLDReducer = (state = InitialData.localidadpld, action) => {
    switch (action.type) {
        case types.ACTION_GET_LOCALIDAD_PLD:
            return action.localidadpld;
        default:
            return state;
    }
};

export default catLocalidadPLDReducer;
