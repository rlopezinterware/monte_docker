module.exports = [
	{id:  0, valor: 'Ninguno'},
	{id:  1, valor: 'Hermano/a'},
	{id:  2, valor: 'Suegro/a'},
	{id:  3, valor: 'Tio/a'},
	{id:  4, valor: 'Primo/a'},
	{id:  5, valor: 'Hijo/a'},
	{id:  6, valor: 'Papa'},
	{id:  7, valor: 'Mama'},
	{id:  8, valor: 'Cuñado/a'},
	{id:  9, valor: 'Abuelo/a'},
	{id: 10, valor: 'Esposo/a'},
	{id: 11, valor: 'Sobrino/a'},
	{id: 12, valor: 'Vecino/a'},
	{id: 13, valor: 'Cuncubino/a'},
	{id: 14, valor: 'Novio/a'},
	{id: 15, valor: 'Amigo/a'},
	{id: 16, valor: 'Compañero de Trabajo'},
	{id: 17, valor: 'Otros'}
]

