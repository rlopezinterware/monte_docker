import * as types from "../../constants";
import InitialData from "../../InitialData";

const navMainReducer = (state = InitialData.navItems, action) => {
	switch (action.type) {
		case types.ACTION_CHANGE_ACTIVE_TAB:
			let tabs=[];
			tabs = state.map((i,x)=>{
					return {...i,active: (i.name == action.id) };
			});
			return tabs;
		case types.ACTION_LOAD_TABS:
			return action.tabs;
			break;
		default:
			return state;
	}
};

export default navMainReducer;
