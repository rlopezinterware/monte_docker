import * as types from "../../constants";

export const setActiveTab = (id) => {
    return (dispatch) => {
        dispatch({ type: types.ACTION_CHANGE_ACTIVE_TAB, id });
    };
};

export const loadTabs = (tabs) =>{
    return (dispatch) => {
        dispatch({ type: types.ACTION_LOAD_TABS, tabs });
    };
};
