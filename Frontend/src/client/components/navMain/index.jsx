import {Grid, Row, Col, Nav, NavItem, Tabs, Tab} from "react-bootstrap";
import React, {PropTypes} from "react";
import {connect} from "react-redux";

class navMain extends React.Component {
	
	constructor(props, context) {
		super(props, context);
		this.mapItems = this.mapItems.bind(this)
	}

	mapItems(item,index) {
		let activeClass = item.active?'active':'';
		let element;

		if (item.href) {
			element = <a href="#" onClick={(e)=>{e.preventDefault();this.context.router.push(item.href)}}><span>{item.name}</span></a>
		} else {
			element = <a><span>{item.name}</span></a>
		}

		return (
			<li key={index} className={activeClass}>{element}</li>);
	}

	render() {
		let items = this.props.navItems.map(this.mapItems);
		return (
			<Row className="navMain">
				<Grid>
					<Row>
						<Col sm={12}>
							<ul role="tablist" className="nav nav-tabs">
								{items}
							</ul>
						</Col>
					</Row>
				</Grid>
			</Row>
		);
	}
};

navMain.contextTypes = {
	router: PropTypes.object
}

const mapStateToProps = (state, ownProps) => {
	return {
		navItems: state.navItems
	};
}

export default connect(mapStateToProps)(navMain);