import * as AlertActions from "./AlertActions.es6";
import {bindActionCreators} from 'redux';
import {browserHistory} from "react-router";
import {connect} from 'react-redux';
import React, {PropTypes} from 'react';

class Alert extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.finishCountdown = this.finishCountdown.bind(this);
		this.handleClickBackground = this.handleClickBackground.bind(this);
		this.handleClickButton = this.handleClickButton.bind(this);
		this.handleClickIconClose = this.handleClickIconClose.bind(this);
	}

	componentWillMount() {
		this.timeouts = [];
	}

	componentWillUnmount() {
		this.clearTimeOuts();
	}

	clearTimeOuts() {
		this.timeouts.forEach(clearTimeout);
	}

	finishCountdown() {
		this.props.actions.hideAlert();
	}

	handleClickBackground() {
		if (this.props.alert.hideWithBackground) {
			this.hideAlert();
		}
	}

	handleClickButton(index) {
		
		if (typeof this.props.alert.buttons[index].onClick === "function") {
			let result = this.props.alert.buttons[index].onClick();
			
			if (result === undefined || result){
				this.props.actions.hideAlert();	
			}
		} else {
			this.props.actions.hideAlert();
		}
	}

	handleClickIconClose() {
		this.props.actions.hideAlert();
	}

	hideAlert() {
		this.props.actions.hideAlert();
		this.clearTimeOuts();
	}

	setTimeOut() {
		this.timeouts.push(setTimeout.apply(null, arguments));
	}

	startCountdown() {
		if (this.props.alert.count !== 0) {
			this.setTimeOut(this.finishCountdown, this.props.alert.count);
		}
	}

	render() {
		let alert = this.props.alert;
		if (typeof alert === 'object' && alert.count) {
			this.startCountdown();
		}
		return (
			<div className={"alert-npm " + (alert ? "alert-show" : "alert-hide") }>
				<div className={"alert-npm alert-background " +
					(alert ? "animated fadeIn" : "animated fadeOut") }
					onClick={this.handleClickBackground} >
					<div className={"alert-npm alert-container-message " +
						(alert ? "animated bounceIn" : "animated bounceOut") } >

						<div className="alert-header text-right">
							{this.props.alert.closeBtn && <i className="fa fa-times-circle alert-icon-close" onClick={this.handleClickIconClose} />}
						</div>

						<div className="alert-body">
							{alert.message}
						</div>

						<div className={(alert.buttons && alert.buttons.length) ? "alert-footer text-center" : ""}>
							{
								alert.buttons && Object.keys(alert.buttons).map((index) => {
									return <button
										key={index + "--" + index}
										className={"btn " + alert.buttons[index].classCss}
										onClick={() => this.handleClickButton(index) }>
										{alert.buttons[index].label}
									</button>;
								})
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

Alert.propTypes = {
	actions: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) =>{
	if (state.alert === false) {
		return {
			alert: state.alert
		};
	}

	let config = {
		buttons: [],
		count: 0,
		hideWithBackground: false,
		message: null,
		closeBtn: true
	};

	if (state.alert === undefined || state.alert.message === undefined) {
		return {
			alert: false
		};
	}
	if (state.alert.message === undefined || state.alert.message !== null) {
		config.message = state.alert.message;
	}

	if (state.alert.buttons !== undefined && state.alert.buttons.length > 0) {
		config.buttons = state.alert.buttons.slice(0, 2);
	}

	if (state.alert.count !== undefined) {
		config.count = state.alert.count;
	}

	if (state.alert.hideWithBackground !== undefined) {
		config.hideWithBackground = state.alert.hideWithBackground;
	}

	if (state.alert.closeBtn !== undefined) {
		config.closeBtn = state.alert.closeBtn;
	}

	return {
		alert: config
	};
};

const mapDispatchToProps = (dispatch) =>{
	return {
		actions: bindActionCreators(AlertActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Alert);