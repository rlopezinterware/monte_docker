import * as types from "../../constants";

export const hideAlert = () => {
    return (dispatch) => {
        dispatch({type: types.ACTION_HIDE_ALERT, flag: false});
    };
};

export const showAlert = (config) => {
    return (dispatch) => {
        dispatch({type: types.ACTION_SHOW_ALERT, alertConfig: config});
    };
};

