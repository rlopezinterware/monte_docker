import * as types from "../../constants";
import InitialData from "../../InitialData";

const alertReducer = (state = InitialData.alertConfig, action) => {
    switch (action.type) {
        case types.ACTION_HIDE_ALERT:
            return action.flag;
        case types.ACTION_SHOW_ALERT:
            return action.alertConfig;
        default:
            return state;
    }
};

export default alertReducer;