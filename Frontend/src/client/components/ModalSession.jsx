import React, {PropTypes} from "react";
import Alert from "./alert/Alert";
import Loading from "./loading/Loading";
import {connect} from "react-redux";

class App extends React.Component {

	constructor(props, context) {
		super(props, context);
	}
	
	render() {
		return (
			<div>
				<h3 className="text-center text-danger">
					<i className="fa fa-clock-o"></i>
					{' Sesión por expirar'}
				</h3>
				<br/>
				<p className="text-center">¿Desea continuar con la sesión activa?</p>
			</div>
		);
	}
}

App.contextTypes = {
	router: PropTypes.object
};

export default App;
