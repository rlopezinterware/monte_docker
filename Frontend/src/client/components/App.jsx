import React, {PropTypes} from "react";
import Alert from "./alert/Alert";
import Loading from "./loading/Loading";
import {connect} from "react-redux";

class App extends React.Component {

	constructor(props, context) {
		super(props, context);
	}
	
	render() {
		return (
			<div className="app container-fluid">
				<Loading />
				<Alert />
				{ this.props.children }
			</div>
		);
	}
}

App.propTypes = {
	children: PropTypes.object.isRequired
};

App.contextTypes = {
	router: PropTypes.object
};

const mapStateToProps = (state, ownProps) => {
	return {};
};

export default connect(mapStateToProps)(App);
