import Constans from "../../constants";
import InitialData from "../../InitialData";

const loadingReducer = (state = InitialData.isLoading, action) => {
    switch (action.type) {
        case Constans.IS_LOADING:
            return action.flag;
        default:
            return state;
    }
};

export default loadingReducer;
