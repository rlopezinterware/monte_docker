import * as loadingActions from "./LoadingActions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Labels from "../../Labels";
import React, {PropTypes} from "react";

class Loading extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.showLoading = this.showLoading.bind(this);
    }

    showLoading() {
        if (this.props.isLoading) {
            return <div className="loading-component"><i className="fa fa-circle-o-notch fa-spin fa-4x"/></div>;
        } else {
            return null;
        }
    }

    render() {
        return (this.showLoading());
    }
}


Loading.propTypes = {
    isLoading: PropTypes.bool
};

const mapStateToProps = (state, ownProps) =>{
    return {
        isLoading: state.isLoading
    };
};

export default connect(mapStateToProps)(Loading);