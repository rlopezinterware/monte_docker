import {Grid, Row, 	Col, Button} from "react-bootstrap";
import React, {PropTypes} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as headerActions from "./HeaderActions";
import {existsTokens} from "../../../utils/Security";
import Labels, {SETTINGS} from "../../../Labels";
import {CONTEXTO} from "../../../constants";
import NavMain from "../../navMain";
import Constants, * as types from "../../../constants";
import * as loginActions from "../body/login/LoginActions";
import LOGO from "../../../svg/logo.svg";

class Header extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.handleClickEndSession = this.handleClickEndSession.bind(this);
	}

	handleClickEndSession() {
		this.props.loginActions.getLogoutToken(sessionStorage.access_token).then(() => {
			this.props.loginActions.getLogoutToken(sessionStorage.refresh_token).then(() => {

				sessionStorage.clear();
				let URL = Constants.BASE_URL_FRONT + CONTEXTO + Constants.END_POINT_GET_LOGOUT;
				window.location.href = URL;

			}).catch(error => {
				console.log(JSON.stringify(error));
			});
		}).catch(error => {
			console.log(JSON.stringify(error));
		});
	}

	render() {
		return (
			<div>
				<header>
					<Grid>
						<Row>
							<Col xs={6} sm={4}>
							<img style={{padding: '5px 0 15px', height: '77px'}} src={LOGO} alt=''/>
							</Col>
							<Col className="text-center hidden-xs hidden-sm">
							</Col>
							<Col xs={6} sm={8}>
							<div className="pull-right">
								<br/>
									<label>{sessionStorage.firstname + ' '+ sessionStorage.lastname + ' - ' + sessionStorage.sucursal}</label>&nbsp;&nbsp;
									<Button bsStyle="info" className="btn-sm" onClick={this.handleClickEndSession}>{'Cerrar sesión'}</Button>
							</div>
							</Col>
						</Row>
					</Grid>
				</header>
				<NavMain />
			</div>
		);

	}
}

Header.contextTypes = {
	router: PropTypes.object
};

const mapStateToProps = (state) => {
	return {
		header: state.header
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		headerActions: bindActionCreators(headerActions, dispatch),
		loginActions: bindActionCreators(loginActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
