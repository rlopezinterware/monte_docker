import Constants, * as types from "../../../constants";

export const verifyHeader = (header) => {
    return  (dispatch) => {
        dispatch({type: types.ACTION_VERIFY_HEADER_TOKENS,header:header});
    };
};
