import * as types from "../../../constants";
import InitialData from "../../../InitialData";

const headerReducer = (state = InitialData.header, action) => {
    switch (action.type) {
        case types.ACTION_VERIFY_HEADER_TOKENS:
            return action.header;
        default:
            return state;
    }
};

export default headerReducer;