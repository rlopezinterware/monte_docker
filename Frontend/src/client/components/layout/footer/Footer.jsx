import React from "react";
import {Grid, Row, Col} from "react-bootstrap";
import Labels from "../../../Labels";

const Footer = () => {
	return (
		<footer>
			<Grid>
				<Row>
					<Col xs={12}>
						<p className="text-right">{Labels.FOOTER_COPY_RIGHT}</p>
					</Col>
				</Row>
			</Grid>
		</footer>
	);
};

export default Footer;