import _ from 'underscore';

import Constants, * as types from "../../../../constants";
import {TIEMPO_SESION} from "../../../../constants";
import ClientHttpRequest from "../../../../utils/ClientHttpRequest";
import {isLoading} from "../../../loading/LoadingActions";

export const getTokenAccess = (code) => {

	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_SEC + Constants.END_POINT_GET_USER_TOKEN,
			method: "POST",
			data: Object.assign({
				"code": code
			})
		}).request().then(response => {

			sessionStorage.uid = response.data.profile.samaccountname;
			sessionStorage.mail = response.data.profile.mail;
			sessionStorage.lastname = response.data.profile.lastname;
			sessionStorage.firstname = response.data.profile.firstname;
			sessionStorage.sucursal = response.data.profile.physicaldeliveryofficename;
			sessionStorage.access_token = response.data.access.access_token;
			sessionStorage.refresh_token = response.data.access.refresh_token;
			sessionStorage.session_time = Number(new Date()) + TIEMPO_SESION.INACTIVIDAD - TIEMPO_SESION.ALERTA;

			let featureArray = []; let stateArray = [];

			response.data.roles.map(role => {
				let feature = role.feature_id.map(item => {
					return  item.feature;
				});

				let state = role.state_id.map(item => {
					return  item._id;
				});

				featureArray = _.union(featureArray, feature);
				stateArray = _.union(stateArray, state);
			});

			console.log("FEATURES: " + JSON.stringify(featureArray) + ", STATE: " + JSON.stringify(stateArray));

			sessionStorage.features = JSON.stringify(featureArray);	
			sessionStorage.states = JSON.stringify(stateArray);	

			dispatch(isLoading(false));
		}).catch(error => {
			throw error;
		});
	};
};

export const getLogoutToken = (code) => {

	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_SEC + Constants.END_POINT_GET_DELETE_TOKEN,
			method: "POST",
			data: Object.assign({
				"code": code
			})
		}).request().then(() => {

		}).catch(error => {
			throw error;
		});
	};
};
