import * as types from "../../../../constants";
import InitialData from "../../../../InitialData";

const loginReducer = (state = InitialData.userData, action) => {
    switch (action.type) {
        case types.ACTION_GET_USER_TOKEN:
            return action.loggedUser;
        default:
            return state;
    }
};

export default loginReducer;

