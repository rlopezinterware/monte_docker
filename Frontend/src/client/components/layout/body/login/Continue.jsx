import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import React, {PropTypes} from "react";
import * as loginActions from "./LoginActions";
import {CONTEXTO,APP} from "../../../../constants";
import Constants from "../../../../constants";

class Continue extends React.Component {

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount () {

		let code = this.props.location.query.code;

		if (code !== undefined) {
			this.props.loginActions.getTokenAccess(code).then(() => {
				this.context.router.push(CONTEXTO + APP);
			}).catch(() => {
				sessionStorage.clear();
				let URL = Constants.BASE_URL_FRONT + CONTEXTO + Constants.END_POINT_GET_LOGOUT;
				window.location.href = URL;
			});
		} else {
			this.context.router.push(CONTEXTO + "/iniciar-sesion");
		}
	}
	
	render() {
		return null;
	}
}

Continue.propTypes = {
	loginActions: React.PropTypes.object.isRequired
};

Continue.contextTypes = {
	router: PropTypes.object
};

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loginActions: bindActionCreators(loginActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Continue);