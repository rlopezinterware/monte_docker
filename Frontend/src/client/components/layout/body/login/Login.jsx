import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import React, {PropTypes} from "react";
import Constants, * as types from "../../../../constants";

class Login extends React.Component {

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount () {

		let URL = Constants.END_POINT_GET_LOGIN + Constants.LOGIN_PATH;
		URL = URL.replace("PATH_URI", Constants.LOGIN_REDIRECT_URI);

		window.location.href = URL;
	}
	
	render() {
		return null;
	}
}

Login.contextTypes = {
	router: PropTypes.object
};

const mapStateToProps = (state) => {
	return {};
};

export default connect(mapStateToProps)(Login);