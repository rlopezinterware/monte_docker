import React, {PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import numeral from 'numeral';
import moment from 'moment';
import {Collapse, Tabs, Tab, Row, Grid, Col, Button, FormControl} from "react-bootstrap";

import SelectInput from "../../../common/SelectInput";
import CurrencyInput from "../../../common/CurrencyInput";
import TextInput from "../../../common/TextInput";
import AmortizationTable from "./forms/AmortizationTable";
import ModalReject from "../../../ModalReject";
import {MODULES,CONTEXTO,APP} from "../../../../constants";
import * as actionsNavMain from "../../../navMain/navMainActions"
import * as actionsSinResguardo from "../sinResguardo/SinResguardoActions"
import * as actionsAlert from "../../../alert/AlertActions";
import * as selectors from "./selectors/selectors"
import {ETAPAS, SPLIT} from "../../../../constants";
import INDICADOR from "../../../../svg/indicador_oferta.png";

let otherVal = 999;

class ManageApplications extends React.Component {

	constructor(props, context) {
		super(props, context);
		
		this.state = {
			validate: false,
			solicitud: {},
			contratado: false,
			noCredito: '123456',
			showTable: false,
			anim: '',
			offers: [],
			radioSel: '',
			otherOffer: {pagoMensual:0,tasa:0, creadoPor:sessionStorage.uid,fechaCreacion:moment().format('YYYY-MM-DDThh:mm:ss'),actualizadoPor:undefined,fechaActualizacion:undefined},
			selectedOffer: {},
			frecuencia: ''
		};

		this.handleCancelSolicitudData = this.handleCancelSolicitudData.bind(this);
		this.handleOnChangeSubProducto = this.handleOnChangeSubProducto.bind(this);
		this.handleOnChangePlazo = this.handleOnChangePlazo.bind(this);
		this.handleOnRadioOffer = this.handleOnRadioOffer.bind(this);
		this.handleOnChangeMonto = this.handleOnChangeMonto.bind(this);
		this.handleSelectTab = this.handleSelectTab.bind(this);
		this.showTable = this.showTable.bind(this);
		this.contract = this.contract.bind(this);
		this.finalize = this.finalize.bind(this);
	}

	componentWillMount() {

		if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.OFERTAR)){
			if (!this.props.solicitud.folio||!this.props.customerData.cliente.datosCuenta.idCliente) {
				this.context.router.push(CONTEXTO + APP + '/solicitud');
			} else {
				this.props.actionsSinResguardo.getMotivosRechazo();
				this.props.actionsNavMain.setActiveTab("Oferta");

				this.props.actionsSinResguardo.getPlazos().then(()=> {

					let data = Object.assign({}, this.props.credito);
					data["plazo"] = parseInt(this.props.plazos[0].id);
					data["tasaInteres"] = this.props.plazos[0].tasa;
					data["frecuencia"] = this.props.plazos[0].frecuencia;
					data["producto"] = parseInt(this.props.productos[0].id);
					data["subProducto"] = this.props.productos[0].subProducto;
					data["montoAvaluo"] = 900000;
		
					this.setState({frecuencia: this.props.plazos[0].frecuencia});
					this.props.actionsSinResguardo.updateStateCredito(data);
					this.props.actionsSinResguardo.getSolicitud({"folio": this.props.solicitud.folio, "numeroCliente": this.props.customerData.cliente.datosCuenta.idCliente}).then(() => {
						this.props.actionsSinResguardo.getCotizacion(this.props.credito, this.props.plazos, data.plazo, this.props.solicitud.vehiculo.anio).then(()=>{
							this.setState({solicitud: Object.assign({}, this.props.solicitud)});
							this.setState({offers: this.props.solicitud.listaOfertas.oferta.map(this.mapOffers, this)});
						});
					});
				});

			}
		} else {
			this.context.router.push(CONTEXTO + APP);
		}
	}

	handleOnChangeSubProducto (selected){

		if (selected === undefined) {
			return;
		}

		const [name, value] = selected.split(SPLIT);

		let data = Object.assign({}, this.props.credito);
		
		data["producto"] = value;
		let subProducto = this.props.actionsSinResguardo.getSubproducto(data, Object.assign({}, this.props.plazos));
		data["subProducto"] = subProducto.subProducto;
		this.props.actionsSinResguardo.updateStateCredito(data);

		this.resetTablaAmortizacion();
	}

	handleOnRadioOffer(event,offer) {
		this.resetTablaAmortizacion();
		let data = Object.assign({}, this.props.credito);
console.log(event.target.value)
		if (Number(event.target.value) !== otherVal) {
			data["montoMaximo"] = offer.montoCredito;
		}

		data["plazo"] = offer.plazo;
		data["montoSolicitado"] = offer.montoCredito;

		this.setState({selectedOffer: offer, radioSel: event.target.value},()=>{
			this.props.actionsSinResguardo.updateStateCredito(data);
		});
	}

	handleOnChangePlazo(selected) {

		if (selected === undefined) {
			return;
		}
		let tasa;
		this.resetTablaAmortizacion();
		const [name, value] = selected.split(SPLIT);

		this.props.plazos.map((item,idx)=>{
			if (item.id == value) {
				tasa = item.tasa;
			}
		});

		let otherOffer = {...this.state.otherOffer,plazo:value, tasa, estadoFormaPago:"ACEPTADA"};
		this.setState({otherOffer, selectedOffer: otherOffer, radioSel: otherVal}, ()=>{
			let data = {...this.props.credito};
			let aOffer = this.props.solicitud.listaOfertas.oferta;
			for (let i=0;i< aOffer.length;i++) {
				if (aOffer[i].plazo == value) {
					data.montoMaximo = aOffer[i].montoCredito;
					data.plazo = aOffer[i].plazo;
				}
			}
			this.props.actionsSinResguardo.updateStateCredito(data);
		});
	}

	handleOnChangeMonto(event) {
		event.preventDefault();

		this.resetTablaAmortizacion();

		let data = Object.assign({}, this.props.credito);
		data["montoSolicitado"] = numeral(event.target.value).value();

		let otherOffer = {...this.state.otherOffer,montoCredito:data.montoSolicitado, estadoFormaPago:"ACEPTADA"};
		document.forms[0].radio.value=0;
		this.setState({otherOffer,selectedOffer: otherOffer, radioSel: otherVal}, ()=>{
			this.props.actionsSinResguardo.updateStateCredito(data);
		});
	}

	resetTablaAmortizacion(){
		this.setState({showTable: false });
		this.props.actionsSinResguardo.resetTablaAmortizacionSuccess();
	}

	showTable (e) {
		
		e.preventDefault();

		let data = Object.assign({}, this.props.credito);
		let pagoMensual = 0;

		if (this.props.credito.plazo &&
				(Number(data.montoMinimo) <= Number(data.montoSolicitado)) &&
				(Number(data.montoSolicitado) <= Number(data.montoMaximo))) {
			this.props.actionsSinResguardo.generateTablaAmortizacion(this.props.credito).then(() => {
				this.setState({validate: false, showTable: true},()=>{
					if (this.state.radioSel == otherVal) {
						pagoMensual = this.props.tablaAmortizacion[0].abonoTotal;
						let otherOffer = Object.assign({}, this.state.otherOffer);
						otherOffer.pagoMensual = pagoMensual;
						this.setState({otherOffer, selectedOffer: otherOffer});
					} else {
						let offer = {...this.state.selectedOffer, pagoMensual: this.props.tablaAmortizacion[0].abonoTotal};
						this.setState({selectedOffer: offer});
					}
				});
			});
		} else {
			this.setState({validate: true});
		}
	}

	handleSelectTab(key) {
		this.setState({key});
	}

	contract() {
		let oferta = [this.state.selectedOffer];
		this.props.actionsSinResguardo.newContrato({...this.props.solicitud,listaOfertas: {oferta},frecuencia:this.state.frecuencia, pagoMensual: this.state.selectedOffer.pagoMensual}).then((response)=>{
			this.setState({contratado: true, noCredito: response.data.idContrato},()=>{ this.setState({anim: 'animate'}); });
		});
	}

	handleConfirm(){
		
		let _this = this;

		this.props.actionsAlert.showAlert({
			message: <h3 className="text-center">¿Desea cancelar la solicitud?</h3>,
			buttons: [
				{
					onClick: () => {
					},
					classCss: "btn-default",
					label: "No"
				},
				{
					onClick: () => {
						let data = Object.assign({}, _this.props.solicitud);
						_this.props.actionsSinResguardo.cancelSolicitud(data.folio, data.cliente.numeroCliente, _this.props.motivoSeleccionado)
						.then(response => {
							_this.props.actionsSinResguardo.resetAllStoreForASR();
							this.context.router.push(CONTEXTO + APP + '/solicitud');
						});
					},
					classCss: "btn-danger",
					label: "Si"
				}
			]
		});
	}

	handleCancelSolicitudData(event) {

		event.preventDefault();

		let _this = this;
		this.props.actionsAlert.showAlert({
			message: <ModalReject etapa={ETAPAS.OFERTA} />,
			buttons: [
				{
					onClick: () => {
						if (_this.props.motivoSeleccionado) {
							_this.handleConfirm();
							return false;
						} else {
							return false;
						}
					},
					classCss: "btn-default",
					label: "Rechazar solicitud"
				}
			]
		});
	}

	finalize() {
		this.context.router.push(CONTEXTO + APP + '/solicitud');
	}

	mapOffers(item,index){

		return (
			<Row key={index}>
				<Col sm={4} lg={3} className="mb14"><label className="visible-xs">{'Crédito'}</label><label><input name="radio" value={item.plazo} type="radio" onChange={(e)=>this.handleOnRadioOffer(e,{...item, estadoFormaPago:"ACEPTADA"})} /> {numeral(item.montoCredito).format('$ 0,0.00')}</label></Col>
				<Col sm={2} lg={3} className="mb14"><label className="visible-xs">{'Plazo'}</label><FormControl value={item.plazo + ' meses'} disabled/></Col>
				<Col sm={4} lg={3} className="mb14"><label className="visible-xs">{'Pago Mensual'}</label><FormControl value={numeral(item.pagoMensual).format('$ 0,0.00')} disabled/></Col>
				<Col sm={2} lg={3} className="mb14"><label className="visible-xs">{'Tasa'}</label><FormControl value={numeral(item.tasa/100).format('0.00 %')} disabled/></Col>
			</Row>
		)
	}

	render() {
		
		return (
			<div>
				{!this.state.contratado && <div>
					<h1 className="text-center" style={{display: 'none'}}>{'Oferta de crédito'}</h1>
					<img className="progress-indicator" src={INDICADOR}/>
					<form onSubmit={this.showTable}>
						<Row>
							<Col sm={4}>
								<SelectInput
									name={'formaPago'}
									label={'Forma de pago'}
									value={this.props.credito.producto || ""}
									options={this.props.productos}
									required
									error={this.state.validate}
									onChange={this.handleOnChangeSubProducto} />
							</Col>
						</Row>
						<Row className="hidden-xs">
							<Col sm={4} lg={3}> <label className="control-label">{'Crédito'}</label> </Col>
							<Col sm={2} lg={3}> <label className="control-label">{'Plazo'}</label> </Col>
							<Col sm={4} lg={3}> <label className="control-label">{'Pago mensual'}</label> </Col>
							<Col sm={2} lg={3}> <label className="control-label">{'Tasa'}</label> </Col>
						</Row>
						{this.state.offers}
						<hr/>
						<Row className="mb5">
							<Col sm={4} lg={3}>
								<label>
									<input name="radio" checked={this.state.radioSel == otherVal} value={otherVal} type="radio" onChange={(e)=>{this.handleOnRadioOffer(e,{...this.state.otherOffer, estadoFormaPago:"ACEPTADA"})}}/>{' Otro '}
								</label>
								<span style={{width: "calc( 100% - 60px)", display: "inline-block", marginLeft: "5px"}}>
									<CurrencyInput
										label="Crédito"
										name="otroCredito"
										max={this.state.radioSel == otherVal ?this.props.credito.montoMaximo:''}
										min={this.state.radioSel == otherVal ?this.props.credito.montoMinimo:''}
										onChange={this.handleOnChangeMonto}
										value={this.state.otherOffer.montoCredito || "0"}
										required={this.state.radioSel == otherVal}
										error={this.state.validate}/>
								</span>
							</Col>
							<Col sm={2} lg={3}>
								<SelectInput
									label="Plazo"
									name="otroPlazo"
									value={this.state.otherOffer.plazo || ""}
									options={this.props.plazos}
									required={this.state.radioSel == otherVal}
									error={this.state.validate}
									onChange={this.handleOnChangePlazo}/>
							</Col>
							<Col sm={4} lg={3}><CurrencyInput label="Pago mensual" name="otroPagoMensual" value={this.state.otherOffer.pagoMensual} disabled/></Col>
							<Col sm={2} lg={3}><TextInput label="Tasa" name="otroTasa" value={numeral(Number(this.state.otherOffer.tasa)/100).format('0.00 %')} disabled/></Col>
						</Row>
						<Row className="mb42"><Col xs={12}><Button type="submit" bsStyle="primary" className="pull-right" disabled={!this.state.radioSel}>{'Consultar tabla de amortización'}</Button></Col></Row>
					</form>
					<Collapse in={this.state.showTable}>
						<div>
							<AmortizationTable 
								data={this.props.tablaAmortizacion} 
								credito={this.props.credito}
								numItems={48} />
							<hr className={'mb70'}/>
							<Row>
								<Col sm={12}>
									<Button bsStyle="link" onClick={this.handleCancelSolicitudData}>{'Cancelar Solicitud'}</Button>
									<Button bsStyle="primary" className="pull-right" onClick={this.contract}>{'Solicitar'}</Button>
								</Col>
							</Row>
						</div>
					</Collapse>
				</div>}
				{this.state.contratado && <div>
					<Row className="text-center">
						<Col xs={12}>
							<h1>Crédito contratado</h1>
							<div className="icon-circle"><div className="icon-check-container"><div className={"icon-check " + this.state.anim}></div></div></div>
						</Col>
						<Col xs={8} xsOffset={2}>
							<h3>Número de crédito</h3>
							<p><strong>{this.state.noCredito}</strong></p>
							<p>Para la dispersión del crédito contratado será necesario contar con la instalacción de GPS en el vehículo y la documentación firmada por parte del cliente.</p>
							<p className="mb19">Una vez concluídos los requerimientos vuelva a la opción Consulta de solicitud, busque el ID del prospecto para acceder a la oferta, dispersar el crédito y finalizar el proceso.</p>
							<Button type="submit" bsStyle="primary" className="center-block" onClick={this.finalize}>{'Aceptar'}</Button>
						</Col>
					</Row>
				</div>}
			</div>
		);
	}
}

ManageApplications.contextTypes = {
	router: PropTypes.object
}

const mapStateToProps = (state, ownProps) =>{


	let solicitud = Object.assign({listaOfertas:{oferta:[]}},state.solicitud);
	let plazos = [], productos = [];

	if (Object.keys(state.plazos).length !== 0) {
		productos = selectors.productosFormattedForForm(state.plazos);

		if (state.credito.producto) {
			plazos = selectors.plazosFormattedForForm(state.plazos, state.credito.producto);
		} else {
			plazos = selectors.plazosFormattedForForm(state.plazos, productos[0].producto);
		}
	}
	return {
		customerData: state.customerData,
		solicitud : state.solicitud,
		productos: productos,
		plazos: plazos,
		credito: state.credito||{},
		tablaAmortizacion: state.tablaAmortizacion,
		motivoSeleccionado: state.motivoSeleccionado
	};
};

const mapDispatchToProps = (dispatch) =>{
	return {
		actionsNavMain: bindActionCreators(actionsNavMain, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch),
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageApplications);
