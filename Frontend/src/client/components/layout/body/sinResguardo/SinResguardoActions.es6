
import _ from 'underscore';

import React from 'react'

import * as actionsAlert from "../../../alert/AlertActions";
import * as actionsNavMain from "../../../navMain/navMainActions";

import Constants, * as types from "../../../../constants";

import ClientHttpRequest from "../../../../utils/ClientHttpRequest";
import {isLoading} from "../../../loading/LoadingActions";

import moment from "moment";

function _alert (codigoError, errorMsg) {
	return actionsAlert.showAlert({
		message: <h3 className="text-center">{errorMsg}</h3>,
		buttons: [ { 
			onClick: () => {
				if (codigoError && codigoError === Constants.CODE_SESSION_OUT) { // Session Out Service
					sessionStorage.clear();
					let URL = Constants.BASE_URL_FRONT + CONTEXTO + Constants.END_POINT_GET_LOGOUT;
					window.location.href = URL;
				}
			}, 
			classCss: "btn-default", 
			label: "Aceptar" 
		}]
	});
}

export const updateError = (errorMsg) => {
	if (errorMsg !== undefined) {
		return {type: types.ERROR, errorMsg};
	}
};

export const getCustomerDataSuccess = (customerData = {}) => {
	return {type: types.ACTION_GET_DATA_CUSTOMER, customerData};
};

export const updateStateAutoSuccess = (auto = {}) => {
	return {type: types.ACTION_UPDATE_STATE_AUTO, auto};
};

export const updateStateAuto = (auto) => {
	return (dispatch) => {
		dispatch(updateStateAutoSuccess(auto));
	};
};

export const getListOfMarcasSuccess = (marcas = []) => {
	return {type: types.ACTION_GET_MARCAS, marcas};
};

export const getListOfMarcas = () => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_MARCAS,
			method: "POST",
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getListOfMarcasSuccess(response.data.data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}getListOfMarcasSuccess
			throw error;
		});
	};
};

export const getListOfSubMarcasSuccess = (submarcas = []) => {
	return {type: types.ACTION_GET_SUBMARCAS, submarcas};
};

export const getListOfSubmarcas = (idMarca) => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_SUBMARCAS,
			method: "POST",
			data: Object.assign({"idMarca": idMarca}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getListOfSubMarcasSuccess(response.data.data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getListOfModelosSuccess = (modelos = []) => {
	return {type: types.ACTION_GET_MODELOS, modelos};
};

export const getListOfModelos = (idMarca, idSubmarca) => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_MODELOS,
			method: "POST",
			data: Object.assign({"idMarca": idMarca, "idSubMarca": idSubmarca }),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getListOfModelosSuccess(response.data.data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getListOfVersionesSuccess = (versiones = []) => {
	return {type: types.ACTION_GET_VERSIONES, versiones};
};

export const getListOfVersiones = (idMarca, idSubmarca, idModelo) => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_VERSIONES,
			method: "POST",
			data: Object.assign({
				"idMarca": idMarca,
				"idSubMarca": idSubmarca,
				"idModelo": idModelo
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getListOfVersionesSuccess(response.data.data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getListOfComplementosSuccess = (complementos = []) => {
	return {type: types.ACTION_GET_COMPLEMENTOS, complementos};
};

export const getListOfKilometrajeSuccess = (kilometrajes = []) => {
	return {type: types.ACTION_GET_KILOMETRAJES, kilometrajes};
};

export const getListOfKilometrajeComplementos = (auto) => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_KILOMETRAJES_COMPLEMENTOS,
			method: "POST",
			data: Object.assign({
				"idMarca": auto.marca,
				"idSubMarca": auto.submarca,
				"idModelo": auto.modelo,
				"idVersion": auto.version
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getListOfKilometrajeSuccess(response.data.dataKil));
			dispatch(getListOfComplementosSuccess(response.data.dataCom));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getPlazosSuccess = (plazos = []) => {
	return {type: types.ACTION_GET_PLAZOS, plazos};
};

export const getPlazos = (param) => {
	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_PLAZOS,
			method: "POST",
			data: Object.assign({
				"producto": "601"
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getPlazosSuccess(response.data.tasaPlazo));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const updateStateCreditoSuccess = (credito = {}) => {
	return {type: types.ACTION_UPDATE_STATE_CREDITO, credito};
};

export const updateStateCredito = (credito) => {
	return (dispatch) => {
		dispatch(updateStateCreditoSuccess(credito));
	};
};

export const getMontoPrestamoSuccess = (prestamo = {}) => {
	return {type: types.ACTION_GET_MONTO_PRESTAMO, prestamo};
};

export const getMontoPrestamo = (credito, auto) => {

	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_MONTO_PRESTAMO,
			method: "POST",
			data: Object.assign({
				"anio": auto.anio,
				"idMarca": auto.marca,
				"marcaDescription": auto.marcaDescription,
				"idSubMarca": auto.submarca,
				"submarcaDescription": auto.submarcaDescription,
				"idModelo": auto.modelo,
				"modeloDescription": auto.modeloDescription,
				"idVersion": auto.version,
				"versionDescription": auto.versionDescription,
				"idKilometraje": auto.kilometraje,
				"kilometrajeDescription": auto.kilometrajeDescription,
				"idComplemento": auto.equipoAdicional == 0 ? "" : auto.equipoAdicional,
				"equipoAdicionalDescription": auto.equipoAdicionalDescription
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {

			dispatch(updateError(''));

			if (credito != null) {
				credito["montoAvaluo"] = response.data.montoAvaluo;
				credito["montoMinimo"] = response.data.montoMinimoPrestamo;
				credito["montoMaximo"] = response.data.montoMaximoPrestamo;
				credito["montoSolicitado"]  = response.data.montoMaximoPrestamo;

				dispatch(updateStateCreditoSuccess(credito));
			}

			dispatch(getMontoPrestamoSuccess(response.data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getSubproducto = (credito, plazos) => {
	return (dispatch) => {
		return _.findWhere(plazos, {"producto": parseInt(credito.producto)});
	};
}

export const getCotizacion = (credito, plazos, plazo, anio, cliente = {}) => {
	
	let data = Object.assign({}, credito);
	let plazoItem = _.findWhere(plazos, {"id": Number(plazo), "subProducto": data.subProducto });

	data["tasaInteres"]   = plazoItem.tasa;
	data["plazo"]  = parseInt(plazo);
	data["frecuencia"]  = parseInt(plazoItem.frecuencia);

	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_COTIZACION,
			method: "POST",
			data: Object.assign({
				"tipoVehiculo": 1,
				"anio": anio,
				"plazo": data.plazo,
				"montoAvaluo": data.montoAvaluo,
				"calificacionMidas": cliente.calificacionMidas,
				"sobreAforo": cliente.sobreAforo
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			
			data["montoAvaluo"] = response.data.montoAvaluo;
			data["montoMinimo"] = response.data.montoMinimoPrestamo;
			data["montoMaximo"] = response.data.montoMaximoPrestamo;
			data["montoSolicitado"]  = Number(data["montoSolicitado"]) <= Number(response.data.montoMaximoPrestamo) && 
                                       Number(data["montoSolicitado"]) >= Number(response.data.montoMinimoPrestamo)  ? 
                                       data["montoSolicitado"] : response.data.montoMaximoPrestamo;

			dispatch(updateStateCreditoSuccess(data));
			dispatch(getMontoPrestamoSuccess(response.data));

			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			dispatch(updateStateCreditoSuccess(data));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getTablaAmortizacionSuccess = (tablaAmortizacion = []) => {
	return {type: types.ACTION_GET_TABLA_AMORTIZACION, tablaAmortizacion};
};

export const resetTablaAmortizacionSuccess = () => {
	return {type: types.ACTION_RESET_TABLA_AMORTIZACION};
};

export const generateTablaAmortizacion = (credito) => {

	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_TABLA_AMORTIZACION,
			method: "POST",
			data: Object.assign({
				"sociedad" : "01",
				"producto" : credito.producto,
				"subproducto" : credito.subProducto,
				"montoSolicitado" : credito.montoSolicitado,
				"frecuencia" : credito.frecuencia,
				"plazo" : credito.plazo,
				"tasaInteres" : credito.tasaInteres,
				"fechaDesembolso" : moment(new Date()).locale("es").format("YYYY-MM-DD", "es").toUpperCase(),
				"tipoPago" : "2"
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(updateStateCreditoSuccess({...credito,cat:response.data.cat}));
			dispatch(getTablaAmortizacionSuccess(response.data.pago));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getTemplate = (type) => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_TEMPLATE,
			method: "POST",
			data: Object.assign({"type": type}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			return response.data.data;
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getSolicitudSuccess = (solicitud = {}) => {
	return {type: types.ACTION_GET_SOLICITUD, solicitud};
};

export const getSolicitud = (solicitud) => {

	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_SOLICITUD,
			method: "POST",
			data: Object.assign({
				"folio": solicitud.folio,
				"numeroCliente": solicitud.numeroCliente
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			let data = response.data;
			dispatch(getSolicitudSuccess(data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const updateSolicitud = (solicitud) => {

	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_UPDATE_SOLICITUD,
			method: "POST",
			data: Object.assign({
				"solicitud": solicitud
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getSolicitudSuccess(solicitud));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const isChange = (object, other) => {
	return (dispatch) => {
		return _.isEqual(object, other);
	};
};

export const cancelSolicitud = (folio, numeroCliente, motivo) => {

	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_CANCEL_SOLICITUD,
			method: "POST",
			data: Object.assign({
				"folio": folio,
				"numeroCliente": numeroCliente,
				"motivo": motivo
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			return response.data;
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getMotivosRechazoSuccess = (motivosRechazo = []) => {
	return {type: types.ACTION_GET_MOTIVOS_RECHAZO, motivosRechazo};
};

export const getMotivosRechazo = () => {
	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_MOTIVOS_RECHAZO,
			method: "POST",
			data: Object.assign({}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getMotivosRechazoSuccess(response.data.motivoRechazo));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const analisisCredito = (folio, numeroCliente, motivo) => {

	return (dispatch) => {
		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_ANALISIS_CREDITO,
			method: "POST",
			data: Object.assign({
				"folio": folio,
				"numeroCliente": numeroCliente,
				"motivo": motivo
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			return response.data;
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getClienteByIdSuccess = (customerData) => {
	return {type: types.ACTION_GET_DATA_CUSTOMER, customerData};
};

export const getClienteById = (idCliente, idSucursal) => {

	return (dispatch) => {

		dispatch(isLoading(true));

		return new ClientHttpRequest({

			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_DATA_CUSTOMER,
			method: "POST",
			data: Object.assign({
				"idCliente": idCliente,
				"idSucursal": idSucursal
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			dispatch(getClienteByIdSuccess(response.data));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getClienteSolicitud = (idCliente) => {

	return (dispatch) => {

		dispatch(isLoading(true));

		return new ClientHttpRequest({

			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_CUSTOMER_SOLICITUD,
			method: "POST",
			data: Object.assign({
				"idCliente": idCliente
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			return response;
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const newClienteBP = (cliente) => {

	return (dispatch) => {

		dispatch(isLoading(true));

		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_NEW_CUSTOMER_BP,
			method: "POST",
			data: Object.assign(cliente),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			return response;
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const newSolicitud = (solicitud) => {

	return (dispatch) => {

		dispatch(isLoading(true));

		return new ClientHttpRequest({
    
			url: Constants.BASE_URL_BEN + Constants.END_POINT_NEW_SOLICITUD,
			method: "POST",
			data: Object.assign({
				"idProducto": solicitud.idProducto,
				"nombreProducto": solicitud.nombreProducto,
				"idSubProducto": solicitud.idSubProducto,
				"numeroCliente" : solicitud.numeroCliente,
				"tipoVehiculo" :solicitud.tipoVehiculo,
				"vin" : solicitud.vin
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(isLoading(false));
			return response;
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const newContrato = (solicitud) => {

	return (dispatch) => {

		dispatch(isLoading(true));

		return new ClientHttpRequest({

			url: Constants.BASE_URL_BEN + Constants.END_POINT_NEW_CONTRATO,
			method: "POST",
			data: Object.assign({
				"sucursal": sessionStorage.sucursal,
				"idCliente": solicitud.cliente.numeroCliente,
				"nombreProducto": solicitud.nombreProducto,
				"folio": solicitud.folio,
				"sociedad": "01",
				"producto": solicitud.idProducto,
				"subproducto": solicitud.idSubProducto,
				"frecuencia": solicitud.frecuencia,
				"montoSolicitado": solicitud.listaOfertas.oferta[0].montoCredito,
				"plazo": solicitud.listaOfertas.oferta[0].plazo,
				"tasaInteres": solicitud.listaOfertas.oferta[0].tasa,
				"fechaDesembolso": moment().format('YYYY-MM-DD'),
				"fechaPrimerCobro": "",
				"multiploMoratorio": "0",
				"origen": "0002",
				"destino": "16",
				"tipoPago": "2",
				"porcentajeAmortizacion": 0,
				"diasGracia": 0,
				"pagoMensual": solicitud.pagoMensual
				//,numeroGarantia: solicitud.numeroGarantia
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response=> {
			dispatch(isLoading(false));
			return(response);
		}).catch(error=> {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const desembolso = (solicitud) => {

	return (dispatch) => {

		dispatch(isLoading(true));

		return new ClientHttpRequest({

			url: Constants.BASE_URL_BEN + Constants.END_POINT_DESEMBOLSO,
			method: "POST",
			data: Object.assign({
				"folio" : solicitud.folio,
				"idCliente" : solicitud.cliente.numeroCliente
			}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response=> {
			dispatch(isLoading(false));
			return(response);
		}).catch(error=> {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const getOffer = (plazo, offers) => {

	return (dispatch) => {
		return _.findWhere(offers, {"plazo": parseInt(plazo)});
	};
};

export const getAnio = (modelo, modelos) => {

	return (dispatch) => {
		let anio = _.findWhere(modelos, {"id": parseInt(modelo)});
		return anio.valor.substring(0,4);
	};
};

export const getDescription = (value, list) => {

	return (dispatch) => {

		if (value === undefined || value === "") {
			return "";
		} else {
			let desc = _.findWhere(list, {"id": parseInt(value)});
			return desc.valor;
		}
	};
};

export const resetStateAuto = () => {
	return (dispatch) => {
		dispatch({type: types.ACTION_RESET_STATE_AUTO});
	};
};

export const getValueForSAP = (list) => {

	return (dispatch) => {
		let item =  _.findWhere(list, {"@origen": "SAP"});

		if (item === undefined || item === null) {
			return null;
		} else {
			return item.$;
		}
	};
};

export const getAuthorizationModule = (features, feature) => {

	return (dispatch) => {
		return _.contains(features, feature);
	};
};

export const getAuthorizationSolicitud = (states, state) => {

	return (dispatch) => {
		return _.contains(states, state);
	};
};

export const getListOfLocalidadPLDSuccess = (localidadpld = []) => {
	return {type: types.ACTION_GET_LOCALIDAD_PLD, localidadpld};
};

export const getListOfLocalidadPLD = (idEstado) => {
	return (dispatch) => {
		dispatch(isLoading(true));
		return new ClientHttpRequest({
			url: Constants.BASE_URL_BEN + Constants.END_POINT_GET_LOCALIDAD_PLD,
			method: "POST",
			data: Object.assign({"idEstado": idEstado}),
			headers: Object.assign({"Authorization": sessionStorage.access_token, "uid": sessionStorage.uid})
		}).request().then(response => {
			dispatch(getListOfLocalidadPLDSuccess(response.data.data));
			dispatch(isLoading(false));
		}).catch(error => {
			dispatch(isLoading(false));
			if (error.response) {
				dispatch(_alert(error.response.data.codigoError, error.response.data.descripcionError));
			}
			throw error;
		});
	};
};

export const resetAllStoreForASR = () => {
	return (dispatch) => {
		return new Promise((resolve, reject) => {
			dispatch(getListOfSubMarcasSuccess());
			dispatch(getListOfModelosSuccess());
			dispatch(getListOfVersionesSuccess());
			dispatch(getListOfKilometrajeSuccess());
			dispatch(getListOfComplementosSuccess());
			dispatch(updateStateAutoSuccess());
			dispatch(getPlazosSuccess());
			dispatch(updateStateCreditoSuccess());
			dispatch(getMontoPrestamoSuccess());
			dispatch(getTablaAmortizacionSuccess());
			dispatch(getSolicitudSuccess());
			dispatch(getMotivosRechazoSuccess());

			resolve({});
		});
	};
};
