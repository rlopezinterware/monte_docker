import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {existsTokens} from "../../../../utils/Security";

import {Grid, Row, Col} from "react-bootstrap";
import Header from "../../header/Header"
import Footer from "../../footer/Footer"
import ModalSession from "../../../ModalSession";

import * as loginActions from "../login/LoginActions";
import * as actionsAlert from "../../../alert/AlertActions";
import * as actionsSinResguardo from "./SinResguardoActions";
import * as navActions from "../../../navMain/navMainActions";

import {MODULES,CONTEXTO,APP,TIEMPO_SESION} from "../../../../constants";
import Constants from "../../../../constants";

class SinResguardo extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.setSessionAlert = this.setSessionAlert.bind(this);
		this.logout = this.logout.bind(this);
	}

	logout() {
		this.props.loginActions.getLogoutToken(sessionStorage.access_token).then(() => {
			this.props.loginActions.getLogoutToken(sessionStorage.refresh_token).then(() => {
				sessionStorage.clear();
				let URL = Constants.BASE_URL_FRONT + CONTEXTO + Constants.END_POINT_GET_LOGOUT;
				window.location.href = URL;
			}).catch(error => {
				console.log(JSON.stringify(error));
			});
		}).catch(error => {
			console.log(JSON.stringify(error));
		});
	}

	setSessionAlert () {
		let _this = this,
				ahora = Number(new Date()),
				tiempo = sessionStorage.session_time - ahora, //Tiempo restante en caso de recargar la página
				minSigAlerta = TIEMPO_SESION.INACTIVIDAD - TIEMPO_SESION.ALERTA,
				tiempoAutoCierre = sessionStorage.session_time - ahora + TIEMPO_SESION.ALERTA,
				cierre;
				cierre = setTimeout(_this.logout,tiempoAutoCierre);

		if (tiempo > 1) { // Reload
			setTimeout(this.setSessionAlert,tiempo);
			clearTimeout(cierre);
		} else {
			this.props.actionsAlert.showAlert({
				closeBtn: false,
				message: <ModalSession />,
				buttons: [
					{ onClick: () => {}, classCss: "btn-default", label: "No" },
					{
						onClick: () => {
							sessionStorage.session_time = ahora + minSigAlerta;
							tiempo = minSigAlerta;
							clearTimeout(cierre);
							setTimeout(_this.setSessionAlert,tiempo);
						},
						classCss: "btn-info",
						label: "Si"
					}
				]
			});
		}
	}

	componentWillMount() {
		this.isTokens = existsTokens();
		if (!this.isTokens) {
			this.context.router.push(CONTEXTO + "/iniciar-sesion");
		} else {
			document.head.getElementsByTagName("title")[0].innerText = "SPA Auto sin resguardo";
			this.setSessionAlert();
			let appContext = CONTEXTO + APP;

			let features = []; let routeInit = "";


			if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.COTIZAR)){
				features.push({ name: 'Cotizador', href: appContext + '/cotizador' });
				routeInit = '/cotizador';
			}
			if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.REGISTRAR) || this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.CONSULTAR)){
				features.push({ name: 'Solicitud', href: appContext + '/solicitud' });
				if (routeInit === "") {
					routeInit = '/solicitud';
				}
			}
			if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.INSPECCIONAR)){
				features.push({ name: 'Inspección' });
			}
			if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.OFERTAR)){
				features.push({ name: 'Oferta' });
			}
			if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.CONTRATAR)){
				features.push({ name: 'Contratar'});
			}

			this.props.navActions.loadTabs(features);
			if (sessionStorage.features === '[]') {
				let _this = this;
				this.props.actionsAlert.showAlert({
					closeBtn: false,
					message: <h3 className="text-center">Usuario sin acceso al sistema.</h3>,
					buttons: [
						{ onClick: () => {_this.logout();}, classCss: "btn-default", label: "Aceptar" }
					]
				});
			} else {
				this.context.router.replace(appContext + routeInit);
			}
		}
	}

	render() { 
		return (
			<div>
				<Header verify={this.isTokens}/>
				<div className="body">
					<Grid>
						<Row>
							<Col sm={12}>
								{this.props.children}
							</Col>
						</Row>
					</Grid>
				</div>
				<Footer />
			</div>
		);
	}
}

SinResguardo.propTypes = {
	actionsSinResguardo: React.PropTypes.object.isRequired,
	children: React.PropTypes.object
};

SinResguardo.contextTypes = {
	router: React.PropTypes.object
};

const mapStateToProps = (state) => {
	return {
		navItems: state.navItems
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		navActions: bindActionCreators(navActions, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch),
		loginActions: bindActionCreators(loginActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(SinResguardo);
