import React from "react";
import {Row, Col, Tabs, Tab, FormGroup, Button, Collapse, Fade} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import numeral from "numeral";
import moment from "moment";

import * as actionsSinResguardo from "./SinResguardoActions";
import * as actionsLoading from "../../../loading/LoadingActions";
import * as actionsAlert from "../../../alert/AlertActions";
import * as actionsNavMain from "../../../navMain/navMainActions"

import CarDataForm from "./forms/CarDataForm";
import CarIdForm from "./forms/CarIdForm";
import ValuerForm from "./forms/ValuerForm";
import ModalReject from "../../../ModalReject";

import LABELS, {ASR} from "../../../../Labels";
import {SPLIT, AUTO, MODULES, CONTEXTO, APP, ETAPAS} from "../../../../constants";
import INDICADOR from "../../../../svg/indicador_inspeccion.png"

class ManageValuer extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.handleSelectTab = this.handleSelectTab.bind(this);

		this.submitCarDataForm = this.submitCarDataForm.bind(this);
		this.submitCarIdDataForm = this.submitCarIdDataForm.bind(this);
		this.handleCancelSolicitudData = this.handleCancelSolicitudData.bind(this);
		this.handleOnChangeSolicitudData = this.handleOnChangeSolicitudData.bind(this);
		this.handleOnChangeSolicitudInspeccionData = this.handleOnChangeSolicitudInspeccionData.bind(this);
		this.handlerEnviarDictaminacion = this.handlerEnviarDictaminacion.bind(this);
		this.finalize = this.finalize.bind(this);

		this.state = {
			key: 1,
			solicitud: {},
			anim: '',
			inspeccionado: false,
			validateCarIdForm: false,
			validateValuerForm: false
		};
	}
	
	componentWillMount() {
		if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.INSPECCIONAR)){
			
			if (!this.props.solicitud.folio || !this.props.customerData.cliente) {
				this.context.router.push(CONTEXTO + APP + '/solicitud');
			} else {
				this.props.actionsNavMain.setActiveTab("Inspección")
				this.props.actionsSinResguardo.updateStateAuto(this.props.auto);
				this.props.actionsSinResguardo.getMotivosRechazo();
				this.props.actionsSinResguardo.getSolicitud({"folio": this.props.solicitud.folio, "numeroCliente": this.props.customerData.cliente.datosCuenta.idCliente}).then(response => {
					this.setState({solicitud: Object.assign({}, this.props.solicitud)});

					let data = Object.assign({}, this.props.auto);
					
					if (this.props.solicitud.vehiculo.marca) {

						data.marca = this.props.solicitud.vehiculo.marca.idMarca;
						data.marcaDescription = this.props.solicitud.vehiculo.marca.descripcion;

						data.submarca = this.props.solicitud.vehiculo.submarca.idSubMarca;
						data.submarcaDescription = this.props.solicitud.vehiculo.submarca.descripcion;

						data.modelo = this.props.solicitud.vehiculo.modelo.idModelo;
						data.modeloDescription = this.props.solicitud.vehiculo.modelo.descripcion;

						data.version = this.props.solicitud.vehiculo.version.idVersion;
						data.versionDescription = this.props.solicitud.vehiculo.version.descripcion;

						data.kilometraje = this.props.solicitud.vehiculo.kilometraje.idKilometraje;
						data.kilometrajeDescription = this.props.solicitud.vehiculo.kilometraje.descripcion;

						if (this.props.solicitud.vehiculo.complementos !== undefined && this.props.solicitud.vehiculo.complementos !== "") {
							let complementos = [], complementosDescription = [];
							
							this.props.solicitud.vehiculo.complementos.complemento.map(complemento => {
								complementos.push(complemento.idComplemento);
								complementosDescription.push(complemento.descripcion);
							});

							data.equipoAdicional = complementos.join(",");
							data.equipoAdicionalDescription = complementosDescription.join("#");
						}

						data.anio = this.props.solicitud.vehiculo.anio;

						this.props.actionsSinResguardo.getListOfSubmarcas(data.marca).then(() => {
							this.props.actionsSinResguardo.getListOfModelos(data.marca, data.submarca).then(() => {
								this.props.actionsSinResguardo.getListOfVersiones(data.marca, data.submarca, data.modelo).then(() => {
									this.props.actionsSinResguardo.getListOfKilometrajeComplementos(data).then (() => {
										this.props.actionsSinResguardo.updateStateAuto(data);
									});
								});	
							});
						});
					}
				});
			}
		} else {
			this.context.router.push(CONTEXTO + APP);
		}
	}

	submitCarDataForm(){
		let auto = Object.assign({}, this.props.auto);
		let solicitud = Object.assign({}, this.props.solicitud); 
		let vehiculo = Object.assign({}, solicitud.vehiculo);
		let cotizacion = Object.assign({}, solicitud.cotizacion);

		vehiculo["marca"] = {"idMarca": auto.marca, "descripcion" : auto.marcaDescription};
		vehiculo["submarca"] = {"idSubMarca": auto.submarca, "descripcion" : auto.submarcaDescription};
		vehiculo["modelo"] = {"idModelo": auto.modelo, "descripcion" : auto.modeloDescription};
		vehiculo["anio"] = auto.anio;
		vehiculo["version"] = {"idVersion": auto.version, "descripcion" : auto.versionDescription};
		vehiculo["kilometraje"] = {"idKilometraje": auto.kilometraje, "descripcion" : auto.kilometrajeDescription};
		vehiculo["complementos"] = {};

		if (auto.equipoAdicional !== undefined && auto.equipoAdicional !== "") {

			let arrayDescription = auto.equipoAdicionalDescription.split("#");
			let index = 0;

			let array = auto.equipoAdicional.split(",");
			let complementos = array.map(complemento => {			
				let value = {
					"idComplemento": complemento,
					"descripcion": arrayDescription[index]
				}; index = index + 1;

				return value;
			});
		
			vehiculo["complementos"].complemento = complementos;            
		}

		solicitud.vehiculo = vehiculo;

		this.props.actionsSinResguardo.getMontoPrestamo(null, this.props.auto).then(() => {

			cotizacion["montoAvaluo"] = this.props.prestamo.montoAvaluo;
			cotizacion["montoCastigo"] = "0";

			solicitud.cotizacion = cotizacion;
			
			this.props.actionsSinResguardo.getSolicitudSuccess(solicitud);
			this.setState({solicitud: solicitud});
			this.handleSelectTab(2);
		});
	}

	submitCarIdDataForm(){
		
		let newSolicitud = Object.assign({}, this.props.solicitud);

		if( !this.props.solicitud.vehiculo || (!this.props.solicitud.vehiculo.vin||
			!this.props.solicitud.vehiculo.serieMotor||
			!this.props.solicitud.vehiculo.numeroTarjetaCirculacion||
			!this.props.solicitud.vehiculo.color||
			!this.props.solicitud.vehiculo.placas||
			!this.props.solicitud.vehiculo.numeroFactura||
			!this.props.solicitud.vehiculo.rfcEmisor||
			!this.props.solicitud.vehiculo.agenciaOrigen)) {

			this.setState({validateCarIdForm: true});

		} else {

			this.setState({validateCarIdForm: false},()=>{
				this.props.actionsSinResguardo.getSolicitudSuccess(newSolicitud);
				this.setState({solicitud: newSolicitud});
				this.handleSelectTab(3);
			});
		}
	}

	handleSelectTab(key){
		this.setState({key});
	}

	handleOnChangeSolicitudData (event) {
		event.preventDefault();
		let data = Object.assign({}, this.props.solicitud);
		let dataAuto = Object.assign({}, data.vehiculo);
		dataAuto[event.target.name] = event.target.value;
		data.vehiculo = dataAuto;
		this.props.actionsSinResguardo.getSolicitudSuccess(data);
	}

	handleOnChangeSolicitudInspeccionData (event) {

		event.preventDefault();

		let data = Object.assign({}, this.props.solicitud);

		switch (event.target.name) {
			case "montoCastigo":
				let cotizacion = Object.assign({}, data.cotizacion);
				cotizacion[event.target.name] = event.target.value;
				data.cotizacion = cotizacion;
				break;
			case "comentarios": 
				let vehiculo = Object.assign({}, data.vehiculo);
				vehiculo[event.target.name] = event.target.value;
				data.vehiculo = vehiculo;
				break;
			default: break;
		}

		this.props.actionsSinResguardo.getSolicitudSuccess(data);
	}

	handlerEnviarDictaminacion () {
		let cTmp = this.props.solicitud.cotizacion;

		let newSolicitud = Object.assign({}, this.props.solicitud);

		if (!cTmp.montoCastigo || !this.props.solicitud.vehiculo.comentarios) {
			this.setState({validateValuerForm: true});
		} else {
			if (!(cTmp.montoCastigo < 0 ) && (cTmp.montoCastigo <= cTmp.montoAvaluo)) {
				this.props.actionsSinResguardo.updateSolicitud(newSolicitud).then(() => {
					this.props.actionsSinResguardo.analisisCredito(newSolicitud.folio, newSolicitud.cliente.numeroCliente, "").then(() => {
						this.setState({validateValuerForm: false, solicitud: newSolicitud,inspeccionado:true},()=>{
							setTimeout(()=>{
								this.setState({anim: 'animate'})
							},200);
						});
					});
				});
			}
		}
	}


	handleConfirm(){
		
		let _this = this;

		this.props.actionsAlert.showAlert({
			message: <h3 className="text-center">¿Desea cancelar la solicitud?</h3>,
			buttons: [
				{
					onClick: () => {
					},
					classCss: "btn-default",
					label: "No"
				},
				{
					onClick: () => {
						let data = Object.assign({}, _this.props.solicitud);
						_this.props.actionsSinResguardo.cancelSolicitud(data.folio, data.cliente.numeroCliente, _this.props.motivoSeleccionado)
						.then(() => {
							_this.props.actionsSinResguardo.resetAllStoreForASR();
							this.context.router.push(CONTEXTO + APP + '/solicitud');
						});	
					},
					classCss: "btn-danger",
					label: "Si"
				}
			]
		});
	}

	handleCancelSolicitudData(event) {

		event.preventDefault();

		let _this = this;
		
		this.props.actionsAlert.showAlert({
			message: <ModalReject etapa={ETAPAS.INSPECCION} />,
			buttons: [
				{
					onClick: () => {
			
						if (_this.props.motivoSeleccionado) {
							_this.handleConfirm();
							return false;
						} else {
							return false;
						}
					},
					classCss: "btn-default",
					label: "Rechazar solicitud"
				}
			]
		});
	}

	finalize () {
		this.context.router.push(CONTEXTO + APP + '/solicitud');
	}

	render() {
		return (
			<div>
				<h1 className="text-center" style={{display: 'none'}}>{'Inspección Física'}</h1>
				<img className="progress-indicator" src={INDICADOR} />
				{!this.state.inspeccionado &&
					<Tabs activeKey={this.state.key} onSelect={this.handleSelectTab} className="tabs2" id="cotizador">
						<Tab eventKey={1} title="Datos de cotización">
							<CarDataForm
								cancel={{action:this.handleCancelSolicitudData, label:'Rechazar solicitud'}}
								submit={{action:this.submitCarDataForm, label: 'Siguiente'}}/>
						</Tab>
						<Tab eventKey={2} title="Datos generales" disabled={this.state.key<2}>
							<CarIdForm solicitud={this.props.solicitud}  onChange={this.handleOnChangeSolicitudData} validate={this.state.validateCarIdForm}/>
							<Row>
								<Col sm={12}>
									<Button bsStyle="link" onClick={this.handleCancelSolicitudData}>{'Rechazar solicitud'}</Button>
									<Button type="submit" bsStyle="primary" className="pull-right" onClick={this.submitCarIdDataForm}>{'Siguiente'}</Button>
								</Col>
							</Row>
						</Tab>
						<Tab eventKey={3} title="Dictaminación" disabled={this.state.key<3}>
							<ValuerForm 
								cotizacion={this.props.solicitud.cotizacion}
								vehiculo={this.props.solicitud.vehiculo}
								validate={this.state.validateValuerForm}
								onChange={this.handleOnChangeSolicitudInspeccionData}/>
							<Row>
								<Col sm={12}>
									<Button bsStyle="link" onClick={this.handleCancelSolicitudData}>{'Rechazar solicitud'}</Button>
									<Button
										type="submit"
										bsStyle="primary"
										className="pull-right"
										active={this.state.valid}
										onClick={this.handlerEnviarDictaminacion}>
										{'Guardar inspección'} 
									</Button>
								</Col>
							</Row>
						</Tab>
					</Tabs>
				}
				{this.state.inspeccionado && 
					<div>
						<Row className="text-center">
							<Col xs={12}>
								<div className="icon-circle"><div className="icon-check-container"><div className={"icon-check " + this.state.anim}></div></div></div>
							</Col>
							<Col xs={8} xsOffset={2}>
								<p className="mb19"><strong>{'Inspección concluida satisfactoriamente.'}</strong></p>
								<Button type="submit" bsStyle="primary" className="center-block"onClick={this.finalize}>{'Finalizar'}</Button>
							</Col>
						</Row>
					</div>
				}
			</div>
		);
	}
}

ManageValuer.contextTypes = {
	router: React.PropTypes.object
}

ManageValuer.propTypes = {
	actionsSinResguardo: React.PropTypes.object.isRequired,
	actionsLoading: React.PropTypes.object.isRequired,
	actionsAlert: React.PropTypes.object.isRequired,
	onCancel: React.PropTypes.func
};

const mapStateToProps = (state, ownProps) => {

	return {
		customerData: state.customerData,
		auto: state.auto,
		onCancel: ownProps.onCancel,
		onReject: ownProps.onReject,
		solicitud : state.solicitud,
		prestamo: state.prestamo,
		motivoSeleccionado: state.motivoSeleccionado
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsLoading: bindActionCreators(actionsLoading, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch),
		actionsNavMain: bindActionCreators(actionsNavMain, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageValuer);