import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const plazosReducer = (state = InitialData.plazos, action) =>{
    switch (action.type) {
        case types.ACTION_GET_PLAZOS:
            return action.plazos;
        default:
            return state;
    }
};

export default plazosReducer;
