import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const customerDataReducer = (state = InitialData.customerData, action) => {
    switch (action.type) {
        case types.ACTION_GET_DATA_CUSTOMER:
            return action.customerData;
        default:
            return state;
    }
};

export default customerDataReducer;