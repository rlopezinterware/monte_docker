import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const solicitudReducer = (state = InitialData.solicitud, action) => {
    switch (action.type) {
        case types.ACTION_GET_SOLICITUD:
            return action.solicitud;
        default:
            return state;
    }
};

export default solicitudReducer;
