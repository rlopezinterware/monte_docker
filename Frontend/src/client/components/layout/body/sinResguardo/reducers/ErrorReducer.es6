import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const errorReducer = (state = InitialData.errorMsg, action) => {
    switch (action.type) {
        case types.ERROR:
            return action.errorMsg;
        default:
            return state;
    }
};

export default errorReducer;