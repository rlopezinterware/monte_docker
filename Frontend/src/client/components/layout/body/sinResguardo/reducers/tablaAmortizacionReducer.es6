import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const tablaAmortizacionReducer = (state = InitialData.tablaAmortizacion, action) => {
    switch (action.type) {
        case types.ACTION_GET_TABLA_AMORTIZACION:
            return action.tablaAmortizacion;
        case types.ACTION_RESET_TABLA_AMORTIZACION:
            return InitialData.tablaAmortizacion;
        default:
            return state;
    }
};

export default tablaAmortizacionReducer;
