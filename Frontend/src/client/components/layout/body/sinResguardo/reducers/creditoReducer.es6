import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const creditoReducer = (state = InitialData.credito, action) => {
    switch (action.type) {
        case types.ACTION_UPDATE_STATE_CREDITO:
            return action.credito;
        default:
            return state;
    }
};

export default creditoReducer;
