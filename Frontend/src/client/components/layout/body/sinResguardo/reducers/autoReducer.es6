import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const autoReducer = (state = InitialData.auto, action) => {
    switch (action.type) {
        case types.ACTION_UPDATE_STATE_AUTO:
            return action.auto;
        case types.ACTION_RESET_STATE_AUTO:
            return {};
        default:
            return state;
    }
};

export default autoReducer;
