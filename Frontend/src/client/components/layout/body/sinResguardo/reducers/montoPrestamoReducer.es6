import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const prestamoReducer = (state = InitialData.prestamo, action) => {
    switch (action.type) {
        case types.ACTION_GET_MONTO_PRESTAMO:
            return action.prestamo;
        default:
            return state;
    }
};

export default prestamoReducer;