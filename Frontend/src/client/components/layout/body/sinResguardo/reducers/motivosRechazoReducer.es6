import * as types from "../../../../../constants";
import InitialData from "../../../../../InitialData";

const motivosRechazoReducer = (state = InitialData.motivosRechazo, action) => {
    switch (action.type) {
        case types.ACTION_GET_MOTIVOS_RECHAZO:
            return action.motivosRechazo;
        default:
            return state;
    }
};

export default motivosRechazoReducer;
