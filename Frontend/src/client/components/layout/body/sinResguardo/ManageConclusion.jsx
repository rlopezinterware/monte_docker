import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import numeral from "numeral";
import {Row, Col, Button,} from "react-bootstrap";

import * as actionsNavMain from "../../../navMain/navMainActions"
import * as actionsSinResguardo from "../sinResguardo/SinResguardoActions"
import * as actionsAlert from "../../../alert/AlertActions";
import * as actionsLoading from "../../../loading/LoadingActions";

import CurrencyInput from "../../../common/CurrencyInput";
import TextInput from "../../../common/TextInput";
import {MODULES, CONTEXTO, APP, ETAPAS} from "../../../../constants";
import ModalReject from "../../../ModalReject";
import INDICADOR from "../../../../svg/indicador_contratar.png"

class ManageConclusion extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			concluido: false,
			anim: ''
		};
		this.conclude = this.conclude.bind(this);
		this.finalize = this.finalize.bind(this);
		this.handleCancelSolicitudData = this.handleCancelSolicitudData.bind(this);
	}
	
	componentWillMount() {

		if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.CONTRATAR)){
			
			if (!this.props.solicitud.folio || !(this.props.customerData.cliente && this.props.customerData.cliente.datosCuenta.idCliente)) {
				this.context.router.push(CONTEXTO + APP + '/solicitud');
			} else {
				this.props.actionsNavMain.setActiveTab("Contratar");
				this.props.actionsSinResguardo.getMotivosRechazo();
				this.props.actionsSinResguardo.getSolicitud({"folio": this.props.solicitud.folio, "numeroCliente": this.props.customerData.cliente.datosCuenta.idCliente}).then(() => {});
			}
		} else {
			this.context.router.push(CONTEXTO + APP);
		}
	}

	handleConfirm(){
		
		let _this = this;

		this.props.actionsAlert.showAlert({
			message: <h3 className="text-center">¿Desea cancelar la solicitud?</h3>,
			buttons: [
				{
					onClick: () => {
					},
					classCss: "btn-default",
					label: "No"
				},
				{
					onClick: () => {
						let data = Object.assign({}, _this.props.solicitud);
						_this.props.actionsSinResguardo.cancelSolicitud(data.folio, data.cliente.numeroCliente, _this.props.motivoSeleccionado)
						.then(response => {
							_this.props.actionsSinResguardo.resetAllStoreForASR();
							_this.context.router.push(CONTEXTO + APP + '/solicitud');
						});
					},
					classCss: "btn-danger",
					label: "Si"
				}
			]
		});
	}

	handleCancelSolicitudData(event) {

		event.preventDefault();

		let _this = this;
		
		this.props.actionsAlert.showAlert({
			message: <ModalReject etapa={ETAPAS.CONTRATAR} />,
			buttons: [
				{
					onClick: () => {
						if (_this.props.motivoSeleccionado) {
							_this.handleConfirm();
							return false;
						} else {
							return false;
						}
					},
					classCss: "btn-default",
					label: "Rechazar solicitud"
				}
			]
		});
	}

	conclude() {
		this.props.actionsSinResguardo.desembolso(this.props.solicitud).then(response=> {
			this.setState({concluido:true},()=>{
				this.setState({anim:'animate'});
			});
		});
	}

	finalize() {
		this.context.router.push(CONTEXTO + APP + '/solicitud');
	}

	render() {
		return (
			<div>
				<img className="progress-indicator" src={INDICADOR} />
				{!this.state.concluido &&
					<div>
						<Row>
							<Col sm={10} smOffset={1}>
								<Row>
									<Col sm={4}>
										<label className="control-label">{'ID de prospecto'}</label>
										<p className="form-readonly">{this.props.customerData.cliente? this.props.customerData.cliente.datosCuenta.idCliente : ''}</p>
									</Col>
									<Col sm={4}>
										<label className="control-label">{'Nombre'}</label>
										<p className="form-readonly">{this.props.customerData.cliente? this.props.customerData.cliente.datosPersonales.nombre + " " + this.props.customerData.cliente.datosPersonales.apellidoPaterno + " " + this.props.customerData.cliente.datosPersonales.apellidoMaterno : ''}</p>
									</Col>
									<Col sm={4}>
										<label className="control-label">{'Número de crédito'}</label>
										<p className="form-readonly">{this.props.solicitud.listaOfertas?this.props.solicitud.idContrato:''}</p>
									</Col>
								</Row>
							</Col>
						</Row>
						<hr/>
						<Row>
							<Col sm={10} smOffset={1}>
								<Row>
									<Col sm={3}><CurrencyInput name="credito" label="Crédito" 
										value={this.props.solicitud.listaOfertas.oferta.length ? this.props.solicitud.listaOfertas.oferta[0].montoCredito : ""} disabled/>
									</Col>
									<Col sm={3}><TextInput label="plazo" label="Plazo" 
										value={this.props.solicitud.listaOfertas.oferta.length ? this.props.solicitud.listaOfertas.oferta[0].plazo + ' meses' : ""} disabled/>
									</Col>
									<Col sm={3}><CurrencyInput label="montoPago" label="Monto de pago" 
										value={this.props.solicitud.listaOfertas.oferta.length ? this.props.solicitud.listaOfertas.oferta[0].pagoMensual : ""} disabled/>
									</Col>
									<Col sm={3}><TextInput label="tasa" label="Tasa" 
										value={this.props.solicitud.listaOfertas.oferta.length ? numeral(Number(this.props.solicitud.listaOfertas.oferta[0].tasa)/100).format('0.00 %') : ""} disabled/>
									</Col>
								</Row>
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<p className="text-right mb25">Si cuenta con toda la documentación firmada por parte del cliente proceda a concluir el proceso</p>
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<Button bsStyle="link" onClick={this.handleCancelSolicitudData}>{'Rechazar solicitud'}</Button>
								<Button bsStyle="primary" className="pull-right" onClick={this.conclude}>{'Contratar'}</Button>
							</Col>
						</Row>
					</div>
				}
				{this.state.concluido &&
					<div>
						<Row className="text-center">
							<Col xs={12}>
								<div className="icon-circle"><div className="icon-check-container"><div className={"icon-check " + this.state.anim}></div></div></div>
							</Col>
						</Row>
						<Row className="text-center">
							<Col xs={12}>
								<h3 className="mb19">Solicitud concluida exitosamente</h3>
								<Button type="submit" bsStyle="primary" className="center-block"onClick={this.finalize}>{'Finalizar'}</Button>
							</Col>
						</Row>
					</div>
				}
			</div>
		);
	}
}

ManageConclusion.contextTypes = {
	router: React.PropTypes.object
}

ManageConclusion.propTypes = {
	actionsSinResguardo: React.PropTypes.object,
	actionsLoading: React.PropTypes.object,
	actionsAlert: React.PropTypes.object,
	onCancel: React.PropTypes.func
};

const mapStateToProps = (state, ownProps) => {
	let customerData = {...state.customerData}
	if (!customerData.cliente) {
		customerData.cliente = {}
	}

	let listaOfertas = {}, oferta = [];

	if (state.solicitud.listaOfertas && state.solicitud.listaOfertas.oferta.length){
		listaOfertas = {...state.solicitud.listaOfertas}
		for (let i=0 ; i< listaOfertas.oferta.length ; i++) {
			if (listaOfertas.oferta[i].estadoFormaPago === "ACEPTADA") {
				oferta = [state.solicitud.listaOfertas.oferta[i]];
			}
		}
	}
	return {
		customerData: state.customerData,
		solicitud : {...state.solicitud, listaOfertas:{oferta}},
		motivoSeleccionado: state.motivoSeleccionado
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsLoading: bindActionCreators(actionsLoading, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch),
		actionsNavMain: bindActionCreators(actionsNavMain, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageConclusion);