import {Tabs, Tab, Row, Grid, Col, Nav, NavItem} from "react-bootstrap";
import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import NewApplicationForm from "./forms/NewApplicationForm";
import FindApplicationForm from "./forms/FindApplicationForm";
import * as actionsSinResguardo from "./SinResguardoActions";

import * as actionsNavMain from "../../../navMain/navMainActions";

import {MODULES} from "../../../../constants";
import INDICADOR from "../../../../svg/indicador_solicitudes.png"

class ManageApplications extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			tab: 'new',
			key : this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.CONSULTAR) ? 1: 2,
			consultar: this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.CONSULTAR),
			registrar: this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.REGISTRAR)
		}
		this.handleSelectTab = this.handleSelectTab.bind(this);
	}

	componentWillMount() {
			this.props.actionsNavMain.setActiveTab("Solicitud");
		if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.REGISTRAR)) {
			this.handleSelectTab(1);
		} else {
			this.handleSelectTab(2);
		}
	}

	handleSelectTab(key) {
		this.setState({key});
	}

	render() {
		
		return (
			<div>
				<h1 className="text-center" style={{display: 'none'}}>{'Solicitud'}</h1>
				<img className="progress-indicator" src={INDICADOR} />
					<Tabs activeKey={this.state.key} onSelect={this.handleSelectTab} className="tabs2" id="solicitudes">
						{ this.state.registrar && <Tab title={'Registro'} eventKey={1}> <NewApplicationForm tab={this.state.key} /> </Tab>}
						{ this.state.consultar && <Tab title={'Consulta'} eventKey={2}> <FindApplicationForm tab={this.state.key} /> </Tab>}
					</Tabs>
			</div>
		);
	}
}

ManageApplications.propTypes = {
	actionsSinResguardo: React.PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) =>{
	return {
	};
};

const mapDispatchToProps = (dispatch) =>{
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsNavMain: bindActionCreators(actionsNavMain,dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageApplications);
