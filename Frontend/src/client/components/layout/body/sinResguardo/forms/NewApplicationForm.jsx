import React from "react";
import {connect} from "react-redux";
import {Collapse, Tabs, Tab, Row, Col, FormGroup, Button, Label} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import SelectInput from "../../../../common/SelectInput";

import BPDataForm from "./BPDataForm";

import * as actionsSinResguardo from "./../SinResguardoActions";
import * as actionsLoading from "../../../../loading/LoadingActions";
import * as actionsAlert from "../../../../alert/AlertActions";
import {bindActionCreators} from "redux";

class NewApplicationForm extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			key: 1,
			toggle: true,
			anim: '',
			idCliente: '',
			noVIN : '',
			folio: '',
			complemento: false
		}

		this.toggle = this.toggle.bind(this);
		this.submit = this.submit.bind(this);
		this.submitBP = this.submitBP.bind(this);
		this.cancelBP = this.cancelBP.bind(this);
		this.finalize = this.finalize.bind(this);
		this.handleSelect =this.handleSelect.bind(this);

		this.handleOnChangeClienteId = this.handleOnChangeClienteId.bind(this);
		this.handleOnChangeNoVIN = this.handleOnChangeNoVIN.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.tab === 2) {
			this.cancelBP();
		}
	}

	handleOnChangeClienteId(event) {
		event.preventDefault();
		this.setState({idCliente: event.target.value});
	}

	handleOnChangeNoVIN(event) {
		event.preventDefault();
		this.setState({noVIN: event.target.value});
	}

	handleSelect(key){
		this.setState({key});
	}

	toggle(e) {
		e.preventDefault();
		if (this.state.toggle ) {
			if (this.state.idCliente <= 2147483647) {
				this.props.actionsSinResguardo.getClienteById(this.state.idCliente, "3").then(() => {
					this.setState({toggle:false})
				});
			} else {
				this.props.actionsAlert.showAlert({
					message: <h3 className="text-center">{'Cliente no existente'}</h3>,
					buttons: [
						{classCss: "btn-default", label: "Aceptar"},
					]
				});
			}
		} else {
			this.setState({toggle:true, key: 1, anim: '', idCliente: '', noVIN : '', folio: '', complemento: false})
		}
	}

	submit(e) {
		e.preventDefault();
		this.setState({complemento:true});
	}

	submitBP(e, response) {

		this.setState({folio: response.data.folio});

		this.setState({key: 2},()=>{
			setTimeout(()=>{
				this.setState({anim:'animate'})
			},200);
		});
	}

	cancelBP() {
		this.setState({complemento: false, toggle: true, idCliente: '', noVIN: ''})
	}

	finalize() {
		this.setState({key: 1, complemento: false, toggle: true, idCliente: '', noVIN: ''})
	}

	render() {
		return (
			<Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="creaSolicitudes">
				<Tab eventKey={1} title="1">
					<Row>
						<Col sm={6} smOffset={3} md={4} mdOffset={4}>
							<Collapse in={!this.state.complemento}>
								<div>
									<form onSubmit={this.toggle}>
										{this.state.toggle && <TextInput label="ID de prospecto" name="idCliente" placeholder="Ingrese número de ID" maxLength="10" numeric value={this.state.idCliente} onChange={this.handleOnChangeClienteId}/>}
										{!this.state.toggle && <div>
											<label className="control-label">{'ID de prospecto'}</label>
											<p className="form-readonly">{this.state.idCliente}</p>
										</div>}
										<Collapse in={this.state.toggle}>
											<div>
												<Button type="submit" bsStyle="primary" className="center-block" disabled={!this.state.idCliente}>{'Buscar'}</Button>
											</div>
										</Collapse>
									</form>
									<Collapse in={!this.state.toggle}>
										<form onSubmit={this.submit}>
											<label className="control-label">{'Nombre'}</label>
											<p className="form-readonly">{this.props.customerData.cliente ? this.props.customerData.cliente.datosPersonales.nombre + " " + this.props.customerData.cliente.datosPersonales.apellidoPaterno + " " + this.props.customerData.cliente.datosPersonales.apellidoMaterno: ""}</p>
											<hr style={{marginTop:0}}/>
											<TextInput label="Número de VIN" name="noVIN" alphanum placeholder="Ingrese número de VIN" maxLength="17" value={this.state.noVIN} onChange={this.handleOnChangeNoVIN}/>
											<Button bsStyle="link" onClick={this.toggle}>{'Cancelar'}</Button>
											<Button type="submit" bsStyle="primary" className="pull-right" onClick={this.submit} disabled={this.state.noVIN.length<17}>{'Siguiente'}</Button>
										</form>
									</Collapse>
								</div>
							</Collapse>
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<Collapse in={this.state.complemento}><div><BPDataForm cancel={{action:this.cancelBP}} submit={{action:this.submitBP}} noVIN={{noVIN: this.state.noVIN}} isVisible={this.state.complemento} /></div></Collapse>
						</Col>
					</Row>
				</Tab>
				<Tab eventKey={2} title="2">
					<Row className="text-center">
						<Col xs={12}>
							<div className="icon-circle"><div className="icon-check-container"><div className={"icon-check " + this.state.anim}></div></div></div>
						</Col>
					</Row>
					<Row className="text-center">
						<Col xs={12}>
							<h3>Solicitud generada exitosamente</h3>
							<p><strong>{'Número de folio'}</strong></p>
							<p><strong>{this.state.folio}</strong></p>
							<p className="mb19">La solicitud ha sido enviada al proceso de inspección</p>
							<Button type="submit" bsStyle="primary" className="center-block"onClick={this.finalize}>{'Finalizar'}</Button>
						</Col>
					</Row>
				</Tab>
			</Tabs>
		);
	};
}

NewApplicationForm.propTypes = {
	actionsSinResguardo: React.PropTypes.object.isRequired,
	actionsLoading: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
	return {
		customerData: state.customerData
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsLoading: bindActionCreators(actionsLoading, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(NewApplicationForm);