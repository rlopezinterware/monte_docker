import React from "react";
import {connect} from "react-redux";
import {Tabs, Tab, Row, Col, Button} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import {MODULES, APPL, CONTEXTO, APP} from "../../../../../constants";

import * as actionsSinResguardo from "./../SinResguardoActions";
import * as actionsLoading from "../../../../loading/LoadingActions";
import * as actionsAlert from "../../../../alert/AlertActions";
import {bindActionCreators} from "redux";

import _ from 'underscore';

class FindApplicationForm extends React.Component {
	constructor(props,context) {
		super(props,context);
		this.state = { key: 1, idCliente: '', solicitudes: ''}

		this.cancel =this.cancel.bind(this);
		this.reloadAppls =this.reloadAppls.bind(this);
		this.handleSelect =this.handleSelect.bind(this);
		this.mapSolicitudes =this.mapSolicitudes.bind(this);
		this.buscaProspecto =this.buscaProspecto.bind(this);
		this.handleOnChangeClienteId = this.handleOnChangeClienteId.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.tab === 1) {
			this.cancel();
		}
	}

	handleOnChangeClienteId(event) {
		event.preventDefault();
		this.setState({idCliente: event.target.value});
	}

	handleSelect(key){ this.setState({key}); }

	cancel(){ this.setState({key:1, idCliente: '', solicitues: ''}); }

	mapSolicitudes(item,index){

		let appContext = CONTEXTO + APP;
		let statusNav = ()=>{};
		let statusTxt = '', statusClass = 'fa-clock-o text-warning', actionClass = '';
		let _this = this

		statusTxt = item.estadoFlujoBpm;

		switch (item.estadoFlujoBpmId) {

			case APPL.TO_INSP: 
				if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.INSPECCIONAR)){
					statusClass = 'fa-check text-success';
					actionClass = 'clickable';
					statusNav = ()=>{
						this.context.router.push(appContext + "/inspeccion")
						_this.props.actionsSinResguardo.getSolicitudSuccess({folio: item.folio});
					};
				}
				break;
			case APPL.TO_OFFER:
				if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.OFERTAR)){
					statusClass = 'fa-check text-success';
					actionClass = 'clickable';
					statusNav = ()=>{
						this.context.router.push(appContext + "/oferta")
						_this.props.actionsSinResguardo.getSolicitudSuccess({folio: item.folio});
					};
				}
				break;
			case APPL.TO_PAYMENT:
				statusClass = 'fa-usd text-success';
				if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.CONTRATAR)){
					actionClass = 'clickable';
					statusNav = ()=>{
						this.context.router.push(appContext + "/conclusion")
						_this.props.actionsSinResguardo.getSolicitudSuccess({folio: item.folio});
					};
				}
				break;
			case APPL.CLOSED:
					statusClass = 'fa-lock text-muted';
				break;
			default: 
				if (item.estadoSolicitudId === APPL.REJECTED) {
					statusTxt =  item.estadoSolicitud + ": " + item.motivoRechazo;
					statusClass = 'fa-close text-danger';
				} else {
					statusTxt = item.estadoFlujoBpm;
				}
				break;
		}

		return (
			<tr key={index} onClick={statusNav} className={actionClass}>
				<td><i className="fa fa-file-text"/> {item.folio}</td>
				<td><i className={"fa " + statusClass}/> {statusTxt}</td>
			</tr>
		);
	}

	reloadAppls(){
		this.props.actionsSinResguardo.getClienteSolicitud(this.state.idCliente).then(response => {
			let solicitudArray = []; 

			response.data.solicitudes.solicitud.map(solicitud => {
				if (this.props.actionsSinResguardo.getAuthorizationSolicitud(JSON.parse(sessionStorage.states), ''+solicitud.estadoFlujoBpmId)
					|| solicitud.estadoFlujoBpmId === APPL.REJECTED){
					solicitudArray.push(solicitud);
				}
			});
			
			this.setState({solicitudes: solicitudArray.map(this.mapSolicitudes)});
		});
	}

	buscaProspecto (e) {
		e.preventDefault();

		if (Number(this.state.idCliente) <= 2147483647 ) {
			this.props.actionsSinResguardo.resetAllStoreForASR().then(() => {
				this.props.actionsSinResguardo.getClienteById(this.state.idCliente, "3").then(() => {
					this.props.actionsSinResguardo.getClienteSolicitud(this.state.idCliente).then(response => {
						let solicitudArray = [];
						response.data.solicitudes.solicitud.map(solicitud => {
							if (this.props.actionsSinResguardo.getAuthorizationSolicitud(JSON.parse(sessionStorage.states), ''+solicitud.estadoFlujoBpmId)
								|| solicitud.estadoFlujoBpmId === APPL.REJECTED){
								solicitudArray.push(solicitud);
							}
						});
						this.setState({key: 2, solicitudes: solicitudArray.map(this.mapSolicitudes)});
					});
				});
			});
		} else {
			this.props.actionsAlert.showAlert({
				message: <h3 className="text-center">{'Cliente no existente'}</h3>,
				buttons: [
					{ onClick: () => {}, classCss: "btn-default", label: "Aceptar" },
				]
			});
		}
	}

	render() {
		
		return (
			<Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="busquedaSolicitudes">
				<Tab eventKey={1} title="1">
					<form onSubmit={this.buscaProspecto}>
						<Row>
							<Col sm={6} smOffset={3} md={4} mdOffset={4}>
								<TextInput label="ID de prospecto" name="idCliente" placeholder="Ingrese número de ID" maxLength={10} numeric value={this.state.idCliente} onChange={this.handleOnChangeClienteId}/>
							</Col>
						</Row>
						<Row>
								<Button type="submit" bsStyle="primary" className="center-block" disabled={!this.state.idCliente}>{'Buscar'}</Button>
						</Row>
					</form>
				</Tab>
				<Tab eventKey={2} title="2">
					<Row>
						<Col sm={4} lg={4} lgOffset={2}>
							<label className="control-label">{'ID de prospecto'}</label>
							<p className="form-readonly">{this.state.idCliente}</p>
							<label className="control-label">{'Nombre'}</label>
							<p className="form-readonly">{this.props.customerData.cliente ? this.props.customerData.cliente.datosPersonales.nombre + " " + this.props.customerData.cliente.datosPersonales.apellidoPaterno + " " + this.props.customerData.cliente.datosPersonales.apellidoMaterno: ""}</p>
						</Col>
						<Col sm={8} lg={6}>
							<label className="control-label">
								{'Solicitudes del prospecto'}
								<Button bsStyle="info" className="pull-right btn-sm btn-refresh" onClick={this.reloadAppls}>
									<i className="fa fa-refresh"></i>
								</Button>
							</label>
							<table className="tabla-solicitudes">
								<thead>
									<tr>
										<th>No. folio</th>
										<th>Status</th>
									</tr>
								</thead>
							</table>
							<div className="scroll-tabla-solicitudes">
								<table className="tabla-solicitudes mb25">
									<tbody>
										{this.state.solicitudes}
									</tbody>
								</table>
							</div>
						</Col>
					</Row>
					<Row>
						<Col lg={8} lgOffset={2}>
							<Button bsStyle="link" onClick={this.cancel}>{'Cancelar'}</Button>
						</Col>
					</Row>
				</Tab>
			</Tabs>
		);
	}
}

FindApplicationForm.propTypes = {
	app: React.PropTypes.object,
	actionsSinResguardo: React.PropTypes.object.isRequired,
	actionsLoading: React.PropTypes.object.isRequired
};

FindApplicationForm.contextTypes = {
	router: React.PropTypes.object
};

const mapStateToProps = (state, ownProps) => {

	return {
		tab: ownProps.tab,
		navApp: ownProps.navApp,
		customerData: state.customerData
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsLoading: bindActionCreators(actionsLoading, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(FindApplicationForm);
