import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {Grid, Row, Col, Button, FormGroup, ControlLabel, FormControl} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import RadioGroupInput from "../../../../common/RadioGroupInput";
import SelectInput from "../../../../common/SelectInput";
import DateInput from "../../../../common/DateInput";

import {SPLIT} from "../../../../../constants";

import * as actionsSinResguardo from "../../../../layout/body/sinResguardo/SinResguardoActions";
import * as actionsLoading from "../../../../loading/LoadingActions";

import catEstados from "../../../../catalogs/catEstados"
import catActEconomica from "../../../../catalogs/catActEconomica"
import catRelAccionista from "../../../../catalogs/catRelacionAccionista"
import catSector from "../../../../catalogs/catSector"
import catGiro from "../../../../catalogs/catGiro"

import * as selectors from "../selectors/selectors"

import * as actionsAlert from "../../../../alert/AlertActions";

class BPDataForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			validate: false,
			isVisible: false,
			form:{}
		};
		this.onSubmit = this.onSubmit.bind(this);
		this.onSelectChange = this.onSelectChange.bind(this);
		this.onInputChange = this.onInputChange.bind(this);
		this.cancelar = this.cancelar.bind(this);
	}

	cancelar (event) {
		let _this = this;

		this.props.actionsAlert.showAlert({
			message: <h3 className="text-center">¿Desea cancelar el registro?</h3>,
			buttons: [
				{
					onClick: () => {
					},
					classCss: "btn-default",
					label: "No"
				},
				{
					onClick: () => {
						_this.setState({form:{}, validate: false});
						_this.props.cancel.action();			
					},
					classCss: "btn-danger",
					label: "Si"
				}
			]
		});
	}

	onSubmit(event) {
		event.preventDefault();

		if (!this.state.form.Nombre||
			!this.state.form.Curp||
			!this.state.form.RegioNac||
			!this.state.form.LocalidadPld ||
			!this.state.form.Calle1||
			!this.state.form.Calle2||
			!this.state.form.FechaVivienda||
			!this.state.form.FechaIngreso||
			!this.state.form.Actividad||
			!this.state.form.SectorNmp||
			!this.state.form.GiroCnbv||
			 this.state.form.Accionista===undefined||
			 this.state.form.RecursoLegal===undefined||
			 this.state.form.ParientePpe===undefined||
			 this.state.form.PersonaPolitica===undefined||
			 this.state.form.NegocioPropio===undefined||
			 this.state.form.ActPropia===undefined||
			!this.state.form.AccionistaPa||
			(this.state.form.Dependientes === "" || this.state.form.Dependientes === undefined)) {

			this.setState({validate: true});

		} else {

			let formBP = this.state.form;

			selectors.clearNullAndEmpty(formBP);

			this.props.actionsSinResguardo.newClienteBP(formBP).then(() => {
		
 	 			let newSolicitud = { idProducto: 601,
					  				 nombreProducto:"Autos sin resguardo", 
									 idSubProducto:"AUTOSIN", 
									 numeroCliente : this.state.form.IdCliente, 
									 tipoVehiculo : 1,
									 vin : this.props.noVIN};

				this.props.actionsSinResguardo.newSolicitud(newSolicitud).then(response => {
					
					if(this.props.submit && this.props.submit.action) {
						this.props.submit.action(event, response);
						this.setState({form: {}, validate: false});
					}
				});
			});
		}
	}

	componentWillReceiveProps(nextProps) {

		if(!nextProps.isVisible) {this.setState({isVisible: false});}

		if(nextProps.isVisible && !this.state.isVisible) {

			let {datosCuenta, datosPersonales, datosContacto, beneficiarios, calificacionCredito} = nextProps.customerData.cliente;

			let direccion;
			datosContacto.direcciones && datosContacto.direcciones.direccion.map(item => {
				
				if (item.tipoDomicilio !== 4){
					direccion = item;
				}
			});

			let casa, celular, trabajo;
			datosContacto.telefonos && datosContacto.telefonos.telefono.map(item => {
				if (item.tipoTelefono === "casa"){
					casa = item;
				}
				if (item.tipoTelefono === "celular"){
					celular = item;
				}
				if (item.tipoTelefono === "oficina"){
					trabajo = item;
				}
			});

			let email;
			datosContacto.correosElectronicos && datosContacto.correosElectronicos.correoElectronico.map(item => {
				if (item.tipoCorreo === "Principal"){
					email = item;
				}
			});
			
			let form = {
				Apematerno: datosPersonales.apellidoMaterno,
				Apepaterno: datosPersonales.apellidoPaterno,
				Calle: direccion ? direccion.nombreDelaCalle : null,
				CapacidadPago: calificacionCredito ? calificacionCredito.capacidadPago : null,
				CatRiesgo: 1,
				Celular: celular ? celular.codigoArea + celular.numeroTelefonico : null,
				Clabe: null,
				ClaveCol: direccion ? this.props.actionsSinResguardo.getValueForSAP(direccion.colonia.clave) : null,
				CompPago: null,
				Comportamiento: null,
				Cp: direccion ? direccion.codigoPostal : null,
				Desempenio: calificacionCredito ? calificacionCredito.porcentajeDesempeño : null,
				Email: email ? email.direccionCorreo : null,
				Escolaridad: this.props.actionsSinResguardo.getValueForSAP(datosPersonales.escolaridad.clave),
				Estado: direccion ? this.props.actionsSinResguardo.getValueForSAP(direccion.estado.clave): null,
				Estadocivil: this.props.actionsSinResguardo.getValueForSAP(datosPersonales.estadoCivil.clave),
				ExtensionTel: trabajo ? trabajo.ext : null,
				Fechanac: datosPersonales.fechaNacimiento.substring(0, 10),
				GiroCnbv: null,
				IdCliente: datosCuenta.idCliente,
				ListaNegra: null,
				LugarTrabajo: null,
				Nacionalidad: this.props.actionsSinResguardo.getValueForSAP(datosPersonales.nacionalidad.clave),
				Nombre: datosPersonales.nombre,
				NumCuenta: null,
				Numeroext: direccion ? direccion.numeroExterior : null,
				Numeroint: direccion ? direccion.numeroInterior : null,
				PagosNmp: null,				
				Pais: this.props.actionsSinResguardo.getValueForSAP(datosPersonales.paisResidencia.clave),
				PaisNac: "MX",
				Pi: calificacionCredito ? calificacionCredito.probabilidadIncumplimiento : null,
				PrestaPromValu: calificacionCredito ? calificacionCredito.prestamoPromedioValuacion: null,
				PrestamoProm: calificacionCredito ? calificacionCredito.prestamoPromedio : null,
				PrestamosNmp: calificacionCredito ? calificacionCredito.prestamos : null,
				Rfc: datosPersonales.rfc,
				Sexo: this.props.actionsSinResguardo.getValueForSAP(datosPersonales.genero.clave),
				Siva: calificacionCredito ? calificacionCredito.calificacionSiva2 : null,
				TelLabora: trabajo ? trabajo.codigoArea + trabajo.numeroTelefonico : null,
  				Telefono: casa ? casa.codigoArea + casa.numeroTelefonico : celular ? celular.codigoArea + celular.numeroTelefonico : trabajo ? trabajo.codigoArea + trabajo.numeroTelefonico : null,
  				Telefono2 : null,
				TipoCuenta: null,
  				TipoVivienda: direccion ? this.props.actionsSinResguardo.getValueForSAP(direccion.tipoVivienda.clave) : null,
  				Tipopersona: this.props.actionsSinResguardo.getValueForSAP(datosPersonales.tipoPersona.clave),
				Town: direccion ? this.props.actionsSinResguardo.getValueForSAP(direccion.delegacionMunicipio.clave) : null,
			};

			this.props.actionsSinResguardo.getListOfLocalidadPLD(this.props.actionsSinResguardo.getValueForSAP(direccion.estado.clave));
			this.setState({form, isVisible: nextProps.isVisible});
		}
	}

	onInputChange(e) {
		let temp = {...this.state.form};
		temp[e.target.name] = e.target.value;
		this.setState({form: temp});
	}

	onSelectChange(selected) {
		let [name,id] = selected.split(SPLIT);		
		let temp = {...this.state.form};
		temp[name] = id;
		this.setState({form: temp});	
	}

	render() {
		return (
			<form>
				<Row>
					<Col xs={12}><h2 className="mb25">{'Nombre'}</h2></Col>
					<Col sm={4}><TextInput label="Primer nombre" name="Nombre" maxLength={40} onChange={this.onInputChange} value={this.state.form.Nombre||""} alpha required error={this.state.validate}/></Col>
					<Col sm={4}><TextInput label="Segundo nombre" name="Nombre2" maxLength={40} onChange={this.onInputChange} alpha value={this.state.form.Nombre2||""}/></Col>
					<Col sm={4}><TextInput label="Apellido paterno" name="Apepaterno" value={this.state.form.Apepaterno||""} readOnly/></Col>
					<Col sm={4}><TextInput label="Apellido materno" name="Apematerno" value={this.state.form.Apematerno||""} readOnly/></Col>
					<Col sm={4}><TextInput label="CURP" name="Curp" maxLength={18} onChange={this.onInputChange} value={this.state.form.Curp||""} alphanum required error={this.state.validate}/></Col>
				</Row>
				<hr/>
				<Row>
					<Col xs={12}><h2 className="mb25">{'Lugar de nacimiento'}</h2></Col>
					<Col sm={4}><TextInput label="País" name="PaisNac" value={this.state.form.PaisNac||""} readOnly/></Col>
					<Col sm={4}>
						<SelectInput
							label="Estado de nacimiento"
							name="RegioNac"
							value={this.state.form.RegioNac}
							options={catEstados}
							onChange={this.onSelectChange}
							required error={this.state.validate}/>
					</Col>
				</Row>
				<hr/>
				<Row>
					<Col xs={12}><h2 className="mb25">{'Domicilio'}</h2></Col>
					<Col sm={4}><TextInput label="Estado" name="Estado" onChange={this.onInputChange} value={this.state.form.Estado||''} readOnly/></Col>
					<Col sm={4}>
						<SelectInput
							label="Localidad PLD"
							name="LocalidadPld"
							value={this.state.form.LocalidadPld}
							options={this.props.localidadpld}
							onChange={this.onSelectChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}><TextInput label="Calle" name="Calle" onChange={this.onInputChange} value={this.state.form.Calle||''} readOnly/></Col>
					<Col sm={4}><TextInput label="Entre calle 1" name="Calle1" maxLength={40} onChange={this.onInputChange} value={this.state.form.Calle1||''} required error={this.state.validate}/></Col>
					<Col sm={4}><TextInput label="Entre calle 2" name="Calle2" maxLength={40} onChange={this.onInputChange} value={this.state.form.Calle2||''} required error={this.state.validate}/></Col>
					<Col sm={4}><TextInput label="Manzana" name="Manzana" maxLength={50} onChange={this.onInputChange} value={this.state.form.Manzana||''}/></Col>
					<Col sm={4}><TextInput label="Lote" name="Lote" maxLength={50} onChange={this.onInputChange} value={this.state.form.Lote||''}/></Col>
					<Col sm={4}><DateInput label="Antigüedad habitando el domicilio" name="FechaVivienda" onChange={this.onInputChange} value={this.state.form.FechaVivienda||''} required error={this.state.validate} maxDate="now"/></Col>
				</Row>
				<hr/>
				<Row>
					<Col xs={12}><h2 className="mb25">{'Información laboral'}</h2></Col>
					<Col sm={4}>
						<RadioGroupInput
							label="¿Labora por cuenta propia?"
							name="ActPropia"
							value={this.state.form.ActPropia}
							value1={"X"} option1={" Si"}
							value2={""} option2={" No"}
							onChange={this.onInputChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<RadioGroupInput
							label="¿Cuenta con algún negocio propio?"
							name="NegocioPropio"
							value={this.state.form.NegocioPropio}
							value1={"X"} option1={" Si"}
							value2={""} option2={" No"}
							onChange={this.onInputChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<SelectInput
							label="Actividad económica"
							name="Actividad"
							value={this.state.form.Actividad}
							onChange={this.onSelectChange}
							options={catActEconomica}
							required error={this.state.validate}/>
					</Col>
				</Row>
				<Row>
					<Col sm={4}>
						<SelectInput
							label="Sector"
							name="SectorNmp"
							value={this.state.form.SectorNmp}
							onChange={this.onSelectChange}
							options={catSector}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<SelectInput
							label="Ocupación"
							name="GiroCnbv"
							value={this.state.form.GiroCnbv}
							onChange={this.onSelectChange}
							options={catGiro[this.state.form.SectorNmp]||[]}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}><DateInput label="Antigüedad en el empleo actual" name="FechaIngreso" onChange={this.onInputChange} value={this.state.form.FechaIngreso||''} required error={this.state.validate}/></Col>
				</Row>
				<hr/>
				<Row>
					<Col xs={12}><h2 className="mb25">{'Prevención de lavado de dinero'}</h2></Col>
					<Col sm={4}>
						<RadioGroupInput
							label="¿Todos sus recursos son legales?"
							name="RecursoLegal"
							value={this.state.form.RecursoLegal}
							value1={"X"} option1={" Si"}
							value2={""} option2={" No"}
							onChange={this.onInputChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<RadioGroupInput
							label="¿Tiene algún puesto político?"
							name="PersonaPolitica"
							value={this.state.form.PersonaPolitica}
							value1={"X"} option1={" Si"}
							value2={""} option2={" No"}
							onChange={this.onInputChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<RadioGroupInput
							label="¿Tiene algún pariente PPE?"
							name="ParientePpe"
							value={this.state.form.ParientePpe}
							value1={"X"} option1={" Si"}
							value2={""} option2={" No"}
							onChange={this.onInputChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<RadioGroupInput
							label="¿Es accionista de La Paz?"
							name="Accionista"
							value={this.state.form.Accionista}
							value1={"X"} option1={" Si"}
							value2={""} option2={" No"}
							onChange={this.onInputChange}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}>
						<SelectInput
							label="¿Tiene relación con un accionista de La Paz?"
							name="AccionistaPa"
							value={this.state.form.AccionistaPa}
							onChange={this.onSelectChange}
							options={catRelAccionista}
							required error={this.state.validate}/>
					</Col>
					<Col sm={4}><TextInput label="Dependientes económicos" name="Dependientes" onChange={this.onInputChange} value={this.state.form.Dependientes||''} required error={this.state.validate} numeric maxLength={2}/></Col>
				</Row>
				<hr/>
				<Row>
					<Col>
						<Button bsStyle="link" className="" onClick={this.cancelar}>{'Cancelar registro'}</Button>
						<Button bsStyle="primary" className="pull-right" onClick={this.onSubmit}>{'Generar folio'}</Button>
					</Col>
				</Row>
			</form>
		);
	}
}

BPDataForm.propTypes = {
	customerData: React.PropTypes.object,
	onChange: React.PropTypes.func
};

const mapStateToProps = (state, ownProps) => {
	let customerData = {...state.customerData};

	if(!state.customerData.Cliente){
		customerData.Cliente = {};
	}

	return {
		customerData,
		submit: ownProps.submit,
		noVIN: ownProps.noVIN.noVIN,
		localidadpld: state.localidadpld,
		isVisible: ownProps.isVisible
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch),
		actionsLoading: bindActionCreators(actionsLoading, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(BPDataForm);
