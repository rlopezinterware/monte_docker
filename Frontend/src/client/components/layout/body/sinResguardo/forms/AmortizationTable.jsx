import {Row, Col, Table, Pagination, Button} from "react-bootstrap";
import numeral from "numeral";
import moment from "moment";
import React from "react";
import {connect} from "react-redux";
import LOGO_SVG from "../../../../../svg/logo.svg";
import * as selectors from "../selectors/selectors"
import * as actionsSinResguardo from "../SinResguardoActions";
import {bindActionCreators} from "redux";

class AmortizationTable extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			pagActual: 1,
			visibles: [],
			numItems: 1
		}

		this.update = this.update.bind(this);
		this.imprimirCotizacion = this.imprimirCotizacion.bind(this);
	}

	componentWillReceiveProps(nextProps) {

		if (nextProps.data !== this.props.data) {

			let items = parseInt(nextProps.data.length/nextProps.numItems);
			let rem = nextProps.data.length%nextProps.numItems;

			this.setState({
				pagActual: 1,
				visibles: nextProps.data.length? nextProps.data.slice(0,nextProps.numItems):[],
				numItems: rem ?  items + 1 : items
			});
		}
	}

	update(page) {
		let inicio = (page - 1) * this.props.numItems;
		let fin = page * this.props.numItems;
		this.setState({visibles: this.props.data.slice(inicio,fin),pagActual: page});
	}


	imprimirCotizacion () {

		const documentData = {
			PARAM_LOGO: LOGO_SVG,
			PARAM_FECHA: moment(new Date()).locale("es").format("DD [DE] MMMM [DE] YYYY", "es").toUpperCase(),
			PARAM_TASA_FIJA: this.props.credito.tasaInteres,
			PARAM_MONTO: numeral(this.props.credito.montoSolicitado).format(" 0,0.00"),
			PARAM_PLAZO: this.props.credito.plazo,
			PARAM_PROMEDIO: Number(this.props.credito.cat).toFixed(2),
			PARAM_TABLA: selectors.tablaAmortizacionFormatted(this.props.data)
		};
		
		this.props.actionsSinResguardo.getTemplate("cotizacion").then(response => {

			let filledDocument;
			filledDocument = response;

			for (let key in documentData) {
				let find = "{" + key + "}";
				let re = new RegExp(find, "g");
				filledDocument = filledDocument.replace(re, documentData[key]);
			}

			let doc = document.getElementById("iframe").contentWindow.document;
			doc.open();
			doc.write(filledDocument);
			doc.close();

			window.frames["iframe"].focus();
			setTimeout(() => {
				window.frames["iframe"].print();
			}, 1000);
		});
	}

	render() {

		let rows = this.state.visibles.map((element,index)=>{
			return (
			<tr key={'a'+index}>
				<td className={'text-center'}>{element.numeroPago}</td>
				<td className={'text-center'}>{element.fecha}</td>
				<td className={'text-center'}>{numeral(element.abonoTotal).format('$0,000.00')}</td>
				<td className={'text-center'}>{numeral(element.abonoCapital).format('$0,000.00')}</td>
				<td className={'text-center'}>{numeral(element.interes).format('$0,000.00')}</td>
				<td className={'text-center'}>{numeral(element.iva).format('$0,000.00')}</td>
				<td className={'text-center'}>{numeral(element.capitalFinal).format('$0,000.00')}</td>
				<td className={'text-center'}>{numeral(element.saldoPagare).format('$0,000.00')}</td>
			</tr>
			)
		})
		return (
			<Row>
				<Col xs={12}>
					<Table responsive striped>
						<thead>
							<tr>
								<th>No.<br/>pagos</th>
								<th>Fecha<br/>de pago</th>
								<th>Monto<br/>de pago</th>
								<th>Capital</th>
								<th>Interés</th>
								<th>I.V.A.<br/>de intereses</th>
								<th>Saldo capital</th>
								<th>Saldo<br/>con intereses</th>
							</tr>
						</thead>
						<tbody>
							{rows}
						</tbody>
						{false && <tfoot>
							<tr>
								<td></td>
								<td>TOTALES</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tfoot>}
					</Table>
					<iframe id="iframe" name="iframe" width="0" height="0" frameborder="0" style={{display: 'none'}}></iframe>
				</Col>
				<Col xs={12}><p className="small table-well">LAS CANTIDADES, TASAS DE INTERÉS, FECHAS, PLAZOS Y DEMÁS REFERENCIAS QUE SE SEÑALAN EN ESTE DOCUMENTO SON MERAMENTE INFORMATIVOS PARA EL DÍA EN QUE SE EXPIDE, SIN QUE GENERE OBLIGACIÓN A CARGO DE SOCIEDAD DE AHORRO Y CRÉDITO LA PAZ, S.A DE C.V., S.F.P PARA APLICARLOS AL DÍA EN QUE SE CONCRETE UNA OPERACIÓN ENTRE ESTA INSTITUCIÓN Y EL SOLICITANTE DEL EJERCICIO DE CRÉDITO</p></Col>
				<Col xs={12}>
					{this.props.imprimir && <Button bsStyle={'default'} bsSize={'small'} onClick={this.imprimirCotizacion}><i className={'fa fa-print'}/> Imprimir</Button>}
					{this.state.numItems>1 && <Pagination className={'pull-right'} prev next
						items={this.state.numItems}
				    activePage={this.state.pagActual}
				    onSelect={this.update} />}
				</Col>
			</Row>
		);
	};
}

//prev next first last ellipsis boundaryLinks

const mapStateToProps = (state, ownProps) => {
	
	return {
		data: ownProps.data,
		credito: ownProps.credito,
		items: ownProps.items,
		imprimir: ownProps.imprimir
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(AmortizationTable);