import React from "react";
import {Grid, Row, Col, FormGroup, ControlLabel} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import TextareaInput from "../../../../common/TextareaInput";
import SelectInput from "../../../../common/SelectInput";
import CurrencyInput from "../../../../common/CurrencyInput";
import Labels from "../../../../../Labels";
import moment from "moment";
import numeral from "numeral";
import {ASR} from "../../../../../Labels";
import {AUTO, SPLIT} from "../../../../../constants";

const ValuerForm = ({onChange, cotizacion, vehiculo, validate}) => {
	let valorFinal = 0;
	if (cotizacion) {
		valorFinal = cotizacion.montoAvaluo?(Number(cotizacion.montoAvaluo) - Number(cotizacion.montoCastigo?cotizacion.montoCastigo:0)):0
	}
	return (
		<form>
		<Row className={'mb5'}>
			<Col sm={6} md={3}>
				<CurrencyInput required
					error={validate}
					name={'montoCastigo'}
					min={0}
					max={cotizacion?cotizacion.montoAvaluo:undefined}
					label={'Castigo dictaminador'}
					value={cotizacion ? cotizacion.montoCastigo : ''}
					onChange={onChange}/>
			</Col>
			<Col sm={6} md={3}>
				<TextInput disabled
					name={'porcentajeCastigo'}
					label={'Porcentaje de castigo'}
					readOnly
					value={cotizacion ?  (Number(cotizacion.montoCastigo) * 100 / Number(cotizacion.montoAvaluo)).toFixed(2) + ' %'  :  numeral(0).format('0.00 %')} />
			</Col>
			<Col sm={12} md={6}>
				<Row>
					<Col sm={6}>
						<ControlLabel htmlFor="">{'Valor vehículo'}</ControlLabel>
						<div className="montoCredito">{numeral(cotizacion ? cotizacion.montoAvaluo : 0).format("$ 0,0.00")}</div>
					</Col>
					<Col sm={6}>
						<ControlLabel htmlFor="">{'Valor final'}</ControlLabel>
						<div className="montoCredito">{numeral(cotizacion ? valorFinal : 0).format("$ 0,0.00")}</div>
					</Col>
				</Row>
			</Col>
		</Row>
		<Row>
			<Col md={6}>
				<TextareaInput
					name={'comentarios'}
					label={'Comentarios'}
					value={vehiculo ? vehiculo.comentarios : ''}
					error={validate}
					maxLength={500}
					rows={4}
					required
					onChange={onChange}/>
			</Col>
		</Row>
		</form>
	);
};

ValuerForm.propTypes = {
	onChange: React.PropTypes.func,
	vehiculo: React.PropTypes.object,
	validate: React.PropTypes.bool,
	cotizacion: React.PropTypes.object
};

export default ValuerForm;
