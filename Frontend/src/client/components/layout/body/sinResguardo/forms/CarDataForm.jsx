import React from "react";
import {Button} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {Grid, Row, Col, FormGroup} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import SelectInput from "../../../../common/SelectInput";
import MultiSelectInput from "../../../../common/MultiSelectInput";

import Labels from "../../../../../Labels";
import {ASR} from "../../../../../Labels";
import {AUTO, SPLIT, DESCRIPTION} from "../../../../../constants";

import * as actionsSinResguardo from "../../../../layout/body/sinResguardo/SinResguardoActions";
import * as actionsLoading from "../../../../loading/LoadingActions";

class CarDataForm extends React.Component {

	constructor(props) {	
		super(props);
		this.state = { validate: false};
		this.onSubmit = this.onSubmit.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleDescription = this.handleDescription.bind(this);
	}	

	componentWillMount() {
		if (!this.props.marcas.length) {
			this.props.actionsSinResguardo.getListOfMarcas();
		}
	}

	onSubmit(e) {
		e.preventDefault();

		if (this.props.auto.equipoAdicional && (this.props.kilometrajes.length === 0 || this.props.auto.kilometraje)) {
			this.props.submit.action();
			this.setState({validate:false });
		} else {
			this.setState({validate:true });
		}
	}

	handleDescription(data, name) {

		switch (name) {
			case AUTO.ATRIBUTES.MARCA:
				data[name + DESCRIPTION] = this.props.actionsSinResguardo.getDescription(data[name], this.props.marcas); break;
			case AUTO.ATRIBUTES.SUB_MARCA:
				data[name + DESCRIPTION] = this.props.actionsSinResguardo.getDescription(data[name], this.props.submarcas); break;
			case AUTO.ATRIBUTES.MODELO:
				data[name + DESCRIPTION] = this.props.actionsSinResguardo.getDescription(data[name], this.props.modelos); break;
			case AUTO.ATRIBUTES.VERSION:
				data[name + DESCRIPTION] = this.props.actionsSinResguardo.getDescription(data[name], this.props.versiones); break;
			case AUTO.ATRIBUTES.KILOMETRAJE:
				data[name + DESCRIPTION] = this.props.actionsSinResguardo.getDescription(data[name], this.props.kilometrajes); break;
			case AUTO.ATRIBUTES.EQUIPO_ADICIONAL:
				let array = data[name].split(",");
				let complementosDescription = array.map(complemento => {
					return this.props.actionsSinResguardo.getDescription(complemento, this.props.complementos);
				});
				data[name + DESCRIPTION] = complementosDescription.join("#"); break;
			default: break;
		}
	}

	handleChange(selected) {

		if (selected === undefined) { return;}

		const [name, value] = selected.split(SPLIT);

		let data = Object.assign({}, this.props.auto);

		if (!value) {
			data[AUTO.ATRIBUTES.EQUIPO_ADICIONAL] = selected;
			this.handleDescription(data, AUTO.ATRIBUTES.EQUIPO_ADICIONAL);
		} else {
			data[name] = parseInt(value);
			this.handleDescription(data, name);

			if (AUTO.ATRIBUTES.MODELO === name){
				data["anio"] = this.props.actionsSinResguardo.getAnio(parseInt(value), this.props.modelos);
			}
		}

		switch (name) {
			case AUTO.ATRIBUTES.MARCA:
				data[AUTO.ATRIBUTES.SUB_MARCA] = parseInt(0);
				data[AUTO.ATRIBUTES.SUB_MARCA + DESCRIPTION] = "";
				if (this.props.submarcas.length) {
					this.props.actionsSinResguardo.getListOfSubMarcasSuccess();
				}
			case AUTO.ATRIBUTES.SUB_MARCA:
				data[AUTO.ATRIBUTES.MODELO] = parseInt(0);
				if (this.props.modelos.length) {
					this.props.actionsSinResguardo.getListOfModelosSuccess();
				}
			case AUTO.ATRIBUTES.MODELO:
				data[AUTO.ATRIBUTES.VERSION] = parseInt(0);
				if (this.props.versiones.length) {
					this.props.actionsSinResguardo.getListOfVersionesSuccess();
				}
			case AUTO.ATRIBUTES.VERSION:
				data[AUTO.ATRIBUTES.KILOMETRAJE] = parseInt(0);
				data[AUTO.ATRIBUTES.EQUIPO_ADICIONAL] = undefined;
				if (this.props.complementos.length) {
					this.props.actionsSinResguardo.getListOfComplementosSuccess();
				}
				if (this.props.kilometrajes.length) {
					this.props.actionsSinResguardo.getListOfKilometrajeSuccess();
				}
			default: break;
		}

		this.props.actionsSinResguardo.updateStateAuto(data);

		switch (name) {
			case AUTO.ATRIBUTES.MARCA:
				this.props.actionsSinResguardo.getListOfSubmarcas(data.marca)
				.then(() => {
					if (this.props.submarcas.length === 1) {
						let changeVal = AUTO.ATRIBUTES.SUB_MARCA + SPLIT + this.props.submarcas[0].id
						this.handleChange(changeVal);
					}
				});
				break;
			case AUTO.ATRIBUTES.SUB_MARCA:
				this.props.actionsSinResguardo.getListOfModelos(data.marca, data.submarca)
				.then(() => {
					if (this.props.modelos.length === 1) {
						let changeVal = AUTO.ATRIBUTES.MODELO + SPLIT + this.props.modelos[0].id
						this.handleChange(changeVal);
					}
				});
				break;
			case AUTO.ATRIBUTES.MODELO:
				this.props.actionsSinResguardo.getListOfVersiones(data.marca, data.submarca, data.modelo)
				.then(() => {
					if (this.props.versiones.length === 1) {
						let changeVal = AUTO.ATRIBUTES.VERSION + SPLIT + this.props.versiones[0].id
						this.handleChange(changeVal);
					}
				});
				break;
			case AUTO.ATRIBUTES.VERSION:
				this.props.actionsSinResguardo.getListOfKilometrajeComplementos(data)
				break;
			default:
				break;
		}
	}

	render() {
		return (
			<form onSubmit={this.onSubmit}>
				<Row className={'mb5'}>
					<Col sm={6} md={4}>
						<SelectInput
							autoFocus={true}
							name={AUTO.ATRIBUTES.MARCA}
							label={ASR.FORMS.AUTO.MARCA}
							value={this.props.auto.marca}
							options={this.props.marcas}
							required
							error={this.state.validate||false}
							onChange={this.handleChange}/>
					</Col>
					<Col sm={6} md={4}>
						<SelectInput
							name={AUTO.ATRIBUTES.SUB_MARCA}
							label={ASR.FORMS.AUTO.SUB_MARCA}
							value={this.props.auto.submarca}
							options={this.props.submarcas}
							required
							error={this.state.validate||false}
							onChange={this.handleChange}/>
					</Col>
					<Col sm={6} md={4}>
						<SelectInput
							name={AUTO.ATRIBUTES.MODELO}
							label={ASR.FORMS.AUTO.MODELO}
							value={this.props.auto.modelo}
							options={this.props.modelos}
							required
							error={this.state.validate||false}
							onChange={this.handleChange}/>
					</Col>
					<Col sm={6} md={4}>
						<SelectInput
							name={AUTO.ATRIBUTES.VERSION}
							label={ASR.FORMS.AUTO.VERSION}
							value={this.props.auto.version}
							options={this.props.versiones}
							required
							error={this.state.validate||false}
							onChange={this.handleChange}/>
					</Col>
					<Col sm={6} md={4}>
						<SelectInput
							name={AUTO.ATRIBUTES.KILOMETRAJE}
							label={ASR.FORMS.AUTO.KILOMETRAJE}
							value={this.props.auto.kilometraje}
							required={this.props.kilometrajes.length == 0 && this.props.complementos == 0 ? true : this.props.kilometrajes.length > 0 ? true : false}
							placeholder={this.props.kilometrajes.length == 0 && this.props.complementos == 0 ? null : this.props.kilometrajes.length > 0 ? null : "No Requerido"}
							options={this.props.kilometrajes}
							error={this.state.validate||false}
							onChange={this.handleChange}/>
					</Col>
					<Col sm={6} md={4}>
						<MultiSelectInput 
							name={AUTO.ATRIBUTES.EQUIPO_ADICIONAL}
							label={'Equipo Adicional'}
							required
							error={this.state.validate||false}
							value={this.props.auto.equipoAdicional}
							options={this.props.complementos}
							onChange={this.handleChange}/>
					</Col>
				</Row>
				<Row>
					<Col sm={12}>
						{this.props.cancel && <Button bsStyle="link" onClick={this.props.cancel.action}>{this.props.cancel.label}</Button>}
						<Button
							type="submit"
							bsStyle="primary"
							className="pull-right">
							{this.props.submit.label} 
						</Button>
					</Col>
				</Row>
			</form>
		);
	}
}

CarDataForm.propTypes = {
	customerData: React.PropTypes.object,
	onChange: React.PropTypes.func
};

const mapStateToProps = (state, ownProps) => {

	return {
		auto: state.auto,
		marcas: state.marcas,
		submarcas: state.submarcas,
		modelos: state.modelos,
		versiones: state.versiones,
		kilometrajes: state.kilometrajes,
		complementos: state.complementos,
		submit: ownProps.submit,
		cancel: ownProps.cancel
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsLoading: bindActionCreators(actionsLoading, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CarDataForm);
