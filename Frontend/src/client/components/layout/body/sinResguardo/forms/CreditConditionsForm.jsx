import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {Collapse, Fade, Grid, Row, Col, FormGroup, ControlLabel, Button} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import SelectInput from "../../../../common/SelectInput";
import CurrencyInput from "../../../../common/CurrencyInput";
import {ASR} from "../../../../../Labels";
import numeral from "numeral";
import * as selectors from "../selectors/selectors"

import {SPLIT} from "../../../../../constants";
import * as actionsSinResguardo from "../SinResguardoActions";
import AmortizationTable from "../forms/AmortizationTable"

class CreditForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			key: 1,
			showTable: false
		}

		this.handleOnChangeMonto = this.handleOnChangeMonto.bind(this);
		this.handleOnChangePlazo = this.handleOnChangePlazo.bind(this);
		this.handleOnChangeSubProducto = this.handleOnChangeSubProducto.bind(this);
		this.showTable = this.showTable.bind(this);
		this.cancel = this.cancel.bind(this);
	}

	componentWillReceiveProps(nextProps) {

		if(!nextProps.tablaAmortizacion.length) {
			this.setState({showTable: false});
		}

		if (!this.props.credito.producto && nextProps.productos.length) {

			let data = Object.assign({}, nextProps.credito);
			
			data["plazo"] = parseInt(nextProps.plazos[0].id);
			data["tasaInteres"] = nextProps.plazos[0].tasa;
			data["frecuencia"] = nextProps.plazos[0].frecuencia;
			data["producto"] = parseInt(nextProps.productos[0].id);
			data["subProducto"] = nextProps.productos[0].subProducto;
			
			this.props.actionsSinResguardo.updateStateCredito(data);
		}
	}

	resetTablaAmortizacion(){
		this.props.actionsSinResguardo.resetTablaAmortizacionSuccess();
	}

	handleOnChangeMonto(event) {
		event.preventDefault();

		this.resetTablaAmortizacion();

		let data = Object.assign({}, this.props.credito);
		data["montoSolicitado"] = numeral(event.target.value).value();

		this.props.actionsSinResguardo.updateStateCredito(data);
	}

	handleOnChangeSubProducto (selected){

		if (selected === undefined) {
			return;
		}

		const [name, value] = selected.split(SPLIT);

		let data = Object.assign({}, this.props.credito);
		
		data["producto"] = value;
		let subProducto = this.props.actionsSinResguardo.getSubproducto(data, Object.assign({}, this.props.plazos));
		data["subProducto"] = subProducto.subProducto;
		this.props.actionsSinResguardo.updateStateCredito(data);

		this.resetTablaAmortizacion();
	}

	handleOnChangePlazo(selected) {

		if (selected === undefined) {
			return;
		}

		this.resetTablaAmortizacion();
		const [name, value] = selected.split(SPLIT);
		this.props.actionsSinResguardo.getCotizacion (this.props.credito, this.props.plazos, value, this.props.anio);
	}

	showTable () {
		let data = Object.assign({}, this.props.credito);

		if ((Number(data.montoMinimo) <= Number(data.montoSolicitado)) && (Number(data.montoSolicitado) <= Number(data.montoMaximo))) {
			this.props.actionsSinResguardo.generateTablaAmortizacion(this.props.credito).then(() => {
				this.setState({showTable: true });
			});
		}
	}

	cancel() {
		this.props.cancel();
	}

	render () {
		return (
			<div>
				<Row className={'mb19'}>
					<Col sm={6} md={4}>
						<ControlLabel htmlFor="">Monto máximo de crédito<br/><span>&nbsp;</span></ControlLabel>
						{!this.props.error && <div className="montoCredito">{this.props.credito ? numeral(this.props.credito.montoMaximo).format("$ 0,0.00") : ""}</div>}
						{this.props.error && <div className="montoError">{this.props.error}</div>}
					</Col>
					<Col sm={6} md={4}>
						<CurrencyInput
							name={'prestamo'}
							label={ASR.FORMS.CREDITO.PRESTAMO }
							subLabel={ASR.FORMS.CREDITO.MONTO_MINIMO + " " + numeral(this.props.credito.montoMinimo).format('$ 0,0.00')  } 
							max={this.props.credito.montoMaximo} 
							min={this.props.credito.montoMinimo}
							onChange={this.handleOnChangeMonto}
							value={this.props.credito.montoSolicitado ? this.props.credito.montoSolicitado : ""}
							required/>
					</Col>
					
					<div className="clearfix"/>
					<Col sm={6} md={4}>
						<SelectInput
							name={'subproducto'}
							label={ASR.FORMS.CREDITO.SUBPRODUCTO}
							value={this.props.credito ? this.props.credito.producto : ""}
							options={this.props.productos}
							required
							onChange={this.handleOnChangeSubProducto}/>
					</Col>
					<Col sm={6} md={4}>
						<SelectInput
							name={'plazo'}
							label={ASR.FORMS.CREDITO.PLAZO}
							value={this.props.credito ? this.props.credito.plazo : ""}
							options={this.props.plazos}
							required
							onChange={this.handleOnChangePlazo}/>
					</Col>
					<Col sm={6} md={4}>
						<TextInput
							name={'tasa'}
							label={ASR.FORMS.CREDITO.TASA}
							value={this.props.credito ? numeral(String(this.props.credito.tasaInteres/100)).format('0.0 %') : ""}
							readOnly
							disabled/>
					</Col>
				</Row>
				<Row className={'mb42'}>
					<Col xs={12}>
						<Fade in={!this.state.showTable}>
							<Button bsStyle="link" onClick={this.cancel}>{'Cancelar'}</Button>
						</Fade>
						<Button bsStyle="primary" className="pull-right" onClick={this.showTable}>{'Consultar tabla de amortización'}</Button>
					</Col>
				</Row>
				<Collapse in={this.state.showTable}>
					<div>
						<AmortizationTable
							imprimir
							data={this.props.tablaAmortizacion} 
							credito={this.props.credito}
							numItems={48} />
						<hr className={'mb70'}/>
						<Row><Col sm={12}><Button bsStyle="primary" className="pull-right" onClick={this.cancel}>{'Finalizar'}</Button></Col></Row>
					</div>
				</Collapse>
			</div>
		);
	}
};

CreditForm.propTypes = {
	credito: React.PropTypes.object
};

const mapStateToProps = (state, ownProps) => {
	
	let plazos = [], productos = [];

	if (Object.keys(state.plazos).length !== 0) {
		productos = selectors.productosFormattedForForm(state.plazos);

		if (state.credito.producto) {
			plazos = selectors.plazosFormattedForForm(state.plazos, state.credito.producto);
		} else {
			plazos = selectors.plazosFormattedForForm(state.plazos, productos[0].producto);
		}
	}
	return {
		anio: state.auto.anio,
		tablaAmortizacion: state.tablaAmortizacion,
		credito: state.credito,
		productos: productos,
		plazos: plazos,
		cancel: ownProps.cancel,
		error: state.error
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch)
	};
}
export default connect(mapStateToProps,mapDispatchToProps)(CreditForm);
