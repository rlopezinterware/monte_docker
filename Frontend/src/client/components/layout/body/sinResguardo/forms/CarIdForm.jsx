import React from "react";
import {Grid, Row, Col, FormGroup} from "react-bootstrap";
import TextInput from "../../../../common/TextInput";
import NumberInput from "../../../../common/NumberInput";

const CarDataForm = ({onChange, solicitud, validate}) => {
	return (
		<form>
			<Row className={'mb5'}>
				<Col sm={6} md={4}>
					<TextInput
						name={'vin'}
						label={'No. de VIN'}
						value={solicitud.vehiculo ? solicitud.vehiculo.vin: ""}
						required
						error={validate}
						disabled
						maxLength={17}
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'serieMotor'}
						label={'No. de Motor'}
						value={solicitud.vehiculo ? solicitud.vehiculo.serieMotor : ""}
						required
						error={validate}
						maxLength={20}
						alphanum
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'numeroTarjetaCirculacion'}
						label={'Tarjeta de circulación'}
						value={solicitud.vehiculo ? solicitud.vehiculo.numeroTarjetaCirculacion: ""}
						required
						error={validate}
						maxLength={30}
						alphanum
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'color'}
						label={'Color'}
						value={solicitud.vehiculo ? solicitud.vehiculo.color: ""}
						required
						error={validate}
						maxLength={30}
						alpha
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'placas'}
						label={'Placas'}
						value={solicitud.vehiculo ? solicitud.vehiculo.placas : ""}
						required
						error={validate}
						maxLength={10}
						alphanum
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'numeroFactura'}
						label={'No. Factura'}
						value={solicitud.vehiculo ? solicitud.vehiculo.numeroFactura : ""}
						required
						error={validate}
						maxLength={30}
						alphanum
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'rfcEmisor'}
						label={'RFC emisor'}
						value={solicitud.vehiculo ? solicitud.vehiculo.rfcEmisor : ""}
						required
						error={validate}
						maxLength={15}
						alphanum
						onChange={onChange}/>
				</Col>
				<Col sm={6} md={4}>
					<TextInput
						name={'agenciaOrigen'}
						label={'Agencia origen'}
						value={solicitud.vehiculo ? solicitud.vehiculo.agenciaOrigen : ""}
						required
						error={validate}
						maxLength={20}
						onChange={onChange}/>
				</Col>
			</Row>
		</form>
	);
};

CarDataForm.propTypes = {
	onChange: React.PropTypes.func
};

export default CarDataForm;
