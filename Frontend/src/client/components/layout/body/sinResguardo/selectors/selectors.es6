import numeral from "numeral";
import moment from "moment";
import _ from 'underscore';

export const productosFormattedForForm = (plazos) => {

    let p = plazos.map(plazo => {
        return {
            "id": plazo.producto,
            "producto": plazo.producto,
            "valor": plazo.subProducto,
            "subProducto": plazo.subProducto
        };
    });

    return _.uniq(p, 'id');
}

export const plazosFormattedForForm = (plazos, producto) => {

    let p = plazos.map(plazo => {
        return {
            "id": plazo.plazo,
            "valor": plazo.plazo,
            "tasa": plazo.tasa,
            "producto": plazo.producto,
            "subProducto": plazo.subProducto,
            "frecuencia": plazo.frecuencia
        };
    });

    return _.where(p, {producto: Number(producto)});
};

export const clearNullAndEmpty = (jsonObj) => {

   _.each(jsonObj, function(value, key){
        //if (value === "" || value === null){
        if (value === null){
            delete jsonObj[key];
        } else if (Object.prototype.toString.call(value) === '[object Object]') {
            clearNullAndEmpty(value);
        } else if (_.isArray(value)) {
            _.each(value, function (k,v) { clearNullAndEmpty(v); });
        }
    });    
};

export const tablaAmortizacionFormatted = (data) => {

    let tabla = data.map(element => {
        return "<tr>" +
            "<td>"+element.numeroPago+ "</td>" +
            "<td>"+element.fecha+ "</td>" +
            "<td>"+numeral(element.abonoTotal).format('$0,0.00')+ "</td>" +
            "<td>"+numeral(element.abonoCapital).format('$0,0.00')+ "</td>" +
            "<td>"+numeral(element.interes).format('$0,0.00')+ "</td>" +
            "<td>"+numeral(element.iva).format('$0,0.00')+ "</td>" +
            "<td>"+numeral(element.capitalFinal).format('$0,0.00')+"</td>" +
            "<td>"+numeral(element.saldoPagare).format('$0,0.00')+ "</td></tr>";
    });

    return tabla.join("");
};