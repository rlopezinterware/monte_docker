import {Nav, NavItem, Tabs, Tab} from "react-bootstrap";
import React, {PropTypes} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import * as actionsSinResguardo from "./SinResguardoActions";
import * as actionsAlert from "../../../alert/AlertActions";
import * as navMainActions from "../../../navMain/navMainActions";
import * as selectors from "./selectors/selectors";
import {existsTokens} from "../../../../utils/Security";

import CarDataForm from "./forms/CarDataForm";
import CreditConditionsForm from "./forms/CreditConditionsForm";

import numeral from "numeral";
import moment from "moment";
import {MODULES, CONTEXTO, APP} from "../../../../constants";
import INDICADOR from "../../../../svg/indicador_cotizador.png"

class ManageFirstOffer extends React.Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			key: 1,
			showTable: false
		};

		this.cancel = this.cancel.bind(this);
		this.handleSelectTab = this.handleSelectTab.bind(this);
		this.submitCarData = this.submitCarData.bind(this);
	}

	componentWillMount() {
		if (this.props.actionsSinResguardo.getAuthorizationModule(JSON.parse(sessionStorage.features), MODULES.COTIZAR)){
			if (existsTokens()) {
				this.props.actionsSinResguardo.resetAllStoreForASR();
				this.props.navMainActions.setActiveTab("Cotizador");
			}
		} else {
			this.context.router.push(CONTEXTO + APP);
		}
	}

	cancel() {
		let _this = this;
		this.props.actionsAlert.showAlert({
			message: <h3 className="text-center">¿Realmente quieres salir?</h3>,
			buttons: [
				{
					onClick: () => {
					},
					classCss: "btn-default",
					label: "Cancelar"
				},
				{
					onClick: () => {
						_this.props.actionsSinResguardo.resetAllStoreForASR();
						_this.handleSelectTab(1)
					},
					classCss: "btn-danger",
					label: "Salir"
				}
			]
		});
	}

	submitCarData () {

		this.props.actionsSinResguardo.getMontoPrestamo({...this.props.credito}, this.props.auto).then(() => {
			this.props.actionsSinResguardo.getPlazos(this.props.auto).then(()=> {
				this.props.actionsSinResguardo.resetTablaAmortizacionSuccess();
				this.handleSelectTab(2);
			});
		});
	}

	handleSelectTab(key) { this.setState({key}); }

	render() {
		return (
			<div>
				<h1 className="text-center" style={{display:'none'}}>{'Cotizador'}</h1>
				<img className="progress-indicator" src={INDICADOR}/>
				<Tabs activeKey={this.state.key} onSelect={this.handleSelectTab} className="tabs2" id="cotizador">
					<Tab eventKey={1} title="Datos del auto">
						<CarDataForm submit={{action:this.submitCarData, label: 'Cotizar'}}/>
					</Tab>
					<Tab eventKey={2} title="Detalles del crédito" disabled>
						<CreditConditionsForm cancel={this.cancel} />
					</Tab>
				</Tabs>
			</div>
		);
	}
}

ManageFirstOffer.propTypes = {
};

ManageFirstOffer.contextTypes = {
	router: PropTypes.object
};

const mapStateToProps = (state) => {
	return {
		auto: state.auto,
		credito: state.credito
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		actionsSinResguardo: bindActionCreators(actionsSinResguardo, dispatch),
		actionsAlert: bindActionCreators(actionsAlert, dispatch),
		navMainActions: bindActionCreators(navMainActions, dispatch)
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageFirstOffer);
