import alert from "./components/alert/AlertReducer";
import auto from "./components/layout/body/sinResguardo/reducers/autoReducer";
import {combineReducers} from "redux";
import customerData from "./components/layout/body/sinResguardo/reducers/customerDataReducer";
import error from "./components/layout/body/sinResguardo/reducers/ErrorReducer";
import header from "./components/layout/header/HeaderReducer";
import isLoading from "./components/loading/LoadingReducer";
import marcas from "./carCatalogueReducers/brandsReducer";
import submarcas from "./carCatalogueReducers/subBrandsReducer";
import modelos from "./carCatalogueReducers/modelsReducer";
import kilometrajes from "./carCatalogueReducers/kilometresReducer";
import complementos from "./carCatalogueReducers/complementsReducer";
import versiones from "./carCatalogueReducers/versionsReducer";
import localidadpld from "./components/catalogs/catLocalidadPLDReducer";
import menu from "./components/menu/MenuReducer";
import navItems from "./components/navMain/navMainReducer";

import plazos from "./components/layout/body/sinResguardo/reducers/plazosReducer";
import credito from "./components/layout/body/sinResguardo/reducers/creditoReducer";
import prestamo from "./components/layout/body/sinResguardo/reducers/montoPrestamoReducer";
import tablaAmortizacion from "./components/layout/body/sinResguardo/reducers/tablaAmortizacionReducer";

import solicitud from "./components/layout/body/sinResguardo/reducers/solicitudReducer";
import motivosRechazo from "./components/layout/body/sinResguardo/reducers/motivosRechazoReducer";
import motivoSeleccionado from "./components/ModalReject/ModalRejectReducer";

const rootReducer = combineReducers({
    alert,
    menu,
    navItems,
    isLoading,
    customerData,
    marcas,
    submarcas,
    modelos,
    versiones,
    kilometrajes,
    complementos,
    localidadpld,
    auto,
    tablaAmortizacion,
    header,
    plazos,
    credito,
    prestamo,
    solicitud,
    error,
    motivosRechazo,
    motivoSeleccionado
});

export default rootReducer;
