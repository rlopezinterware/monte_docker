import * as types from "../constants";
import InitialData from "../InitialData";

const complementsReducer = (state = InitialData.complementos, action) => {
    switch (action.type) {
        case types.ACTION_GET_COMPLEMENTOS:
            return action.complementos;
        default:
            return state;
    }
};

export default complementsReducer;
