import * as types from "../constants";
import InitialData from "../InitialData";

const subBrandsReducer = (state = InitialData.submarcas, action) => {
    switch (action.type) {
        case types.ACTION_GET_SUBMARCAS:
            return action.submarcas;
        default:
            return state;
    }
};

export default subBrandsReducer;
