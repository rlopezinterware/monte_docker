import * as types from "../constants";
import InitialData from "../InitialData";

const versionsReducer = (state = InitialData.versiones, action) => {
    switch (action.type) {
        case types.ACTION_GET_VERSIONES:
            return action.versiones;
        default:
            return state;
    }
};

export default versionsReducer;
