import * as types from "../constants";

import InitialData from "../InitialData";

const brandsReducer = (state = InitialData.marcas, action) => {
    switch (action.type) {
        case types.ACTION_GET_MARCAS:
            return action.marcas;
        default:
            return state;
    }
};

export default brandsReducer;
