import * as types from "../constants";
import InitialData from "../InitialData";

const kilometresReducer = (state = InitialData.kilometrajes, action) => {
    switch (action.type) {
        case types.ACTION_GET_KILOMETRAJES:
            return action.kilometrajes;
        default:
            return state;
    }
};

export default kilometresReducer;
