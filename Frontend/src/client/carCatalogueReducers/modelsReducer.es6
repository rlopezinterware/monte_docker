import * as types from "../constants";
import InitialData from "../InitialData";

const modelsReducer = (state = InitialData.modelos, action) => {
    switch (action.type) {
        case types.ACTION_GET_MODELOS:
            return action.modelos;
        default:
            return state;
    }
};

export default modelsReducer;
