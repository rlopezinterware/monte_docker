import React from "react";
import {Route, IndexRoute} from "react-router";
import App from "./components/App";
import SinResguardo from "./components/layout/body/sinResguardo/SinResguardo";
import ManageFirstOffer from "./components/layout/body/sinResguardo/ManageFirstOffer";
import ManageValuer from "./components/layout/body/sinResguardo/ManageValuer";
import ManageApplications from "./components/layout/body/sinResguardo/ManageApplications";
import ManageOfferCredit from "./components/layout/body/sinResguardo/ManageOfferCredit";
import ManageConclusion from "./components/layout/body/sinResguardo/ManageConclusion";
import Login from "./components/layout/body/login/Login";
import Continue from "./components/layout/body/login/Continue";
import {CONTEXTO,APP} from "./constants";

export default (
	<Route path={CONTEXTO} component={App}>
		<Route path={CONTEXTO + "/iniciar-sesion"} component={Login}/>
		<Route path={CONTEXTO + "/continue"} component={Continue}/>
		<Route path={CONTEXTO + APP} component={SinResguardo}>
			<Route path={CONTEXTO + APP + "/cotizador"} component={ManageFirstOffer}/>
			<Route path={CONTEXTO + APP + "/solicitud"} component={ManageApplications}/>
			<Route path={CONTEXTO + APP + "/inspeccion"} component={ManageValuer}/>
			<Route path={CONTEXTO + APP + "/oferta"} component={ManageOfferCredit}/>
			<Route path={CONTEXTO + APP + "/conclusion"} component={ManageConclusion}/>
		</Route>
	</Route>
);
