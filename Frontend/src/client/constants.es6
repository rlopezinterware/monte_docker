export const ERROR = 'ERROR';

export const CONTEXTO = '/asr'

export const APP = '/sin-resguardo'

export const ACTION_SHOW_MENU = 'ACTION_SHOW_MENU';
export const ACTION_HIDE_MENU = 'ACTION_HIDE_MENU';
export const ACTION_SHOW_ALERT = 'ACTION_SHOW_ALERT';
export const ACTION_HIDE_ALERT = 'ACTION_HIDE_ALERT';

export const ACTION_LOAD_TABS = 'ACTION_LOAD_TABS';
export const ACTION_GET_INITIAL_TABS = 'ACTION_GET_INITIAL_TABS';
export const ACTION_CHANGE_ACTIVE_TAB = 'ACTION_CHANGE_ACTIVE_TAB';

export const ACTION_GET_MARCAS = 'ACTION_GET_MARCAS';
export const ACTION_GET_SUBMARCAS = 'ACTION_GET_SUBMARCAS';
export const ACTION_GET_MODELOS = 'ACTION_GET_MODELOS';
export const ACTION_GET_VERSIONES = 'ACTION_GET_VERSIONES';
export const ACTION_GET_KILOMETRAJES = 'ACTION_GET_KILOMETRAJES';
export const ACTION_GET_COMPLEMENTOS = 'ACTION_GET_COMPLEMENTOS';
export const ACTION_GET_LOCALIDAD_PLD = 'ACTION_GET_LOCALIDAD_PLD';
export const ACTION_UPDATE_STATE_AUTO = 'ACTION_UPDATE_STATE_AUTO';
export const ACTION_RESET_STATE_AUTO = 'ACTION_RESET_STATE_AUTO';

export const ACTION_GET_TABLA_AMORTIZACION = 'ACTION_GET_TABLA_AMORTIZACION';
export const ACTION_RESET_TABLA_AMORTIZACION = 'ACTION_RESET_TABLA_AMORTIZACION';

export const ACTION_GET_USER_TOKEN = 'ACTION_GET_USER_TOKEN';
export const ACTION_VERIFY_HEADER_TOKENS = 'ACTION_VERIFY_HEADER_TOKENS';
export const ACTION_GET_DATA_CUSTOMER = 'ACTION_GET_DATA_CUSTOMER';

export const ACTION_ENABLE_FINGER_PRINT = 'ACTION_ENABLE_FINGER_PRINT';
export const ACTION_DISABLE_FINGER_PRINT = 'ACTION_DISABLE_FINGER_PRINT';

export const ACTION_GET_PLAZOS = 'ACTION_GET_PLAZOS';
export const ACTION_UPDATE_STATE_CREDITO = 'ACTION_UPDATE_STATE_CREDITO';
export const ACTION_GET_MONTO_PRESTAMO = 'ACTION_GET_MONTO_PRESTAMO';
export const ACTION_GET_SOLICITUD = 'ACTION_GET_SOLICITUD';
export const ACTION_GET_MOTIVOS_RECHAZO = 'ACTION_GET_MOTIVOS_RECHAZO';
export const ACTION_SET_MOTIVO_RECHAZO = 'ACTION_SET_MOTIVO_RECHAZO';

export const SPLIT = '#';
export const DESCRIPTION = 'Description';

const Constants = {

    BASE_URL_FRONT: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === 'local') ? 'http://localhost:9090' :
        process.env.NODE_DEPLOY === 'bluemix' ? 
        process.env.APP_ENV === "dev" ? 'https://dev1775-frontasr.mybluemix.net' : 
        process.env.APP_ENV === "tst" ? 'https://tst1775-frontasr.mybluemix.net' : 
        process.env.APP_ENV === "pre" ? 'https://pre1775-frontasr.mybluemix.net' : 
        process.env.APP_ENV === "pro" ? 'https://1775-frontasr.mybluemix.net': 'ERROR' :
        process.env.NODE_DEPLOY === 'docker' ? 
        process.env.APP_ENV === "local" ? 'http://localhost:9090' : 
        process.env.APP_ENV === "dev" ? 'http://10.30.4.213:9090' : 
        process.env.APP_ENV === "tst" ? 'http://10.30.4.44:9090' : 
        process.env.APP_ENV === "pre" ? 'http://10.30.4.213:9090' : 
        process.env.APP_ENV === "pro" ? 'http://10.30.4.44:9090': 'ERROR' : 'ERROR',

    BASE_URL_BEN: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === 'local') ? 'http://localhost:4000/api/' :
        process.env.NODE_DEPLOY === 'bluemix' ? 
        process.env.APP_ENV === "dev" ? 'https://dev1775-backasr.mybluemix.net/api/' : 
        process.env.APP_ENV === "tst" ? 'https://tst1775-backasr.mybluemix.net/api/' : 
        process.env.APP_ENV === "pre" ? 'https://pre1775-backasr.mybluemix.net/api/' : 
        process.env.APP_ENV === "pro" ? 'https://1775-backasr.mybluemix.net/api/': 'ERROR' : 
        process.env.NODE_DEPLOY === 'docker' ? 
        process.env.APP_ENV === "local" ? 'http://localhost:4000/api/' : 
        process.env.APP_ENV === "dev" ? 'http://10.30.4.213:4000/api/' : 
        process.env.APP_ENV === "tst" ? 'http://10.30.4.44:4000/api/' : 
        process.env.APP_ENV === "pre" ? 'http://10.30.4.213:4000/api/' : 
        process.env.APP_ENV === "pro" ? 'http://10.30.4.44:4000/api/': 'ERROR' : 'ERROR',

    BASE_URL_SEC: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === 'local') ? 'http://localhost:4000/auth/' :
        process.env.NODE_DEPLOY === 'bluemix' ? 
        process.env.APP_ENV === "dev" ? 'https://dev1775-backasr.mybluemix.net/auth/' : 
        process.env.APP_ENV === "tst" ? 'https://tst1775-backasr.mybluemix.net/auth/' : 
        process.env.APP_ENV === "pre" ? 'https://pre1775-backasr.mybluemix.net/auth/' : 
        process.env.APP_ENV === "pro" ? 'https://1775-backasr.mybluemix.net/auth/': 'ERROR' :
        process.env.NODE_DEPLOY === 'docker' ? 
        process.env.APP_ENV === "local" ? 'http://localhost:4000/auth/' : 
        process.env.APP_ENV === "dev" ? 'http://10.30.4.213:4000/auth/' : 
        process.env.APP_ENV === "tst" ? 'http://10.30.4.44:4000/auth/' : 
        process.env.APP_ENV === "pre" ? 'http://10.30.4.213:4000/auth/' : 
        process.env.APP_ENV === "pro" ? 'http://10.30.4.44:4000/auth/': 'ERROR' : 'ERROR',
        
    END_POINT_GET_LOGIN: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === 'local') ? 'https://iamdr.montepiedad.com.mx:4444' :
        process.env.NODE_DEPLOY === 'bluemix' ? 
        process.env.APP_ENV === "dev" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "tst" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "pre" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "pro" ? 'https://dcpsoapsr1.nmp.com.mx:54101': 'ERROR' :
        process.env.NODE_DEPLOY === 'docker' ? 
        process.env.APP_ENV === "local" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "dev" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "tst" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "pre" ? 'https://iamdr.montepiedad.com.mx:4444' : 
        process.env.APP_ENV === "pro" ? 'https://dcpsoapsr1.nmp.com.mx:54101': 'ERROR' : 'ERROR',

    LOGIN_REDIRECT_URI: (process.env.NODE_DEPLOY === undefined || process.env.NODE_DEPLOY === 'local') ? 'http://localhost:9090/asr/Continue' :
        process.env.NODE_DEPLOY === 'bluemix' ? 
        process.env.APP_ENV === "dev" ? 'https://dev1775-frontasr.mybluemix.net/asr/Continue' : 
        process.env.APP_ENV === "tst" ? 'https://tst1775-frontasr.mybluemix.net/asr/Continue' : 
        process.env.APP_ENV === "pre" ? 'https://pre1775-frontasr.mybluemix.net/asr/Continue' : 
        process.env.APP_ENV === "pro" ? 'https://1775-frontasr.mybluemix.net/asr/Continue': 'ERROR' :
        process.env.NODE_DEPLOY === 'docker' ? 
        process.env.APP_ENV === "local" ? 'http://localhost:9090/asr/Continue' : 
        process.env.APP_ENV === "dev" ? 'http://10.30.4.213:9090/asr/Continue' : 
        process.env.APP_ENV === "tst" ? 'http://10.30.4.44:9090/asr/Continue' : 
        process.env.APP_ENV === "pre" ? 'http://10.30.4.213:9090/asr/Continue' : 
        process.env.APP_ENV === "pro" ? 'http://10.30.4.44:9090/asr/Continue': 'ERROR' : 'ERROR',

    LOGIN_PATH: "/ms_oauth/oauth2/endpoints/oauthservice/authorize?response_type=code&client_id=asr&redirect_uri=PATH_URI&scope=Customer.Info%20UserProfile.me&state=getauthz",
    
    END_POINT_GET_LOGOUT: "/logout",

    CODE_REFRESH: "NMP-901",

    CODE_SESSION_OUT: "NMP-902",

    END_POINT_GET_USER_TOKEN: 'getUserToken',
    END_POINT_GET_DELETE_TOKEN: 'getLogoutToken',

    END_POINT_GET_DATA_CUSTOMER: 'getClienteById',
    END_POINT_GET_CUSTOMER_SOLICITUD: 'getClienteSolicitud',
    END_POINT_NEW_CUSTOMER_BP: 'newClienteBP',

    END_POINT_GET_MARCAS: 'getMarcas',
    END_POINT_GET_SUBMARCAS: 'getSubMarcas',
    END_POINT_GET_MODELOS: 'getModelos',
    END_POINT_GET_VERSIONES: 'getVersiones',
    END_POINT_GET_KILOMETRAJES_COMPLEMENTOS: 'getKilometrajeComplementos',
    END_POINT_GET_TABLA_AMORTIZACION: 'getTablaAmortizacion',
    END_POINT_GET_USER_REFRESH: 'getRefreshToken',
    END_POINT_GET_TEMPLATE: 'getTemplate',
    END_POINT_GET_LOCALIDAD_PLD: 'getLocalidadPLD',

    END_POINT_GET_PLAZOS: 'getPlazos',
    END_POINT_GET_MONTO_PRESTAMO: 'getMontoPrestamo',
    END_POINT_GET_COTIZACION: 'getCotizacion',
    END_POINT_NEW_SOLICITUD: 'newSolicitud',
    END_POINT_GET_SOLICITUD: 'getSolicitud',
    END_POINT_UPDATE_SOLICITUD: 'updateSolicitud',
    END_POINT_CANCEL_SOLICITUD: 'cancelSolicitud',
    END_POINT_GET_MOTIVOS_RECHAZO: 'getMotivosRechazo',
    END_POINT_ANALISIS_CREDITO: 'analisisCredito',

    END_POINT_NEW_CONTRATO: 'newContrato',
    END_POINT_DESEMBOLSO: 'desembolso',

    IS_LOADING: 'IS_LOADING',
    FORMAT_DATE_INPUT: 'YYYY-MM-DD'
};
export default Constants;

export const MIMETYPES = {
    JPG: 'image/jpeg',
    PNG: 'image/png',
    PDF: 'application/pdf',
    XML: 'text/xml'
};

export const ICONS = {
    IMG: 'icon-file-image-o',
    XML: 'icon-file-xml-o',
    PDF: 'icon-file-pdf-o',
    DEFAULT: 'icon-fa-check-square'
};

export const AUTO = {
    ATRIBUTES: {
        MARCA: 'marca',
        SUB_MARCA: 'submarca',
        MODELO: 'modelo',
        VERSION: 'version',
        KILOMETRAJE: 'kilometraje',
        NUMERO_MOTOR: 'numeroSerieMotor',
        NUMERO_CHASIS: 'numeroSerieChasis',
        COLOR: 'color',
        PLACAS: 'placas',
        EQUIPO_ADICIONAL: 'equipoAdicional'
    }
};

export const MODULES = {
    COTIZAR: 'Cotizar',
    REGISTRAR: 'Registrar',
    CONSULTAR: 'Consultar',
    INSPECCIONAR: 'Inspeccionar',
    ANALIZAR: 'Analizar',
    OFERTAR: 'Ofertar',
    CONTRATAR: 'Contratar'
}

export const APPL = {
    REJECTED: 0, // "Rechazado",
    TO_INSP: 3, // "Realizar inspección física",
    TO_OFFER: 6, // "Ofrecer oferta",
    TO_PAYMENT: 8, // "Documentación generada",
    CLOSED: 10 // "Cierre de folio"
}

export const ETAPAS = {
    INSPECCION: 4,
    ANALISIS: 5,
    OFERTA: 6,
    VERIFICACION: 8,
    CONTRATAR: 7
};

export const TIEMPO_SESION = {
    INACTIVIDAD: 900000, // 15 minutos
    ALERTA: 120000 // 2 minutos
}