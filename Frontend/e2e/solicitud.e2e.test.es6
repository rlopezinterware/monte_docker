import assert from "assert";
import test from "selenium-webdriver/testing";
import webdriver from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import chromedriver from "chromedriver";

const By = webdriver.By;
const until = webdriver.until;
const path = chromedriver.path;
const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);

const BASE_URL = "http://localhsot:8080/";
const credencial = "0000000000074593";

const driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();

test.describe("TEST E2E - LOGIN", () => {

    test.it("Ingreso de credenciales, inicio de sesión", () => {
        driver.manage().window().maximize();
        driver.get(BASE_URL + "");
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys("rcarrasco");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Manager1$");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        driver.sleep(1000);
    });

    test.it("Despliegue de menú", () => {
        driver.findElement(By.xpath('//i[contains(concat(" ", @class, " "), " icon-menu ")]')).click();
        driver.sleep(1000);
    });

    test.it("Click en asr", () => {
        driver.findElement(By.xpath('//div[contains(concat(" ", @class, " "), " menu-item ")][2]')).click();
        driver.sleep(1000);
    });

    test.it("Buscar cliente", () => {
        driver.findElement(By.xpath("//input[@name='numberCredential']")).sendKeys(credencial);
        driver.sleep(1000);
        driver.findElement(By.xpath("//input[@type='submit'][@class='main-btn']")).click();
        driver.sleep(4000);
    });

    test.it("Seleccionar marca", () => {
        driver.findElement(By.xpath("//select[@name='marca']")).click();
        driver.sleep(2000);
        driver.findElement(By.xpath("//select[@name='marca']//option[6]")).click();
        driver.sleep(1000);
    });

    test.it("Seleccionar submarca", () => {
        driver.findElement(By.xpath("//select[@name='submarca']")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath("//select[@name='submarca']//option[9]")).click();
        driver.sleep(1000);
    });

    test.it("Seleccionar modelo", () => {
        driver.findElement(By.xpath("//select[@name='modelo']")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath("//select[@name='modelo']//option[5]")).click();
        driver.sleep(2000);
    });

    test.it("Seleccionar version", () => {
        driver.findElement(By.xpath("//select[@name='version']")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath("//select[@name='version']//option[2]")).click();
        driver.sleep(1000);
    });

    test.it("Seleccionar kilometraje", () => {
        driver.findElement(By.xpath("//select[@name='kilometraje']")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath("//select[@name='kilometraje']//option[3]")).then(() => {
            driver.findElement(By.xpath("//select[@name='kilometraje']//option[3]")).click();
        });
        driver.sleep(1000);
    });

    test.it("Obtener avaluo", () => {
        driver.findElement(By.xpath("//input[@type='submit'][@class='form-btn-main']")).click();
        driver.sleep(4000);
    });

    test.it("Pasar a siguiente tab", () => {
        driver.findElement(By.xpath("//button[@class='form-btn-main']")).click();
        driver.sleep(1000);
    });

    test.it("Añadir porcentaje de castigo", () => {
        driver.findElement(By.xpath("//input[@name='porcentajeCastigo']")).sendKeys("22");
        driver.sleep(1000);
        driver.findElement(By.xpath("//textarea[@name='comentarios']")).sendKeys("esto es una prueba end to end");
        driver.sleep(1000);
    });

    test.it("Solicitar prestamo", () => {
        driver.findElement(By.xpath("//input[@type='submit']")).click();
        driver.sleep(4000);
    });

    test.it("Pasar a siguiente tab", () => {
        driver.findElement(By.xpath("//button[@class='form-btn-main']")).click();
        driver.sleep(1000);
    });

    test.it("Añadir datos del auto", () => {
        driver.findElement(By.xpath("//input[@name='numeroSerieMotor']")).sendKeys("32132");
        driver.sleep(1000);
        driver.findElement(By.xpath("//input[@name='numeroSerieChasis']")).sendKeys("7809789");
        driver.sleep(1000);
        driver.findElement(By.xpath("//input[@name='color']")).sendKeys("rojo");
        driver.sleep(1000);
        driver.findElement(By.xpath("//input[@name='placas']")).sendKeys("3123dwsa");
        driver.sleep(1000);
        driver.findElement(By.xpath("//label[@for='file-IFE']")).click();
        driver.sleep(6000);
        driver.findElement(By.xpath("//label[@for='file-Comprobante de domicilio']")).click();
        driver.sleep(4000);
        driver.findElement(By.xpath("//label[@for='file-Factura']")).click();
        driver.sleep(4000);
        driver.findElement(By.xpath("//label[@for='file-Tarjeta de circulación']")).click();
        driver.sleep(4000);
    });

    test.it("Solicitar prestamo", () => {
        driver.findElement(By.xpath("//button[@class='form-btn-main']")).click();
        driver.sleep(10000);
        driver.findElement(By.xpath('//i[contains(concat(" ", @class, " "), " icon-delete ")]')).click();
        driver.sleep(1000);
    });

});
