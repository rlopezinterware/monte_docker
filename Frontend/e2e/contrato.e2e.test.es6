import assert from "assert";
import test from "selenium-webdriver/testing";
import webdriver from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome";
import chromedriver from "chromedriver";

const By = webdriver.By;
const until = webdriver.until;
const path = chromedriver.path;
const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);


const BASE_URL = "http://localhsot:8080/";
const credencial = "0000000000074593";

const driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();

test.describe("TEST E2E - LOGIN", () => {

    test.it("Ingreso de credenciales, inicio de sesión", () => {
        driver.manage().window().maximize();
        driver.get(BASE_URL +"");
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys("rcarrasco");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Manager1$");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        driver.sleep(1000);
    });

    test.it("Despliegue de menú", () => {
        driver.findElement(By.xpath('//i[contains(concat(" ", @class, " "), " icon-menu ")]')).click();
        driver.sleep(1000);
    });

    test.it("Click en ASR", () => {
        driver.findElement(By.xpath('//div[contains(concat(" ", @class, " "), " menu-item ")][2]')).click();
        driver.sleep(1000);
    });

    test.it("Buscar cliente", () => {
        driver.findElement(By.xpath("//input[@name='numberCredential']")).sendKeys(credencial);
        driver.sleep(1000);
        driver.findElement(By.xpath("//input[@type='submit'][@class='main-btn']")).click();
        driver.sleep(2000);
    });

    test.it("Seleccionar Tramite Rechazado", () => {
        driver.findElement(By.xpath("//select[@name='tramite']")).click();
        driver.sleep(2000);
        driver.findElement(By.xpath("//select[@name='tramite']/option[contains(text(),'Rechazado')]")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath('//i[contains(concat(" ", @class, " "), " icon-delete ")]')).click();
        driver.sleep(2000);
    });

    test.it("Seleccionar Tramite Pendiente", () => {
        driver.findElement(By.xpath("//select[@name='tramite']")).click();
        driver.sleep(2000);
        driver.findElement(By.xpath("//select[@name='tramite']/option[contains(text(),'Pendiente')]")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath('//i[contains(concat(" ", @class, " "), " icon-delete ")]')).click();
        driver.sleep(2000);
    });

    test.it("Seleccionar Tramite Aprobado", () => {
        driver.findElement(By.xpath("//select[@name='tramite']")).click();
        driver.sleep(2000);
        driver.findElement(By.xpath("//select[@name='tramite']/option[contains(text(),'Aprobado')][last()]")).click();
        driver.sleep(1000);
        driver.findElement(By.xpath('//i[contains(concat(" ", @class, " "), " icon-delete ")]')).click();
        driver.sleep(1000);
    });

    test.it("Pasar al Tab de Credito", () => {
        driver.findElement(By.xpath("//ul[@class='nav nav-tabs']/li[4]")).click();
        driver.sleep(1000);
    });

    test.it("Generar tabla de Amortizacion", () => {
        driver.findElement(By.xpath("//input[@type='submit'][@class='form-btn-main']")).click();
        driver.sleep(2000);
    });

    test.it("Imprimir Contrato", () => {
        driver.findElement(By.xpath("//button[@class='form-btn-main']")).click();
        driver.sleep(2000);
	    //driver.quit();
    });
    


});
