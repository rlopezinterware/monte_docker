import express from "express";
import session from "express-session"
import webpack from "webpack";
import path from "path";
import config from "../webpack.config.dev";
import open from "open";
import 'colors';

/* eslint-disable no-console */

const port = process.env.PORT || 9090;
const app = express();
const compiler = webpack(config);

app.set('views', path.join(__dirname, "../src/client/"));

app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

app.use(session({secret: require('crypto').randomBytes(64).toString('hex'),
                 cookie:{maxAge:60000},
                 resave: true,
                 rolling: true,
                 saveUninitialized: true}));

app.use(require("webpack-dev-middleware")(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require("webpack-hot-middleware")(compiler));

app.get("/asr/logout", function (req, res) {
    
    req.session.destroy(function(err) {
        if(err) {
            console.log(err);
        } else {
            res.redirect('https://iamdr.montepiedad.com.mx:4444/loginNMP/ssoff.jsp');
        }
    });
});

app.get("/asr/Continue", function (req, res) {
    res.sendFile(path.join(__dirname, "../src/client/index.html"));
});

app.get("/asr/sin-resguardo*", function (req, res) {
    res.sendFile(path.join(__dirname, "../src/client/index.html"));
});

app.use(function(req, res){
  res.render('404', { status: 404, url: '404.html'});
});

app.listen(port, function (err) {

    if (err) {
        console.log(err);
    } else {
        var address = getExternalIp();
        var message = "Node server running on http://" + address + ":"+ port;
        console.log(message.green);
        
        open(`http://${address}:${port}/asr/sin-resguardo/`);

    }
});

const getExternalIp = () => {

	var ifconfig = require('os').networkInterfaces();
	var device, i, I, protocol;

	for (device in ifconfig) {
		// ignore network loopback interface
		if (device.indexOf('lo') !== -1 || !ifconfig.hasOwnProperty(device)) {
			continue;
		}
		for (i=0, I=ifconfig[device].length; i<I; i++) {
			protocol = ifconfig[device][i];

			// filter for external IPv4 addresses
			if (protocol.family === 'IPv4' && protocol.internal === false) {
				return protocol.address;
			}
		}
	}

	return null;
};

require("cf-deployment-tracker-client").track();