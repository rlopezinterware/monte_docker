import React, {PropTypes} from "react";

const TextInput = ({error, name, label, subLabel, onChange, placeholder, readOnly, required, value, disabled, maxLength, numeric}) => {

		let wrapperClass = "form-group";
		if (error && required && (!value)) {
				wrapperClass += " has-error";
		}

		let handleChange = (e) => {

				if (numeric) {
					e.preventDefault();
					let selTemp = e.target.selectionStart;
					let ival = e.target.value.replace(/[a-zA-ZáéíóúÁÉÍÓÚÄËÏÖÜäëïöüñÑ!*#$%&()=",+_{}´¨~¿?'¡!;:\.\-\/\[\]]/g,'');
					e.target.value = ival;
					e.target.selectionStart = selTemp;
					e.target.selectionEnd = selTemp;
				} else {
					e.target.value = e.target.value.toUpperCase();
				}
				onChange(e);
		}

		return <div className={wrapperClass}>
				<label className="control-label" htmlFor={name}>
						{label} {subLabel && <span className="small text-muted" >{subLabel}</span>}
						{required && <i className={'fa fa-circle requerido'}/>}
				</label>
				
				<input
						type="text"
						name={name}
						className="form-control"
						placeholder={placeholder}
						value={value}
						readOnly={readOnly}
						maxLength={maxLength}
						autoComplete="off"
						disabled={disabled}
						onChange={handleChange}/>
						{error && required && (!value) && <p>{'Este campo es requerido'}</p>}
		</div>;
};

TextInput.propTypes = {
		name: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired,
		onChange: PropTypes.func,
		placeholder: PropTypes.string,
		error: PropTypes.bool,
		required: PropTypes.bool,
		readOnly: PropTypes.bool
};

export default TextInput;
