#!/usr/bin/env nodejs

/*eslint-disable no-console */

var express = require("express");
var session = require("express-session");
var path = require("path");
var compression = require("compression");
var cfenv = require('cfenv');
var appenv = cfenv.getAppEnv();
var fs = require("fs"),
    path = require('path'),
    https = require("https")

import 'colors';

var port = process.env.PORT || 9090;
var app = express();
var server =  process.env.SSL ? https.createServer( { key: fs.readFileSync(path.join(__dirname, '../', "crs/key.pem")), cert: fs.readFileSync(path.join(__dirname, '../', "crs/cert.pem")), passphrase: 'montedepiedad'}, app) : http.createServer(app);

app.set('views', path.join(__dirname, "../src/client/"));

app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');

app.use(session({secret: require('crypto').randomBytes(64).toString('hex'),
                 cookie:{maxAge:60000},
                 resave: true,
                 rolling: true,
                 saveUninitialized: true}));

app.use(compression());
app.use(express.static("dist"));

app.get("/asr/logout", function (req, res) {

    req.session.destroy(function(err) {
        if(err) {
            console.log(err);
        } else {
            if (process.env.APP_ENV === undefined || process.env.APP_ENV !== "pro") {
                res.redirect('https://iamdr.montepiedad.com.mx:4444/loginNMP/ssoff.jsp');
            } else {
                res.redirect('https://dcpsoapsr1.nmp.com.mx:54101/lapaz/ssoff.jsp');
            } 
        }
    });
});

app.get("/asr/Continue", function (req, res) {
    res.sendFile(path.join(__dirname, "../dist/index.html"));
});

app.get("/asr/sin-resguardo*", function (req, res) {
    res.sendFile(path.join(__dirname, "../dist/index.html"));
});

app.use(function(req, res){
  res.render('404', { status: 404, url: '404.html'});
});


server.listen(port, function (err) {

    let protocol = process.env.SSL ? "https" : "http"

    if (err) {
        console.log(err);
    } else {
        var address = getExternalIp();
        console.log(("Node server running on " + protocol + "://" + address + ":"+ port).green);
    } 
});




const getExternalIp = () => {

	var ifconfig = require('os').networkInterfaces();
	var device, i, I, protocol;

	for (device in ifconfig) {
		// ignore network loopback interface
		if (device.indexOf('lo') !== -1 || !ifconfig.hasOwnProperty(device)) {
			continue;
		}
		for (i=0, I=ifconfig[device].length; i<I; i++) {
			protocol = ifconfig[device][i];

			// filter for external IPv4 addresses
			if (protocol.family === 'IPv4' && protocol.internal === false) {
				return protocol.address;
			}
		}
	}

	return null;
};

require("cf-deployment-tracker-client").track();