var webpack = require ("webpack");
var path = require ("path");
var ExtractTextPlugin = require ("extract-text-webpack-plugin");

var GLOBALS = {
    "process.env.NODE_ENV": JSON.stringify("production"),
    "process.env.NODE_DEPLOY": JSON.stringify(process.env.NODE_DEPLOY),
    "process.env.APP_ENV": JSON.stringify(process.env.APP_ENV),
    "process.env.SSL": JSON.stringify(process.env.SSL)
};

module.exports = {
    debug: true,
    devtool: "source-map",
    noInfo: false,
    entry: "./src/client/index",
    target: "web",
    output: {
        path: __dirname + "/dist", // Note: Physical files are only output by the production build task `npm run build`.
        publicPath: "/",
        filename: "bundle.js"
    },
    devServer: {
        contentBase: "./dist"
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin(GLOBALS),
        new ExtractTextPlugin("main.css"),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin()
    ],
    module: {
        loaders: [
            {test: /\.js$/, include: path.join(__dirname, "src"), loaders: ["babel"]},
            {test: /\.jsx$/, include: path.join(__dirname, "src"), loaders: ["babel"]},
            {test: /\.es6$/, include: path.join(__dirname, "src"), loaders: ["babel"]},
            {test: /(\.css)$/, loaders: ["style", "css"]},
            {test: /\.less$/, include: path.join(__dirname, "src"), exclude: /(node_modules)/, loader: "less"},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file"},
            {test: /\.(woff|woff2)$/, loader: "url?prefix=font/&limit=5000"},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream"},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml"},
            {test: /\.png$/, loader: "url?limit=10000&mimetype=image/png"}
        ]
    },
    resolve: {
        extensions: ["", ".js", ".jsx", ".es6", "json"]
    }
}
